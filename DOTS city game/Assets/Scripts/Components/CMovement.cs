﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Movement : IComponentData
{
    public Entity target;

    public float speed;
    public byte arrive; //also determines if its active
    public byte lookingForStoreTarget;
    public float3 targetPos;
    public float3 movePos;
    public uint seed;
}
public class CMovement : ComponentDataWrapper<Movement> { }