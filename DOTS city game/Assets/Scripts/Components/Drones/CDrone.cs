﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum NextJobType:byte
{
    None,
    Idle,
    JustCreated,
}

[System.Serializable]
public struct Drone : IComponentData
{
    public Entity workEntity;

    public Item carrying;
    public Item toGet;
    //public Item buildProcessItem;
    public byte getOrStore; //1 get // 2 store

    public NextJobType nextJob;
    public byte doChangeJob;

    public Entity specificTarget;
    public Entity closestReassignmentTarget;

    public MyJob myJob;

    public float currentTime;
    public byte employed;
}
public class CDrone : ComponentDataWrapper<Drone> { }
