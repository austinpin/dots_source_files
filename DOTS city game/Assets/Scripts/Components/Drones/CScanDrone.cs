﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum ScanDroneState : byte
{
    NewTarget,
    Working,
    NoMoreTargets,
}

[System.Serializable]
public struct ScanDrone : IComponentData
{
    public Entity scanTarget;
    public ScanDroneState state;
    public uint partition; //which target its going to go to
}
public class CScanDrone : ComponentDataWrapper<ScanDrone> { }
