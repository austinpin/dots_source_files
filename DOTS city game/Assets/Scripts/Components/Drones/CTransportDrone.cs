﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct TransportDrone : IComponentData
{
    public byte StoreOrGet; //1 store // 2 get

    public Entity storageHome;
    public Entity ParkingTo;
    public byte parkedWhere;// 0 = anywhere //1 = storage home , 2 = parking spot with fellow transports
    public byte switchParked;

    public float currentTime;

    public Item itemToGet;
    public ushort howManyOfItemToGet;

    public byte modeIdle; //0 not idle , 1 is idle dont work

    public int currentItems;
    public int maxItems;
}
public class CTransportDrone : ComponentDataWrapper<TransportDrone> { }
