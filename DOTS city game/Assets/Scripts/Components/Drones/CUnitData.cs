﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Mathematics;

[System.Serializable]
public struct UnitData : ISharedComponentData
{
    public Entity workerDrone;
    public Entity transportDrone;
    public Entity scanDrone;
}
public class CUnitData : SharedComponentDataWrapper<UnitData> { }
