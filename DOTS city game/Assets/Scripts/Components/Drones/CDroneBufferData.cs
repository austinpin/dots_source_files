﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public enum MyJob: byte
{
    NONE,
    MineField,
    SalvageField,
}

public enum AffinityType : byte
{
    NONE,
    Power,
    Data,
    Drones,
    Resources,
}

[System.Serializable]
public struct DroneBufferData : IComponentData
{
    public ushort maxDrones;
    public MyJob myJob;
    public AffinityType affinity;

    public ushort comingDrones;
}
public class CDroneBufferData : ComponentDataWrapper<DroneBufferData> { }