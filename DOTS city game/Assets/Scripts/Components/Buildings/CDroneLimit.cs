﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct DroneLimit : IComponentData
{
    public ushort workerAddition;
    public ushort scanAddition;
    public ushort transportAddition;
}
public class CDroneLimit : ComponentDataWrapper<DroneLimit> { }
