﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct TransportParking : IComponentData
{
    public Entity storageTarget;
    public byte active;

    public int maxItems;
    public int itemsAboutToBeTaken;  //Transfered give or take pending
    public int currentItems;

    public byte StoreOrGet; //1=store items into warehouses , 2 = get items for buildings
    public Entity buildTarget;
}
public class CTransportParking : ComponentDataWrapper<TransportParking> { }
