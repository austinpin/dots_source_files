﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum Research : byte
{
    ABC,
    DEF,
}

[System.Serializable]
public struct ResearchNode : ISharedComponentData
{
    public Research containedResearch;
}
public class CResearchNode : SharedComponentDataWrapper<ResearchNode> { }
