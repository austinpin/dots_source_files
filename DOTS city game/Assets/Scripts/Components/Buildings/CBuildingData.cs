﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Mathematics;

[System.Serializable]
public struct BuildingData : ISharedComponentData
{
    [Tooltip("building site")]
    public Material validHologramMat;
    public Material invalidHologramMat;

    public Mesh buildSiteMesh;
    public Material buildSiteMaterial;
    public float2 buildSiteSquareRadius;

    public MeshInstanceRenderer tPR;
    public MeshInstanceRenderer idleR;
    public MeshInstanceRenderer droneFactoryR;
    public MeshInstanceRenderer VMR;

    public MeshInstanceRenderer StartR;
    public MeshInstanceRenderer BeaconR;

    public MeshInstanceRenderer IrradiatedR;
    public MeshInstanceRenderer WindR;
    public MeshInstanceRenderer GeothermalR;
    public MeshInstanceRenderer FissionR;

    public MeshInstanceRenderer BaseR;

    public MeshInstanceRenderer TransmitterFactoryR;
    public MeshInstanceRenderer ReadvancementCentreR;

    public MeshInstanceRenderer PhotiteMinerR;
    public MeshInstanceRenderer NaniteMakerR;

    public MeshInstanceRenderer DuriumMinerR;
    public MeshInstanceRenderer ZoriumMakerR;

    public MeshInstanceRenderer VionsMinerR;
    public MeshInstanceRenderer NornMakerR;

    public MeshInstanceRenderer TechR;
}
public class CBuildingData : SharedComponentDataWrapper<BuildingData> { }
