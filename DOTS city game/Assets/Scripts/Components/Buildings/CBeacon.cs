﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct Beacon: IComponentData
{
    public byte activated; //single use structure for now
    //public float radius;
}
public class CBeacon : ComponentDataWrapper<Beacon> { }

