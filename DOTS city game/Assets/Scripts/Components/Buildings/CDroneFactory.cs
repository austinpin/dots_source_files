﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum DroneToMake : byte
{
    None,
    WorkerDrone,
    TransportDrone,
    ScanDrone,
}

[System.Serializable]
public struct DroneFactory : IComponentData
{
    public DroneToMake droneTomake;
    public DroneToMake changeOrder;
    public byte ammountToSpawn;
    public byte toSpawnIncrement;

    public byte working;

    public float maxTime;
    public float currentTime;
}
public class CDroneFactory : ComponentDataWrapper<DroneFactory> { }
