﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct Refinery : IComponentData
{
    public byte working;

    public Item input1;
    public int input1Ammount;

    public Item output1;
    public int output1Ammount;

    public Item output2;
    public int output2Ammount;

    public float currentTime;
    public float maxTime;
    public int gain;
}
public class CRefinery : ComponentDataWrapper<Refinery> { }

