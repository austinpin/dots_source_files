﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct StorageTarget : IComponentData
{
    public Entity storageTarget;
}
public class CStorageTarget : ComponentDataWrapper<StorageTarget> { }
