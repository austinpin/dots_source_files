﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct Salvage : IComponentData
{
    public Item scrap;
    public int scrapAmmount;

    public Item reward;
    public int rewardAmmount;

    //public Entity storeTarget;
    public Building changeInto;
    public byte change;

    public float currentTime;
    public float maxTime;
}
public class CSalvage : ComponentDataWrapper<Salvage> { }

