﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct NodeData : ISharedComponentData
{
    public Mesh unscannedMesh;
    public Material unscannedMaterial;

    public Mesh zoneMesh;
    public Material tileMat1White;
    public Material tileMat2Blue;
    public Material tileMat3Green;
    public Material tileMat4Purple;
    public Material tileMat5Yellow;
    public Material tileMat6Cyan;
    public Material tileMat7gold;
    public Material zoneMaterial;

    public Material hillMat;
    public Material geothermalMat;
    public Material techMat;
    public Material photiteMat;
    public Material duriumMat;
    public Material vionsMat;

    public Mesh pickUpMesh; //debris field
    public Material pickUpMaterial;
    public RadiusSet pickUpRadius;
    //public ushort pickUpMaxDrones;
    //public pickUp pickUpData;

    public Mesh salvageMesh; //debris field
    public Material salvageMaterial;
    public float2 salvageSquareRadius;
    public ushort salvageMaxDrones;
    public Salvage salvageData;

    public Mesh largeSalvageMesh;  // Scrap field
    public Material largeSalvageMaterial;
    public float2 largeSalvageSquareRadius;
    public ushort largeSalvageMaxDrones;
    public Salvage largeSalvageData;

    public Mesh overGrownMesh; 
    public Material overGrownMaterial;
    public float2 overGrownSquareRadius;
    public ushort overGrownMaxDrones;
    public OverGrown overGrownData;

    public Mesh overGrown2Mesh;
    public Material overGrown2Material;
    public float2 overGrown2SquareRadius;
    public ushort overGrown2MaxDrones;
    public OverGrown overGrown2Data;

    public Mesh hullMesh;
    public Material hullMaterial;
    public float2 hullSquareRadius;
    public ushort hullMaxDrones;
    public Salvage hullData;

    public Mesh workshopMesh;
    public Material workshopMaterial;
    public float2 workshopSquareRadius;
    public ushort workshopMaxDrones;
    public Salvage workshopData;

    public Mesh serverRoomMesh;
    public Material serverRoomMaterial;
    public float2 serverRoomSquareRadius;
    public ushort serverRoomMaxDrones;
    public Salvage serverRoomData;

    public Mesh metalMineMesh;
    public Material metalMineMaterial;
    public float2 metalMineSquareRadius;
    public ushort metalMineMaxDrones;
    public Salvage metalMineData;
}
public class CNodeData : SharedComponentDataWrapper<NodeData> { }
