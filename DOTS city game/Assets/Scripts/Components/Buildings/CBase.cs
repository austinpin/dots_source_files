﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserBase : IComponentData
{
    public byte transmitterFactories;
    public byte levetationFactories;
}
public class CUserBase: ComponentDataWrapper<UserBase> { }
