﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public enum ResType : byte
{
    None,
    Photite,
    Durium,
    Vions,

    Nanite,
    Zorium,
    Norn,

    Tech, //avoider
}


[System.Serializable]
public struct Harvester : IComponentData
{
    public float currentTime;
    public float maxTime;
    public int gain;

    public ResType type;
}
public class CHarvester : ComponentDataWrapper<Harvester> { }

