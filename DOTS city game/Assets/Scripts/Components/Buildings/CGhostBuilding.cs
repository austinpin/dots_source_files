﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct GhostBuilding : IComponentData
{
    //public Building whatIAm;
    public byte stateValidOrInvalid; //1 valid //2 invalid
}
public class CGhostBuilding : ComponentDataWrapper<GhostBuilding> { }
