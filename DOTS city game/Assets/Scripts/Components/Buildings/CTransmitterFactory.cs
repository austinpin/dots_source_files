﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct TransmitterFactory : IComponentData
{
    public float currentTime;
    public float maxTime;
    public uint gain;

    public byte type; //1 = transmitters , 2= levetations
}
public class CTransmitterFactory : ComponentDataWrapper<TransmitterFactory> { }

