﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct Farm : IComponentData
{
    public float currentTime;
    public float maxTime;
    public uint gain;
}
public class CFarm : ComponentDataWrapper<Farm> { }

