﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Generator : IComponentData
{
    public float currentTime;
    public float maxTime;

    public byte production;
}
public class CGenerator : ComponentDataWrapper<Generator> { }
