﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Science : IComponentData
{
    public float currentTime;
    public float maxTime;

    public byte ammount;
}
public class CScience : ComponentDataWrapper<Science> { }
