﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct HazardZone : IComponentData
{
    public float currentTime;
    public float maxTime;

    public byte ammount;

    public Building changeInto;
    public byte change;
}
public class CHazardZone : ComponentDataWrapper<HazardZone> { }
