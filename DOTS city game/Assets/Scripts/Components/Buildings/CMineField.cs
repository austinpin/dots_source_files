﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct MineField : IComponentData
{
    public Item ore;
    //public Entity storeTarget;

    public float maxTime;
}
public class CMineField : ComponentDataWrapper<MineField> { }
