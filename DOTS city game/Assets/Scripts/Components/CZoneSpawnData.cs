﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

[System.Serializable]
public struct ZoneSpawnData : IComponentData
{
    public Position pos;
    public Zone zone;
}
public class CZoneSpawnData : ComponentDataWrapper<ZoneSpawnData> { }

