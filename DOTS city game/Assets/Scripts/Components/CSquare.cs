﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Square : IComponentData
{
    public float2 length;
}
public class CSquare : ComponentDataWrapper<Square> { }

