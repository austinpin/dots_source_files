﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Status : IComponentData
{
    public byte active;
    public byte boosted;
    public byte level;

    public float radius;

    public byte reset;

    public byte autoUpgrade;
    public byte doUpgradeOnce;

    public byte processUpgraded;
    public Building iAm;
}
public class CStatus : ComponentDataWrapper<Status> { }
