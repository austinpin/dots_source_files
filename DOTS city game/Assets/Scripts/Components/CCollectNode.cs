﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum Pickup: byte
{
    Drone,
    Energy,
    //Resource,

    photite=50,
    durium=51,
    vions =52,
}

[System.Serializable]
public struct CollectNode : IComponentData
{
    public Pickup give;
    public byte ammount;
    public byte ifGive;
}
public class CCollectNode : ComponentDataWrapper<CollectNode> { }
