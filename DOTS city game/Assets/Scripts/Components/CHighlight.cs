﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct Highlight : IComponentData
{
    public byte id;
}
public class CHighlight : ComponentDataWrapper<Highlight> { }

