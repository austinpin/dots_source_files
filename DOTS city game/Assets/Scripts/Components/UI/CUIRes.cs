﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIRes : ISharedComponentData
{
    public TextMeshProUGUI money;

    public TextMeshProUGUI hasPower;
    public TextMeshProUGUI allocatePower;

    public TextMeshProUGUI transmitters;
    public TextMeshProUGUI levetations;

    public TextMeshProUGUI hasPhotite;
    public TextMeshProUGUI allocatePhotite;

    public TextMeshProUGUI hasNanites;
    public TextMeshProUGUI allocateNanites;

    public TextMeshProUGUI hasDurium;
    public TextMeshProUGUI allocateDurium;

    public TextMeshProUGUI hasZorium;
    public TextMeshProUGUI allocateZorium;

    public TextMeshProUGUI hasVions;
    public TextMeshProUGUI allocateVions;

    public TextMeshProUGUI hasNorn;
    public TextMeshProUGUI allocateNorn;

    public TextMeshProUGUI objMain;
    public TextMeshProUGUI obj1;
    public TextMeshProUGUI obj2;

    //readvancements
    public TextMeshProUGUI title;
    public TextMeshProUGUI description;
    public TextMeshProUGUI name;

    //upgrading
    public TextMeshProUGUI has1;
    public TextMeshProUGUI allocate1;
    public Image Icon1;

    public TextMeshProUGUI has2;
    public TextMeshProUGUI allocate2;
    public Image Icon2;

    public TextMeshProUGUI has3;
    public TextMeshProUGUI allocate3;
    public Image Icon3;

    public TextMeshProUGUI comp1;
    public Image Icon4;

    public TextMeshProUGUI power1;
    public TextMeshProUGUI descriptionText;

    public TextMeshProUGUI levelText;
    public TextMeshProUGUI VMLevelText;

    //sprites;
    public Sprite Photite;
    public Sprite Durium;
    public Sprite Vions;

    public Sprite Nanite;
    public Sprite Zorium;
    public Sprite Norn;
}
public class CUIRes : SharedComponentDataWrapper<UIRes> { }