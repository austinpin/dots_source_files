﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIButtons : ISharedComponentData
{
    public Button transmitterFactory;
}
public class CUIButtons : SharedComponentDataWrapper<UIButtons> { }