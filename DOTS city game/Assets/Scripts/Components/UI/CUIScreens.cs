﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIScreens : ISharedComponentData
{
    public GameObject PlaceStart;
    public GameObject Click;
    public GameObject Stats;
}
public class CUIScreens: SharedComponentDataWrapper<UIScreens> { }