﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIReferances : ISharedComponentData
{
    public GameObject primaryBG;
    public GameObject buildMenu;
    public GameObject InvMoveMenu;
    public GameObject InvMenu;
    public GameObject Info;
    public GameObject DroneFactoryMenu;
    public GameObject UnscannedMenu;
    public GameObject TradeMenu;
    public GameObject ReadvMenu;

    public GameObject subBuildMenu;
    public GameObject viewMenu;

    public TextMeshProUGUI DFCurrentMake;
    public TextMeshProUGUI DFNextMake;

    public TextMeshProUGUI DFtimeCurrent;
    public TextMeshProUGUI DFtimeMax;

    public TextMeshProUGUI unscannedCurrentTime;
    public TextMeshProUGUI unscannedMaxTime;
    public TextMeshProUGUI unscannedPower;

    public TextMeshProUGUI selectedText;

    public TextMeshProUGUI infoTitleText;
    public TextMeshProUGUI InfoDescriptionText;

    public TextMeshProUGUI notificationText;

    public EncylopediaButtons buttons;
}
public class CUIReferances : SharedComponentDataWrapper<UIReferances> { }