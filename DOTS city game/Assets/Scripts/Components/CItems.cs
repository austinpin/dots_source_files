﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct Storage
{
    public Item itemStoring;
    public ushort containsAmmount;
    public ushort maxContains;

    public ushort itemsComing;
    public ushort askForMore;
}

[InternalBufferCapacity(8)]
[System.Serializable]
public struct StorageBuffer : IBufferElementData
{
    public Storage storage;

    public static implicit operator Storage(StorageBuffer e) { return e.storage; }
    public static implicit operator StorageBuffer(Storage e) { return new StorageBuffer{ storage = e }; }
}

[InternalBufferCapacity(8)]
[System.Serializable]
public struct TransportDroneBuffer : IBufferElementData
{
    public Storage storage;

    public static implicit operator Storage(TransportDroneBuffer e) { return e.storage; }
    public static implicit operator TransportDroneBuffer(Storage e) { return new TransportDroneBuffer { storage = e }; }
}

[InternalBufferCapacity(8)]
[System.Serializable]
public struct TransportParkingBuffer : IBufferElementData
{
    public Storage storage;

    public static implicit operator Storage(TransportParkingBuffer e) { return e.storage; }
    public static implicit operator TransportParkingBuffer(Storage e) { return new TransportParkingBuffer { storage = e }; }
}

[InternalBufferCapacity(8)]
[System.Serializable]
public struct NeededItemsBuffer : IBufferElementData
{
    public Storage storage;

    public static implicit operator Storage(NeededItemsBuffer e) { return e.storage; }
    public static implicit operator NeededItemsBuffer(Storage e) { return new NeededItemsBuffer { storage = e }; }
}

[InternalBufferCapacity(8)]
[System.Serializable]
public struct TPNeededItemsBuffer : IBufferElementData
{
    public Storage storage;

    public static implicit operator Storage(TPNeededItemsBuffer e) { return e.storage; }
    public static implicit operator TPNeededItemsBuffer(Storage e) { return new TPNeededItemsBuffer { storage = e }; }
}

[InternalBufferCapacity(8)]
[System.Serializable]
public struct DroneFactoryBuffer : IBufferElementData
{
    public Storage storage;

    public static implicit operator Storage(DroneFactoryBuffer e) { return e.storage; }
    public static implicit operator DroneFactoryBuffer(Storage e) { return new DroneFactoryBuffer { storage = e }; }
}

public enum Item : byte
{
    Empty,
    Scrap,
    Materials,
    Water,
    Oxegon,
    Hydrogen,
    food,
}
