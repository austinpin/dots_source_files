﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[InternalBufferCapacity(25)]
[System.Serializable]
public struct IdleDroneBuffer : IBufferElementData
{
    public Entity entity;

    public static implicit operator Entity(IdleDroneBuffer e) { return e.entity; }
    public static implicit operator IdleDroneBuffer(Entity e) { return new IdleDroneBuffer { entity = e }; }
}

[InternalBufferCapacity(25)]
[System.Serializable]
public struct DroneBuffer : IBufferElementData
{
    public Entity entity;

    public static implicit operator Entity(DroneBuffer e) { return e.entity; }
    public static implicit operator DroneBuffer(Entity e) { return new DroneBuffer { entity = e }; }
}

[InternalBufferCapacity(25)]
[System.Serializable]
public struct PlayerResearchedBuffer : IBufferElementData
{
    public Research research;

    public static implicit operator Research(PlayerResearchedBuffer e) { return e.research ; }
    public static implicit operator PlayerResearchedBuffer(Research e) { return new PlayerResearchedBuffer { research = e}; }
}

public enum PrefabType : byte
{
    None,
    TransmitterFactory,
    LevetationsFactory,
    VM,
    ReadvancementCentre,
    GrandBeacon,
    TerraPort,
}

public struct UpgradeItems
{
    public byte energyHas;
    public byte energyAllo;

    public int drones;
    public byte transmitters;
    public byte levetations;

    public byte photiteHas;
    public byte photiteAllo;
    public byte nanitesHas;
    public byte nanitesAllo;
    public byte duriumHas;
    public byte duriumAllo;
    public byte zoriumHas;
    public byte zoriumAllo;
    public byte vionsHas;
    public byte vionsAllo;
    public byte nornHas;
    public byte nornAllo;

    public PrefabType prefab;
}

[InternalBufferCapacity(10)]
[System.Serializable]
public struct Upgradeable : IBufferElementData
{
    public UpgradeItems itemsNeeded;

    public static implicit operator UpgradeItems(Upgradeable e) { return e.itemsNeeded; }
    public static implicit operator Upgradeable(UpgradeItems e) { return new Upgradeable { itemsNeeded = e }; }
}

/*
[InternalBufferCapacity(5)]
[System.Serializable]
public struct UserToScanBuffer : IBufferElementData
{
    public Entity e;

    public static implicit operator Entity(UserToScanBuffer ex) { return ex.e; }
    public static implicit operator UserToScanBuffer(Entity ex) { return new UserToScanBuffer { e = ex }; }
}
*/
