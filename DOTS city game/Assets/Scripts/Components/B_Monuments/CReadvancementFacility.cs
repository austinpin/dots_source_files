﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct ReadvancementFacility : IComponentData
{
    public int x;
}
public class CReadvancementFacility : ComponentDataWrapper<ReadvancementFacility> { }

