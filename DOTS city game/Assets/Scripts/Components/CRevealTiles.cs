﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Mathematics;

public enum RevealType : byte
{
    None,
    GeothermalZone,
    BoostZone,
    Pickup,

    Hill=30,

    photiteClickable=40,

    PhotiteZone=60,
    SmallPhotiteZone=61,
    DuriumZone=62,
    SmallDuriumZone=63,
    VionsZone=64,
    SmallVionsZone=65,

    TechZone = 70,
}

[System.Serializable]
public struct RevealTile : IComponentData
{
    public ushort powerNeeded;
    public RevealType type;
    public byte ammount;
    public byte size;

    public byte reveal;
}
public class CRevealTiles : ComponentDataWrapper<RevealTile> { }