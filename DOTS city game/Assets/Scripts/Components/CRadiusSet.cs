﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct RadiusSet : ISharedComponentData
{
    public float clickRadius;
    public float buildingRadius;

    public byte manualID; //prevent selfcomparison
}
public class CRadiusSet : SharedComponentDataWrapper<RadiusSet> { }
