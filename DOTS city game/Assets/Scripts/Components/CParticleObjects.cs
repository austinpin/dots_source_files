﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct ParticleObjects : ISharedComponentData
{
    public GameObject effect1;
}
public class CParticleObjects : SharedComponentDataWrapper<ParticleObjects> { }
