﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct Zone : IComponentData
{
    public RevealType type;
    public float radius;
    public int ammount;
}
public class CZone : ComponentDataWrapper<Zone> { }

