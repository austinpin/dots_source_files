﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserPrefabs : IComponentData
{
    public byte transmitterFactories;
    public byte levetationFactories;

    public byte VM;


    public byte ReadvancementFacility;
    public byte GrandBeacon;
    public byte TerraPort;
}
public class CUserPrefabs: ComponentDataWrapper<UserPrefabs> { }
