﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserStats : IComponentData
{
    public short _photiteMinerLevel1;
    public short _NaniteMakerLevel1;

    public short _duriumMinerLevel1;
    public short _zoriumMakerLevel1;

    public short _vionsMinerLevel1;
    public short _nornMakerLevel1;

    public short _irradiatedGeneratorLevel1;
    
    public short _baseLevel1;

    public short _transmitterFactoryLevel1;
    public short _levetationsFactoryLevel1;
    public short _beaconsLevel1;
}
public class CUserStats : ComponentDataWrapper<UserStats> { }
