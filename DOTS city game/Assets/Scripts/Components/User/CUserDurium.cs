﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserDurium: IComponentData
{
    public uint hasMax;
    public uint hasCurrent;

    public uint allocationCurrent;
    public uint allocationMax;
}
public class CUserDurium : ComponentDataWrapper<UserDurium> { }
