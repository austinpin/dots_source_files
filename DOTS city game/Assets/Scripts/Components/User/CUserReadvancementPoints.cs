﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserReadvancementPoints : IComponentData
{
    public int points;
}
public class CUserReadvancementPoints : ComponentDataWrapper<UserReadvancementPoints> { }
