﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum Readvancements : byte
{
    None,
    TransmitterFactoryBP1, //1
    //M_GrandBeacon, //2
    WindTurbines, //2
    NaniteMaker, //2
    TerraPort, //4
    DuriumMine,//3
    GeothermalGenerator,//3
    ZoriumMaker,//4
    VionsMine,//5
    FissionGenerator,//5
    NornMaker,//6
}

[System.Serializable]
public struct UserUnlocks: IComponentData
{
    public byte unlockReadvancements;
    public byte unlockReadvancementsBuilding;
    public byte techTransmitterFactory;
    //public byte unlockGrandBeacon;

    public byte unlockWindTurbine;
    public byte unlockNaniteMaker;
    public byte unlockGeothermalGenerator;

    public byte unlockTerraPort;

    public byte unlockDuriumMine;
    public byte unlockZoriumMaker;

    public byte unlockVionsMine;
    public byte unlockFissionGenerator;
    public byte unlockNornMaker;
}
public class CUserUnlocks : ComponentDataWrapper<UserUnlocks> { }
