﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserMoney : IComponentData
{
    public uint Max;
    public uint current;
}
public class CUserMoney : ComponentDataWrapper<UserMoney> { }
