﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserPickReadv : IComponentData
{
    public Readvancements Clicked;
    public byte tryBuy;
}
public class CUserPickReadv : ComponentDataWrapper<UserPickReadv> { }
