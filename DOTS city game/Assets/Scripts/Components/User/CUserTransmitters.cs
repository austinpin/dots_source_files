﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserTransmitters : IComponentData
{
    public uint Max;
    public uint current;
    public float power;
    public float updateTime;
}
public class CUserTransmitters : ComponentDataWrapper<UserTransmitters> { }
