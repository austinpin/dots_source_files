﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserQuests : IComponentData
{
    public byte placedStart;

    public byte _UpgradeGrandBeacon;
    public byte _ClickMinePhotite;

    public byte _1irradiatedGeneratorLevel1;
    public byte _1photiteMinerLevel1;
    public byte _1baseLevel1;

    public byte _transmitterFactoryTech;
    public byte _1readvancementCentreLevel1;

    public byte _1transmitterFactoryLevel1;
    //public byte _3beaconsLevel1;

    public byte _1VMLevel1;

    public byte TotalVMLevel;
}
public class CUserQuests : ComponentDataWrapper<UserQuests> { }
