﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

[System.Serializable]
public struct PlayerOther : IComponentData
{
    public byte placedStart;
    public byte updateReveal;
}
public class CPlayerOther : ComponentDataWrapper<PlayerOther> { }

