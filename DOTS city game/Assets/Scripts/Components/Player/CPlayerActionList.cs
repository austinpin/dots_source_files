﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum PlayerAction : byte
{
    Nothing,
    AddDrone,
    SubtractDrone,
}


[System.Serializable]
public struct PlayerActionList : IComponentData
{
    public PlayerAction playerAction;
}
public class CPlayerActionList : ComponentDataWrapper<PlayerActionList> { }
