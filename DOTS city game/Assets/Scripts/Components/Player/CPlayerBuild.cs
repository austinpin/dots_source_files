﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct PlayerBuild : IComponentData
{
    public float3 worldPos;
    public Building toBuild;

    public byte doVerifyPlaceBuild;
}
public class CPlayerBuild : ComponentDataWrapper<PlayerBuild> { }
