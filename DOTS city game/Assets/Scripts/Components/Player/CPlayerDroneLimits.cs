﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct PlayerDroneLimits : IComponentData
{
    public ushort workerLimit;
    public ushort scanLimit;
    public ushort transportLimit;
}
public class CPlayerDroneLimits : ComponentDataWrapper<PlayerDroneLimits> { }
