﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;

public enum Info : byte
{
    None,
    B_TransportParking,
    B_Idle,
    B_WareHouse,
    B_Science,
    B_DroneFactory,
    B_WorkerDroneFacility,
    B_VictoryMonument,
}

[System.Serializable]
public struct PlayerInfo : IComponentData
{
    public Info showing;
}
public class CInfo : ComponentDataWrapper<PlayerInfo> { }