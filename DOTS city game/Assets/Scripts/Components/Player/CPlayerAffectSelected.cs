﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public enum HowToAffect : byte
{
    None,
    Deactivate,
    DroneFactory_WorkerDrone,
    DroneFactory_transportDrone,
    DroneFactory_ScanDrone,
    Unscanned_ActivateScan,

    LevelUp = 100,
}

[System.Serializable]
public struct PlayerAffectSelected : IComponentData
{
    public HowToAffect howToAffect;
}
public class CPlayerAffectSelected : ComponentDataWrapper<PlayerAffectSelected> { }