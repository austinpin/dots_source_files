﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;


[System.Serializable]
public struct PlayerData : IComponentData
{
    public Entity selected;
    public Entity highlighted;
    public Entity subSelected;
}
public class CPlayerData : ComponentDataWrapper<PlayerData> { }
