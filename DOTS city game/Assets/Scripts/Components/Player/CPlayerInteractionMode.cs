﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum PlayerInteractionModes : byte
{
    View,
    Build,
    Selected,
    Info,
    Trade,
    Readvancement,
}

[System.Serializable]
public struct PlayerInteractionMode : IComponentData
{
    public PlayerInteractionModes playerInteractMode;
}
public class CPlayerInteractionMode : ComponentDataWrapper<PlayerInteractionMode> { }
