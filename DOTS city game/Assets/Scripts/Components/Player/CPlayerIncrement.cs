﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct PlayerIncrement : IComponentData
{
    public ushort increment;
}
public class CPlayerIncrement : ComponentDataWrapper<PlayerIncrement> { }
