﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIReadv : ISharedComponentData
{
    public TextMeshProUGUI pointsText;
    public TextMeshProUGUI pointsText2;
    public Image TransmitterFactoryTech;
    public Image GrandBeaconTech;
    public Image WindTurbineTech;
    public Image NaniteMakerTech;
    public Image TerraPortTech;
    public Image DuriumMineTech;
    public Image GeothermalTech;
    public Image ZoriumMalerTech;
    public Image VionsMineTech;
    public Image FissionGeneratorTech;
    public Image NornMakerTech;
}
public class CUIReadv : SharedComponentDataWrapper<UIReadv> { }