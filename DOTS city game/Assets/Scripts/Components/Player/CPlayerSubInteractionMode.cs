﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum PlayerSubInteractionModes : byte
{
    None,
    MoveOrder,
    AssignOrder,
}

[System.Serializable]
public struct PlayerSubInteractionMode : IComponentData
{
    public PlayerSubInteractionModes playerSubInteractMode;
}
public class CPlayerSubInteractionMode : ComponentDataWrapper<PlayerSubInteractionMode> { }
