﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Home : IComponentData
{
    public float3 actualPos;
}
public class CHome : ComponentDataWrapper<Home> { }