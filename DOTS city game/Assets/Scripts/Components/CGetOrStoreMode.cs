﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct GetOrStoreMode : IComponentData
{
    public byte getOrStoreMode; //0 nomode //1 get // 2 store
}
public class CGetOrStoreMode : ComponentDataWrapper<GetOrStoreMode> { }
