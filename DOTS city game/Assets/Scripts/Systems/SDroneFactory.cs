﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

public class SDroneFactory : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _DroneFactory
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<DroneFactory> droneFactory;
        public BufferArray<NeededItemsBuffer> neededItemsBuffer;
        public BufferArray<DroneFactoryBuffer> droneFactoryBuffer;
    }

    struct _UnitData
    {
        [ReadOnly] public SharedComponentDataArray<UnitData> unitData;
    }

    [Inject] _DroneFactory _droneFactory;
    [Inject] _UnitData _unitData;

    [ReadOnly] [Inject] BufferFromEntity<NeededItemsBuffer> allNeededItems;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        PrepareDroneFactoryJob job1 = new PrepareDroneFactoryJob
        {
            droneFactory = _droneFactory.droneFactory,
            droneFactoryBuffer = _droneFactory.droneFactoryBuffer,

            unitData = _unitData.unitData,

            allNeededItems = allNeededItems,
        };
        JobHandle handle = job1.Schedule(_droneFactory.Length, 1, inputDeps);

        DroneFactoryJob job = new DroneFactoryJob
        {
            self = _droneFactory.self,
            droneFactory = _droneFactory.droneFactory,
            neededItemsBuffer = _droneFactory.neededItemsBuffer,
            droneFactoryBuffer=_droneFactory.droneFactoryBuffer,

            deltaTime = Time.deltaTime,
        };
        buildHandle = job.Schedule(_droneFactory.Length, 1, handle);

        return buildHandle;
    }

    //[BurstCompile]
    struct PrepareDroneFactoryJob : IJobParallelFor
    {
        public ComponentDataArray<DroneFactory> droneFactory;
        public BufferArray<DroneFactoryBuffer> droneFactoryBuffer;

        [ReadOnly] public SharedComponentDataArray<UnitData> unitData;

        [ReadOnly] public BufferFromEntity<NeededItemsBuffer> allNeededItems;
        public void Execute(int index)
        {
            if (droneFactory[index].droneTomake!=DroneToMake.None)
            {
                //Essentially, grab the differnt kinds of resources a drone needs to be created, its "needed items buffer"
                //we will use that later
                Entity droneToMake = Entity.Null;
                if (droneFactory[index].droneTomake == DroneToMake.WorkerDrone)
                {
                    droneToMake = unitData[0].workerDrone;
                }
                if (droneFactory[index].droneTomake == DroneToMake.TransportDrone)
                {
                    droneToMake = unitData[0].transportDrone;
                }
                if (droneFactory[index].droneTomake == DroneToMake.ScanDrone)
                {
                    droneToMake = unitData[0].scanDrone;
                }
                DynamicBuffer<NeededItemsBuffer> nib = allNeededItems[droneToMake];

                //Make the drone factory buffer an exact copy of what we need to make a specfic drone the player has chosen;
                droneFactoryBuffer[index].Clear();

                for (int i = 0; i < nib.Length; i++)
                {
                    droneFactoryBuffer[index].Add(nib[i].storage);
                }
            }
        }
    }

    [BurstCompile]
    struct DroneFactoryJob : IJobParallelFor
    {
        public EntityArray self;
        public ComponentDataArray<DroneFactory> droneFactory; 
        public BufferArray<NeededItemsBuffer> neededItemsBuffer; //actual inventory
        public BufferArray<DroneFactoryBuffer> droneFactoryBuffer; //items needed to make drone

        public float deltaTime;

        public void Execute(int index)
        {
            DroneFactory df = droneFactory[index];

            //Sequential from last if statement, It requires it to apply working = 0 for this to work
            if (df.working == 0)
            {
                if (df.changeOrder != DroneToMake.None)
                {
                    if (df.changeOrder == DroneToMake.WorkerDrone)
                    {
                        df.droneTomake = DroneToMake.WorkerDrone;
                        df.maxTime = 2;
                        df.toSpawnIncrement = 1;
                    }
                    if (df.changeOrder == DroneToMake.TransportDrone)
                    {
                        df.droneTomake = DroneToMake.TransportDrone;
                        df.maxTime = 22;
                        df.toSpawnIncrement = 3;
                    }
                    if (df.changeOrder == DroneToMake.ScanDrone)
                    {
                        df.droneTomake = DroneToMake.ScanDrone;
                        df.maxTime = 16;
                        df.toSpawnIncrement = 7;
                    }

                    df.changeOrder = DroneToMake.None;
                    droneFactory[index] = df;
                    return; //Allow change to process otherwise resources might be taken as seen in script below
                }
            }

            //if its not working, find and consume resources then begin work
            if (df.working == 0 && df.droneTomake!=DroneToMake.None)
            {
                int validaton = 0;
                int validatonTotal = droneFactoryBuffer[index].Length;

                for (int i = 0; i < droneFactoryBuffer[index].Length; i++)
                {
                    for (int j = 0; j < neededItemsBuffer[index].Length; j++)
                    {
                        if (droneFactoryBuffer[index][i].storage.itemStoring == neededItemsBuffer[index][j].storage.itemStoring)
                        {
                            Storage s1 = droneFactoryBuffer[index][i].storage;
                            Storage s2 = neededItemsBuffer[index][j].storage;

                            if (s2.containsAmmount >= s1.maxContains)
                            {
                                validaton++;
                            }

                            break;
                        }
                    }
                }

                if (validaton == validatonTotal)
                {
                    for (int i = 0; i < droneFactoryBuffer[index].Length; i++)
                    {
                        for (int j = 0; j < neededItemsBuffer[index].Length; j++)
                        {
                            if (droneFactoryBuffer[index][i].storage.itemStoring == neededItemsBuffer[index][j].storage.itemStoring)
                            {
                                Storage s1 = droneFactoryBuffer[index][i].storage;
                                Storage s2 = neededItemsBuffer[index][j].storage;

                                s2.containsAmmount -= s1.maxContains;
                                s2.askForMore += s1.maxContains;

                                neededItemsBuffer[index].RemoveAt(j);
                                neededItemsBuffer[index].Insert(j, s2);

                                break;
                            }
                        }
                    }
                    //MAKE DRONES
                    df.working = 1;
                }
            }

            if (df.working == 1)
            {

            }

            df.currentTime += deltaTime;
            if (df.currentTime >= df.maxTime)
            {
                df.ammountToSpawn += df.toSpawnIncrement;
                df.working = 0;
                df.currentTime = 0;
            }

            droneFactory[index] = df;
        }
    }
}
