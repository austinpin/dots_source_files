﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;
using UnityEngine;

//[UpdateAfter(typeof(SBuildManager))]
[UpdateAfter(typeof(BPlaceBuilding))]
public class SBuildingZoneTest : JobComponentSystem
{
    public static JobHandle handle;

    struct _Beacons
    {
        public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<Build> build;
        [ReadOnly] public ComponentDataArray<Position> pos1;
    }

    struct _Unrevealed
    {
        public readonly int Length;
        //public EntityArray self;
        [ReadOnly] public ComponentDataArray<Zone> zone;
        [ReadOnly] public ComponentDataArray<Position> pos2;
    }

    struct _Users
    {
        [ReadOnly] public ComponentDataArray<UserUnlocks> pos2;
    }

    [Inject] _Beacons _beacons;
    [Inject] _Unrevealed _unrevealed;
    [Inject] _Users _users;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        BeaconJob job = new BeaconJob
        {
            build = _beacons.build,
            pos1 = _beacons.pos1,

            Length = _unrevealed.Length,
            zone = _unrevealed.zone,
            pos2 = _unrevealed.pos2,
        };
        handle = job.Schedule(_beacons.Length, 1, inputDeps);

        return handle;
    }

    [BurstCompile]
    struct BeaconJob : IJobParallelFor
    {
        //public EntityArray self;
        public ComponentDataArray<Build> build;
        [ReadOnly] public ComponentDataArray<Position> pos1;

        public int Length;
        //public EntityArray self;
        [ReadOnly] public ComponentDataArray<Zone> zone;
        [ReadOnly] public ComponentDataArray<Position> pos2;

        public void Execute(int index)
        {
            Build b = build[index];
            if (b.affinityDemand== RevealType.None)
            {
                b.spawnBuilding = 2;
            }
            for (int i = 0; i < Length; i++)
            {
                if (build[index].affinityDemand == zone[i].type)
                {
                    float dist = math.distance(pos1[index].Value, pos2[i].Value);
                    if (dist < zone[i].radius)
                    {
                        b.spawnBuilding = 2;
                        break;
                    }
                }
                if (build[index].affinityNormal == zone[i].type)
                {
                    float dist = math.distance(pos1[index].Value, pos2[i].Value);
                    if (dist < zone[i].radius)
                    {
                        b.boosted = 1;
                        break;
                    }
                }
            }
            build[index] = b;
        }
    }
}
