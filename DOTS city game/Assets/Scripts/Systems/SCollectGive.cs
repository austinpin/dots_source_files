﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;
using UnityEngine;

//[UpdateAfter(typeof(SBuildManager))]
//[UpdateAfter(typeof(STransferDrone))]
public class SCollectGive: ComponentSystem
{
    //public static JobHandle buildHandle;
    Unity.Mathematics.Random rand = new Unity.Mathematics.Random(327895732);

    struct _Collect
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<CollectNode> collect;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _BuildData
    {
        //public readonly int Length;
        //public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<BuildingData> buildData;
    }

    struct _UnitData
    {
        [ReadOnly] public SharedComponentDataArray<UnitData> unitData;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserEletricity> power;
        public ComponentDataArray<UserMoney> money;
        public ComponentDataArray<UserTransmitters> transmitters;
        public ComponentDataArray<UserLevetations> levetations;

        public ComponentDataArray<UserPhotite> photite;
        public ComponentDataArray<UserNanites> nanites;

        public ComponentDataArray<UserDurium> durium;
        public ComponentDataArray<UserZorium> zorium;

        public ComponentDataArray<UserVions> vions;
        public ComponentDataArray<UserNorn> norn;
    }

    [Inject] _User _user;
    [Inject] _UnitData _unitData;
    [Inject] _Collect _collect;
    [Inject] _BuildData _buildData;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        for (int i = 0; i < _collect.Length; i++)
        {
            CollectNode c = _collect.collect[i];

            if (c.ifGive == 1)
            {
                float3 myPos = _collect.pos[i].Value;

                /*
                if (c.give == Pickup.Drone)
                {
                    float3 randPos = rand.NextFloat3(new float3(1, 0, 1), new float3(-1, 0, -1));
                    randPos *= 3;
                    float3 myNewPos = randPos + myPos;

                    PostUpdateCommands.CreateEntity(Data.drone);
                    PostUpdateCommands.SetComponent(new Position { Value = myNewPos });
                    PostUpdateCommands.SetComponent(new Drone { nextJob = NextJobType.Idle, doChangeJob = 1 });
                    PostUpdateCommands.SetComponent(new Movement { speed = 14, seed = rand.NextUInt(), arrive = 1 });
                    PostUpdateCommands.SetSharedComponent(EntityManager.GetSharedComponentData<MeshInstanceRenderer>(_unitData.unitData[0].workerDrone));
                    c.ammount--;
                    c.ifGive = 0;
                }
                */

                if (c.give == Pickup.Energy)
                {
                    UserEletricity ue = _user.power[0];
                    ue.hasCurrent += c.ammount;
                    ue.hasMax += c.ammount;
                    c.ammount = 0;
                    _user.power[0] = ue;
                }

                if (c.give== Pickup.photite)
                {
                    UserPhotite up = _user.photite[0];
                    up.hasCurrent+=3;
                    c.ammount-=3;
                    _user.photite[0] = up;
                }

                if (c.ammount==0)
                {
                    PostUpdateCommands.DestroyEntity(_collect.self[i]);
                }

                c.ifGive = 0;

                _collect.collect[i] = c;
            }
        }
    }
}
