﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;
using TMPro;

[UpdateAfter(typeof(SDoPlayerAction))]
[UpdateAfter(typeof(SPlayerSelect))]
//[UpdateAfter(typeof(SPlayerSubAction))]
public class SUI : ComponentSystem
{

    struct _PlayerSelect
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<PlayerData> player;
        [ReadOnly] public ComponentDataArray<PlayerActionList> playerAction;
        public ComponentDataArray<PlayerInteractionMode> interact;
        public ComponentDataArray<PlayerSubInteractionMode> subInteract;
        public ComponentDataArray<PlayerBuild> build;
        public ComponentDataArray<GetOrStoreMode> getOrStoreMode;
        public ComponentDataArray<PlayerInfo> playerInfo;
        public ComponentDataArray<PlayerAffectSelected> affect;
        [ReadOnly] public SharedComponentDataArray<UIReferances> uiReferances;
        [ReadOnly] public SharedComponentDataArray<UIRes> uiRes;
        [ReadOnly] public SharedComponentDataArray<BuildingData> buldingData;
        [ReadOnly] public SharedComponentDataArray<ParticleObjects> particleObjects;
        [ReadOnly] public SharedComponentDataArray<UIButtons> buttons;
    }

    struct _Highlight
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<Highlight> highlight;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<UserEletricity> power;
        [ReadOnly] public ComponentDataArray<UserMoney> money;
        [ReadOnly] public ComponentDataArray<UserUnlocks> unlocks;
        public ComponentDataArray<UserQuests> quests;
        [ReadOnly] public ComponentDataArray<UserTransmitters> transmitters;
        [ReadOnly] public ComponentDataArray<UserLevetations> levetations;

        [ReadOnly] public ComponentDataArray<UserPhotite> photite;
        [ReadOnly] public ComponentDataArray<UserNanites> nanites;

        [ReadOnly] public ComponentDataArray<UserDurium> durium;
        [ReadOnly] public ComponentDataArray<UserZorium> zorium;

        [ReadOnly] public ComponentDataArray<UserVions> vions;
        [ReadOnly] public ComponentDataArray<UserNorn> norn;
    }

    [Inject] _Highlight _highlight;
    [Inject] _PlayerSelect _player;
    [Inject] _User _user;

    [ReadOnly] [Inject] BufferFromEntity<DroneBuffer> allDroneBuffer;
    [ReadOnly] [Inject] ComponentDataFromEntity<DroneBufferData> allDroneBufferData;

    private Entity lastFrameSelected;
    private PlayerInteractionModes interactionModes = PlayerInteractionModes.View;
    private float notificationTimer = 0;

    protected override void OnUpdate()
    {
        TextMeshProUGUI selectedText = _player.uiReferances[0].selectedText;

        TextMeshProUGUI notificationText = _player.uiReferances[0].notificationText;

        PlayerData d = _player.player[0];

        if (Input.GetButtonDown("Escape"))
        {
            if (_player.subInteract[0].playerSubInteractMode==PlayerSubInteractionModes.None)
            {
                PlayerBuild b = _player.build[0];
                b.toBuild = Building.Null;
                _player.build[0] = b;
            }
            else
            {
                PlayerSubInteractionMode i = _player.subInteract[0];
                i.playerSubInteractMode = PlayerSubInteractionModes.None;
                _player.subInteract[0] = i;
            }

        }

        Entity e = d.selected;
        Entity h = d.highlighted;
        if (e==lastFrameSelected && SDoPlayerAction.doUpdate==0)
        {
            //return;
        }

        //HIGHLIGHT SEGMENT
        if (EntityManager.Exists(h))
        {
            Position po = EntityManager.GetComponentData<Position>(h);

            if (_highlight.highlight[0].id==2)
            {
                _highlight.pos[0] = po;
            }
            else if (_highlight.highlight[1].id == 2)
            {
                _highlight.pos[1] = po;
            }
        }
        else
        {
            if (_highlight.highlight[0].id == 2)
            {
                _highlight.pos[0] = new Position { Value = new float3(0, -20, 0) };
            }
            else if (_highlight.highlight[1].id == 2)
            {
                _highlight.pos[1] = new Position { Value = new float3(0, -20, 0) };
            }
        }
        //END HIGHLIGHT SEGMENT

        //Other
        _player.uiRes[0].money.text = _user.money[0].current.ToString();

        _player.uiRes[0].hasPower.text = _user.power[0].allocationCurrent.ToString();
        _player.uiRes[0].allocatePower.text = _user.power[0].allocationCurrent.ToString();
        
        _player.uiRes[0].transmitters.text = _user.transmitters[0].current.ToString();
        _player.uiRes[0].levetations.text = _user.levetations[0].current.ToString();

        _player.uiRes[0].hasPhotite.text = _user.photite[0].hasCurrent.ToString();
        _player.uiRes[0].allocatePhotite.text = _user.photite[0].allocationCurrent.ToString();

        _player.uiRes[0].hasNanites.text = _user.nanites[0].hasCurrent.ToString();
        _player.uiRes[0].allocateNanites.text = _user.nanites[0].allocationCurrent.ToString();

        _player.uiRes[0].hasDurium.text = _user.durium[0].hasCurrent.ToString();
        _player.uiRes[0].allocateDurium.text = _user.durium[0].allocationCurrent.ToString();

        _player.uiRes[0].hasZorium.text = _user.zorium[0].hasCurrent.ToString();
        _player.uiRes[0].allocateZorium.text = _user.zorium[0].allocationCurrent.ToString();

        _player.uiRes[0].hasVions.text = _user.vions[0].hasCurrent.ToString();
        _player.uiRes[0].allocateVions.text = _user.vions[0].allocationCurrent.ToString();

        _player.uiRes[0].hasNorn.text = _user.norn[0].hasCurrent.ToString();
        _player.uiRes[0].allocateNorn.text = _user.norn[0].allocationCurrent.ToString();

        if (_user.unlocks[0].techTransmitterFactory==2)
        {
            _player.buttons[0].transmitterFactory.interactable = true;
        }

        //Debug.Log(_user.quests[0]._1VMLevel1);
        if (_user.quests[0].TotalVMLevel==7)
        {
            UserQuests uq = _user.quests[0];
            uq.TotalVMLevel = 8;
            _user.quests[0] = uq;
            _player.uiReferances[0].buttons.Win();
        }
        //Other

        //MAIN SELECTION SEGMENT
        if (EntityManager.Exists(e))
        {
            //check if it has a drone array

            PlayerAffectSelected pas = _player.affect[0];
            if (EntityManager.HasComponent<DroneFactory>(e))
            {
                DroneFactory df = EntityManager.GetComponentData<DroneFactory>(e);

                if (pas.howToAffect != HowToAffect.None)
                {
                    //df.curretTime = 0;
                    //df.ammountToSpawn = 0;

                    if (pas.howToAffect == HowToAffect.DroneFactory_WorkerDrone)
                    {
                        df.changeOrder = DroneToMake.WorkerDrone;
                    }
                    if (pas.howToAffect == HowToAffect.DroneFactory_transportDrone)
                    {
                        df.changeOrder = DroneToMake.TransportDrone;
                    }
                    if (pas.howToAffect == HowToAffect.DroneFactory_ScanDrone)
                    {
                        df.changeOrder = DroneToMake.ScanDrone;
                    }
                    EntityManager.SetComponentData(e, df);
                }

                _player.uiReferances[0].DFCurrentMake.text = df.droneTomake.ToString();
                _player.uiReferances[0].DFNextMake.text = df.changeOrder.ToString();
                _player.uiReferances[0].DFtimeCurrent.text = df.currentTime.ToString("0.0");
                _player.uiReferances[0].DFtimeMax.text = df.maxTime.ToString("0");
            }

            if (EntityManager.HasComponent<UnscannedNode>(e))
            {
                UnscannedNode un = EntityManager.GetComponentData<UnscannedNode>(e);
                if (pas.howToAffect == HowToAffect.Unscanned_ActivateScan)
                {
                    un.active = 1;
                }

                _player.uiReferances[0].unscannedCurrentTime.text = un.currentScanTime.ToString("0.0");
                _player.uiReferances[0].unscannedMaxTime.text = un.maxScanTime.ToString();
                _player.uiReferances[0].unscannedPower.text = un.powerNeeded.ToString();

                EntityManager.SetComponentData(e, un);
            }

            if (EntityManager.HasComponent<RevealTile>(e))
            {
                RevealTile t = EntityManager.GetComponentData<RevealTile>(e);
                if (pas.howToAffect == HowToAffect.Unscanned_ActivateScan && t.reveal==1)
                {
                    t.reveal = 2;
                }

                _player.uiReferances[0].unscannedPower.text = t.powerNeeded.ToString();

                EntityManager.SetComponentData(e, t);
            }
            pas.howToAffect = HowToAffect.None;
            _player.affect[0] = pas;

            //START INVENTORY SELECTION SET
            //check if it has an item inside it
            if (EntityManager.HasComponent<Salvage>(e))
            {
                /*
                _player.uiReferances[0].InvMenu.SetActive(true);
                Salvage s = EntityManager.GetComponentData<Salvage>(e);
                byte b = (byte)s.scrap;
                b -= 1;
                _player.invTextData[0].data[b].min.text = s.scrapAmmount.ToString();

                b = (byte)s.reward;
                b -= 1;
                _player.invTextData[0].data[b].min.text = s.rewardAmmount.ToString();
                */

            }
            else
            {
                _player.uiReferances[0].InvMenu.SetActive(false);
            }

            //END INVENTORY SELECTION SET

            //Move highlight object over it
            Position po = EntityManager.GetComponentData<Position>(e);

            if (_highlight.highlight[0].id == 1)
            {
                _highlight.pos[0] = po;
            }
            else if (_highlight.highlight[1].id == 1)
            {
                _highlight.pos[1] = po;
            }

            if (e != lastFrameSelected)
            {
                if (EntityManager.HasComponent<CollectNode>(e))
                {
                    CollectNode n = EntityManager.GetComponentData<CollectNode>(e);
                    n.ifGive = 1;
                    EntityManager.SetComponentData(e, n);
                    e = Entity.Null;
                }

                if (EntityManager.HasComponent<Upgradeable>(e))
                {
                    _player.uiReferances[0].primaryBG.SetActive(true);
                }
                else
                {
                    _player.uiReferances[0].primaryBG.SetActive(false);
                }

                selectedText.text = "Nothing Selected";
                //check what type it is
                if (EntityManager.HasComponent<MineField>(e))
                {
                    selectedText.text = "Mine field";
                }
                if (EntityManager.HasComponent<Salvage>(e))
                {
                    selectedText.text = "SalvageField";
                }
                if (EntityManager.HasComponent<Build>(e))
                {
                    selectedText.text = "Building site";
                }
                if (EntityManager.HasComponent<DroneFactory>(e))
                {
                    _player.uiReferances[0].DroneFactoryMenu.SetActive(true);
                    selectedText.text = "Drone Factory";
                }
                else
                {
                    _player.uiReferances[0].DroneFactoryMenu.SetActive(false);
                }

                if (EntityManager.HasComponent<TransportDrone>(e))
                {
                    selectedText.text = "Transport drone";
                    //_player.uiReferances[0].actionMenu.SetActive(true);
                }

                if (EntityManager.HasComponent<StorageTarget>(e))
                {
                    //selectedText.text = "Building site";
                }
                else
                {
                    //_player.uiReferances[0].actionMenu.SetActive(false);
                }

                if (EntityManager.HasComponent<TransportParking>(e))
                {
                    selectedText.text = "Transport parking";
                    //_player.uiReferances[0].InvMoveMenu.SetActive(true);
                }
                else
                {
                    _player.uiReferances[0].InvMoveMenu.SetActive(false);
                    //_player.uiReferances[0].actionMenu.SetActive(false);
                }

                if (EntityManager.HasComponent<UnscannedNode>(e))
                {
                    selectedText.text = "Unscanned object";
                    _player.uiReferances[0].UnscannedMenu.SetActive(true);
                }
                else if(EntityManager.HasComponent<RevealTile>(e))
                {
                    selectedText.text = "Unrevealed Area";
                    _player.uiReferances[0].UnscannedMenu.SetActive(true);
                }
                else
                {
                    _player.uiReferances[0].UnscannedMenu.SetActive(false);
                }
            }
        }
        else
        {
            if (_highlight.highlight[0].id == 1)
            {
                _highlight.pos[0] = new Position { Value = new float3(0, -20, 0) };
            }
            else if (_highlight.highlight[1].id == 1)
            {
                _highlight.pos[1] = new Position { Value = new float3(0, -20, 0) };
            }

            lastFrameSelected = Entity.Null;
            d.selected = Entity.Null;

            if (_player.uiReferances[0].UnscannedMenu.activeInHierarchy)
            {
                _player.uiReferances[0].UnscannedMenu.SetActive(false);
            }
            if (_player.uiReferances[0].InvMoveMenu.activeInHierarchy)
            {
                _player.uiReferances[0].InvMoveMenu.SetActive(false);
            }
        }
        //END MAIN SELECTION SEGMENT

        lastFrameSelected = e;

        /*
        //BUILD REQUIREMENTS AS INVNETORY SEGMENT
        //how what a building needs to be built when the info pannel is active, show the items on the inventory on the right
        DynamicBuffer<NeededItemsBuffer> buildRequirements = new DynamicBuffer<NeededItemsBuffer>();

        //Show data reqirements
        if (_player.playerInfo[0].showing == Info.B_TransportParking)
        {
            buildRequirements = EntityManager.GetBuffer<NeededItemsBuffer>(_player.buldingData[0].transportParking);
            _player.invTextData[0].data[0].max.text = "its free";
        }

        if (_player.playerInfo[0].showing == Info.B_Science)
        {
            buildRequirements = EntityManager.GetBuffer<NeededItemsBuffer>(_player.buldingData[0].science);
        }

        if (_player.playerInfo[0].showing == Info.B_DroneFactory)
        {
            buildRequirements = EntityManager.GetBuffer<NeededItemsBuffer>(_player.buldingData[0].droneFactory);
        }
        if (_player.playerInfo[0].showing == Info.B_VictoryMonument)
        {
            buildRequirements = EntityManager.GetBuffer<NeededItemsBuffer>(_player.buldingData[0].victoryMonument);
        }

        if (_player.playerInfo[0].showing != Info.None)
        {
            for (int i = 0; i < buildRequirements.Length; i++)
            {
                Storage s = buildRequirements[i].storage;
                byte n = (byte)s.itemStoring;
                n -= 1;
                _player.invTextData[0].data[n].min.text = s.maxContains.ToString();
            }
        }
        //END BUILD REQUIREMENTS AS INVENTORY SEGMENT
        */

        if (_player.build[0].toBuild == Building.Null)
        {
            _player.uiReferances[0].subBuildMenu.SetActive(true);
            _player.uiReferances[0].viewMenu.SetActive(true);
        }
        else
        {
            notificationText.text = "Press escape to exit build mode, left click to place";

            _player.uiReferances[0].subBuildMenu.SetActive(false);
            _player.uiReferances[0].viewMenu.SetActive(false);
        }

        //INTERTACTION MODE UI PANNEL
        if (_player.interact[0].playerInteractMode != interactionModes)
        {
            if (_player.interact[0].playerInteractMode == PlayerInteractionModes.View)
            {
                e = Entity.Null;
                _player.uiReferances[0].buildMenu.SetActive(false);
                _player.uiReferances[0].InvMoveMenu.SetActive(false);
                _player.uiReferances[0].InvMenu.SetActive(false);
                _player.uiReferances[0].TradeMenu.SetActive(false);
                _player.uiReferances[0].primaryBG.SetActive(false);
                _player.uiReferances[0].ReadvMenu.SetActive(false);
                //override entity seletion, allow other script to change interaction mode
            }

            if (_player.interact[0].playerInteractMode == PlayerInteractionModes.Build)
            {
                e = Entity.Null;
                _player.uiReferances[0].InvMoveMenu.SetActive(false);
                _player.uiReferances[0].InvMenu.SetActive(false);
                _player.uiReferances[0].TradeMenu.SetActive(false);
                _player.uiReferances[0].primaryBG.SetActive(false);
                _player.uiReferances[0].ReadvMenu.SetActive(false);

                _player.uiReferances[0].buildMenu.SetActive(true);
            }
            else
            {
                _player.uiReferances[0].buildMenu.SetActive(false);
            }

            if (_player.interact[0].playerInteractMode == PlayerInteractionModes.Info)
            {
                e = Entity.Null;
                //_player.uiReferances[0].buildMenu.SetActive();
                _player.uiReferances[0].InvMoveMenu.SetActive(false);
                _player.uiReferances[0].InvMenu.SetActive(true);
                _player.uiReferances[0].TradeMenu.SetActive(false);
                _player.uiReferances[0].primaryBG.SetActive(false);
                _player.uiReferances[0].ReadvMenu.SetActive(false);

                _player.uiReferances[0].Info.SetActive(true);
            }
            else
            {
                _player.uiReferances[0].Info.SetActive(false);

                PlayerInfo inf = _player.playerInfo[0];
                inf.showing = Info.None;
                _player.playerInfo[0] = inf;
            }

            if (_player.interact[0].playerInteractMode == PlayerInteractionModes.Selected)
            {
                _player.uiReferances[0].buildMenu.SetActive(false);
                _player.uiReferances[0].TradeMenu.SetActive(false);
                _player.uiReferances[0].ReadvMenu.SetActive(false);
                //override entity seletion, allow other script to change interaction mode
            }
            else
            {
                _player.uiReferances[0].DroneFactoryMenu.SetActive(false);
                lastFrameSelected = Entity.Null;
                d.selected = Entity.Null;
                d.subSelected = Entity.Null;

                _highlight.pos[0] = new Position { Value = new float3(0, -20, 0) };
                _player.subInteract[0] = new PlayerSubInteractionMode { playerSubInteractMode = PlayerSubInteractionModes.None};
            }

            if (_player.interact[0].playerInteractMode == PlayerInteractionModes.Trade)
            {
                e = Entity.Null;
                _player.uiReferances[0].InvMoveMenu.SetActive(false);
                _player.uiReferances[0].InvMenu.SetActive(false);
                _player.uiReferances[0].buildMenu.SetActive(false);
                _player.uiReferances[0].TradeMenu.SetActive(true);
                _player.uiReferances[0].primaryBG.SetActive(false);
                _player.uiReferances[0].ReadvMenu.SetActive(false);
            }

            if (_player.interact[0].playerInteractMode == PlayerInteractionModes.Readvancement)
            {
                if (_user.unlocks[0].unlockReadvancements==1)
                {
                    e = Entity.Null;
                    _player.uiReferances[0].InvMoveMenu.SetActive(false);
                    _player.uiReferances[0].InvMenu.SetActive(false);
                    _player.uiReferances[0].buildMenu.SetActive(false);
                    _player.uiReferances[0].TradeMenu.SetActive(false);
                    _player.uiReferances[0].primaryBG.SetActive(false);
                    _player.uiReferances[0].ReadvMenu.SetActive(true);
                }
                else
                {
                    PlayerInteractionMode pim = _player.interact[0];
                    pim.playerInteractMode = PlayerInteractionModes.View;
                    _player.interact[0] = pim;
                }
            }
        }
        //END INTERTACTION MODE UI PANNEL

        if (_player.subInteract[0].playerSubInteractMode == PlayerSubInteractionModes.AssignOrder)
        {
            notificationText.text = "Currently assigning";
            notificationTimer = 0.1f;

            _player.particleObjects[0].effect1.SetActive(true);
        }
        else
        {
            _player.particleObjects[0].effect1.SetActive(false);
        }

        //BEGIN SUBSELECTION SEGMENT
        //used by assign UI button to link entities
        if (_player.interact[0].playerInteractMode == PlayerInteractionModes.Selected)
        {
            //subselected entity
            Entity t = d.subSelected;

            if (EntityManager.Exists(t))
            {
                if (_player.subInteract[0].playerSubInteractMode == PlayerSubInteractionModes.AssignOrder)
                {
                    //Code block to assign the storage or parking to transport drones
                    if (EntityManager.HasComponent<TransportDrone>(e))
                    {
                        /*
                        Movement m = EntityManager.GetComponentData<Movement>(e);
                        
                        if (EntityManager.HasComponent<TransportParking>(t))
                        {
                            notificationText.text = "assigned to transport parking sucessfully";
                            notificationTimer = 3;

                            TransportDrone td = EntityManager.GetComponentData<TransportDrone>(e);
                            TransportParking p = EntityManager.GetComponentData<TransportParking>(t);

                            td.StoreOrGet = p.StoreOrGet;

                            if (m.target == td.ParkingTo)
                            {
                                m.target = t;
                                m.arrive = 0;
                                //td.switchParked = 1;
                            }

                            td.ParkingTo = t;
                            EntityManager.SetComponentData(e, td);

                            _player.subInteract[0] = new PlayerSubInteractionMode { playerSubInteractMode = PlayerSubInteractionModes.None };
                            d.subSelected = Entity.Null;
                        }
                        
                        if (EntityManager.HasComponent<StorageBuffer>(t))
                        {
                            notificationText.text = "assigned to storagebuffer sucessfully";
                            notificationTimer = 3;

                            TransportDrone td = EntityManager.GetComponentData<TransportDrone>(e);

                            if (m.target == td.storageHome)
                            {
                                m.target = t;
                                m.arrive = 0;
                                td.switchParked = 1;
                            }

                            td.storageHome = t;
                            EntityManager.SetComponentData(e, td);

                            _player.subInteract[0] = new PlayerSubInteractionMode { playerSubInteractMode = PlayerSubInteractionModes.None };
                            d.subSelected = Entity.Null;
                        }
                        EntityManager.SetComponentData(e, m);
                        */
                    }

                    //code block to assign parking to buildings
                    if (EntityManager.HasComponent<StorageTarget>(e))
                    {
                        if (EntityManager.HasComponent<TransportParking>(t))
                        {
                            notificationText.text = "assigned to transport parking sucessfully";
                            notificationTimer = 3;

                            StorageTarget st = EntityManager.GetComponentData<StorageTarget>(e);
                            st.storageTarget = t;
                            EntityManager.SetComponentData(e, st);

                            if (EntityManager.HasComponent<Status>(e))
                            {
                                Status b = EntityManager.GetComponentData<Status>(e);
                                b.active = 1;
                                EntityManager.SetComponentData(e, b);
                            }

                            TransportParking p = EntityManager.GetComponentData<TransportParking>(t);
                            p.buildTarget = e;
                            p.active = 1;

                            if (EntityManager.HasComponent<NeededItemsBuffer>(e))
                            {
                                p.StoreOrGet = 2;
                                DynamicBuffer<NeededItemsBuffer> nib = EntityManager.GetBuffer<NeededItemsBuffer>(e); //selected entity wants these items
                                DynamicBuffer<TPNeededItemsBuffer> nib2 = EntityManager.GetBuffer<TPNeededItemsBuffer>(t); //transport parking should also want those items

                                //add the items together to allow grabbing of multiple items

                                for (int m = 0; m < nib.Length; m++)
                                {
                                    bool valid = false;
                                    for (int n = 0; n < nib2.Length; n++)
                                    {
                                        Storage s = nib2[n];

                                        s.maxContains += nib[m].storage.maxContains;

                                        nib2.RemoveAt(n);
                                        nib2.Insert(n, s);

                                        valid = true;
                                    }

                                    if (valid == false)
                                    {
                                        nib2.Add(nib[m].storage);
                                    }
                                }
                            }
                            if (EntityManager.HasComponent<MineField>(e) || EntityManager.HasComponent<Salvage>(e))
                            {
                                p.StoreOrGet = 1;
                            }

                            EntityManager.SetComponentData(t, p);

                            _player.subInteract[0] = new PlayerSubInteractionMode { playerSubInteractMode = PlayerSubInteractionModes.None };
                            d.subSelected = Entity.Null;
                        }
                    }
                    if (EntityManager.HasComponent<TransportParking>(e))
                    {
                        if (EntityManager.HasComponent<StorageBuffer>(t))
                        {
                            notificationText.text = "assigned to warehouse sucessfully";
                            notificationTimer = 3;

                            TransportParking p = EntityManager.GetComponentData<TransportParking>(e);
                            p.storageTarget = t;
                            p.active = 1;
                            EntityManager.SetComponentData(e, p);

                            _player.subInteract[0] = new PlayerSubInteractionMode { playerSubInteractMode = PlayerSubInteractionModes.None };
                            d.subSelected = Entity.Null;
                        }
                    }
                }
            }
        }
        //END SUBSELECTION SEGEMENT

        notificationTimer -= Time.deltaTime;
        if (notificationTimer<=0)
        {
            notificationTimer = 0;
            notificationText.text = "";
        }

        d.selected = e;
        _player.player[0] = d;
        interactionModes = _player.interact[0].playerInteractMode;
    }
}
