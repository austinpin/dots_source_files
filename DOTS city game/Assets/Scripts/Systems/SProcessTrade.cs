﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class SProcessTrade : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Unlocks
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserUnlocks> unlock;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserMoney> money;
    }

    [Inject] _User _user;
    [Inject] _Unlocks _unlocks;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            money = _user.money,

            Length = _unlocks.Length,
            salvageSelf = _unlocks.self,
            unlock = _unlocks.unlock,
        };
        arrivalHandle = job.Schedule(_user.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public ComponentDataArray<UserMoney> money;

        public int Length;
        public EntityArray salvageSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<UserUnlocks> unlock;

        public void Execute(int index)
        {
            UserMoney um = money[index];
            UserUnlocks uu = unlock[0];

            /*
            if (uu.techTransmitterFactory==1)
            {
                if (um.current>=10)
                {
                    um.current -= 10;
                    uu.techTransmitterFactory = 2;
                }
                else
                {
                    uu.techTransmitterFactory = 0;
                }
            }

            if (uu.terraPort==1)
            {
                if (um.current >= 15)
                {
                    um.current -= 15;
                    uu.terraPort = 2;
                }
                else
                {
                    uu.terraPort = 0;
                }
            }
            */

            money[index] = um;
            unlock[0] = uu;
        }
    }
}
