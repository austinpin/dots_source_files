﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;
using TMPro;

[UpdateAfter(typeof(SDoPlayerAction))]
[UpdateAfter(typeof(SPlayerSelect))]
public class SProcessReadvancement : ComponentSystem
{

    struct _PlayerSelect
    {
        [ReadOnly] public SharedComponentDataArray<UIRes> uiRes;
        [ReadOnly] public SharedComponentDataArray<UIReadv> uiReadv;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserQuests> quests;
        public ComponentDataArray<UserUnlocks> unlocks;
        public ComponentDataArray<UserPickReadv> pick;
        public ComponentDataArray<UserReadvancementPoints> points;
        public ComponentDataArray<UserPrefabs> prefabs;
    }

    [Inject] _PlayerSelect _player;
    [Inject] _User _user;

    protected override void OnUpdate()
    {
        string title = "";
        string desctiption = "";

        UserPickReadv pick = _user.pick[0];
        UserReadvancementPoints points = _user.points[0];
        UserUnlocks unlocks = _user.unlocks[0];
        UserPrefabs uPre = _user.prefabs[0];
        UserQuests uq = _user.quests[0];

        if (pick.Clicked== Readvancements.TransmitterFactoryBP1)
        {
            title = "Unlock Transmitter Factory";
            desctiption = "You need the transmitter factory to expand your influence range";

            if (pick.tryBuy==1 && unlocks.techTransmitterFactory==0)
            {
                pick.tryBuy = 0;
                if (points.points>=1)
                {
                    points.points -= 1;
                    unlocks.techTransmitterFactory = 1;
                    uPre.transmitterFactories++;
                    _player.uiReadv[0].TransmitterFactoryTech.enabled = true;
                    uq._transmitterFactoryTech = 1;
                }
            }
        }
        /*
        if (pick.Clicked == Readvancements.M_GrandBeacon)
        {
            title = "Grand Beacon";
            desctiption = "monument beacon with extreamly large range, 20+X";
            
            if (pick.tryBuy == 1 && unlocks.unlockGrandBeacon == 0)
            {
                pick.tryBuy = 0;
                if (points.points >= 2)
                {
                    points.points -= 2;
                    unlocks.unlockGrandBeacon = 1;
                    uPre.GrandBeacon += 1;
                    _player.uiReadv[0].GrandBeaconTech.enabled = true;
                }
            }
            
        }
        */

        if (pick.Clicked == Readvancements.WindTurbines)
        {
            title = "Wind turbines";
            desctiption = "Makes 3 base power, build on brown elevated zones";

            if (pick.tryBuy == 1 && unlocks.unlockWindTurbine == 0)
            {
                pick.tryBuy = 0;
                if (points.points >= 2)
                {
                    points.points -= 2;
                    unlocks.unlockWindTurbine = 1;
                    _player.uiReadv[0].WindTurbineTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.GeothermalGenerator)
        {
            title = "Geothermal Generator";
            desctiption = "Makes 6 base power, build on brown elevated zones";

            if (pick.tryBuy == 1 && unlocks.unlockGeothermalGenerator == 0 && unlocks.unlockWindTurbine==1)
            {
                pick.tryBuy = 0;
                if (points.points >= 3)
                {
                    points.points -= 3;
                    unlocks.unlockGeothermalGenerator = 1;
                    _player.uiReadv[0].GeothermalTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.FissionGenerator)
        {
            title = "Fission Generator";
            desctiption = "Makes 12 base power, build on brown elevated zones";

            if (pick.tryBuy == 1 && unlocks.unlockFissionGenerator == 0 && unlocks.unlockGeothermalGenerator==1)
            {
                pick.tryBuy = 0;
                if (points.points >= 5)
                {
                    points.points -= 5;
                    unlocks.unlockFissionGenerator = 1;
                    _player.uiReadv[0].FissionGeneratorTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.NaniteMaker)
        {
            title = "Nanite maker";
            desctiption = "Nanite resource set, build on photite zones";

            if (pick.tryBuy == 1 && unlocks.unlockNaniteMaker == 0)
            {
                pick.tryBuy = 0;
                if (points.points >= 2)
                {
                    points.points -= 2;
                    unlocks.unlockNaniteMaker = 1;
                    _player.uiReadv[0].NaniteMakerTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.DuriumMine)
        {
            title = "Nanite maker";
            desctiption = "durium resource set, build on durium zone";

            if (pick.tryBuy == 1 && unlocks.unlockDuriumMine == 0 && unlocks.unlockNaniteMaker==1)
            {
                pick.tryBuy = 0;
                if (points.points >= 3)
                {
                    points.points -= 3;
                    unlocks.unlockDuriumMine = 1;
                    _player.uiReadv[0].DuriumMineTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.ZoriumMaker)
        {
            title = "Nanite maker";
            desctiption = "zorium resource set, build on durium zone";

            if (pick.tryBuy == 1 && unlocks.unlockZoriumMaker == 0 && unlocks.unlockDuriumMine==1)
            {
                pick.tryBuy = 0;
                if (points.points >= 4)
                {
                    points.points -= 4;
                    unlocks.unlockZoriumMaker = 1;
                    _player.uiReadv[0].ZoriumMalerTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.VionsMine)
        {
            title = "Nanite maker";
            desctiption = "vions resource set, build on vions zone";

            if (pick.tryBuy == 1 && unlocks.unlockVionsMine == 0 && unlocks.unlockZoriumMaker==1)
            {
                pick.tryBuy = 0;
                if (points.points >= 5)
                {
                    points.points -= 5;
                    unlocks.unlockVionsMine = 1;
                    _player.uiReadv[0].VionsMineTech.enabled = true;
                }
            }
        }

        if (pick.Clicked == Readvancements.NornMaker)
        {
            title = "Nanite maker";
            desctiption = "Norn resource set, build on vions zone";

            if (pick.tryBuy == 1 && unlocks.unlockNornMaker == 0 && unlocks.unlockVionsMine==1)
            {
                pick.tryBuy = 0;
                if (points.points >= 6)
                {
                    points.points -= 6;
                    unlocks.unlockNornMaker= 1;
                    _player.uiReadv[0].NornMakerTech.enabled = true;
                }
            }
        }

        _user.pick[0] = pick;
        _user.points[0] = points;
        _user.unlocks[0] = unlocks;
        _user.prefabs[0] = uPre;
        _user.quests[0] = uq;

        //print text
        _player.uiRes[0].title.text = title;
        _player.uiRes[0].description.text = desctiption;
        _player.uiReadv[0].pointsText.text = _user.points[0].points.ToString();
        _player.uiReadv[0].pointsText2.text = _user.points[0].points.ToString();
    }
}
