﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;
using UnityEngine;

//[UpdateAfter(typeof(SBuildManager))]
//[UpdateAfter(typeof(STransferDrone))]
public class SBeaconRevealArea : JobComponentSystem
{
    public static JobHandle handle;

    struct _Beacons
    {
        public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<Beacon> beacon;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<Position> pos1;
    }

    struct _Unrevealed
    {
        public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<RevealTile> reveal;
        [ReadOnly] public ComponentDataArray<Position> pos2;
    }

    struct _User
    {
        public readonly int Length;
        //public EntityArray self;
         public ComponentDataArray<UserTransmitters> ut;
    }

    struct _Player
    {
        //public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<PlayerOther> other;
    }

    [Inject] _Beacons _beacons;
    [Inject] _Unrevealed _unrevealed;
    [Inject] _User _user;
    [Inject] _Player _player;

    struct _Time
    {
        [ReadOnly] public ComponentDataArray<GD_Time> time;
    }

    [Inject] _Time _time;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        BeaconJob job = new BeaconJob
        {
            beacon = _beacons.beacon,
            status = _beacons.status,
            pos1 = _beacons.pos1,

            Length = _unrevealed.Length,
            reveal = _unrevealed.reveal,
            pos2 = _unrevealed.pos2,

            ut = _user.ut,

            other = _player.other,

            deltaTime = Time.deltaTime * _time.time[0].timeSpeed
        };
        handle = job.Schedule(_beacons.Length, 1, inputDeps);

        return handle;
    }

    [BurstCompile]
    struct BeaconJob : IJobParallelFor
    {
        //public int Length;
        //public EntityArray self;
        public ComponentDataArray<Beacon> beacon;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<Position> pos1;

        public int Length;
        //public EntityArray self;
        [NativeDisableParallelForRestriction] public ComponentDataArray<RevealTile> reveal;
        [ReadOnly] public ComponentDataArray<Position> pos2;

        public ComponentDataArray<UserTransmitters> ut;

        public ComponentDataArray<PlayerOther> other;

        public float deltaTime;

        public void Execute(int index)
        {
            Beacon b = beacon[index];
            Status s = status[index];
            UserTransmitters ut2 = ut[0];
            PlayerOther po = other[0];

            ut2.updateTime += deltaTime;

            if (ut2.updateTime>8)
            {
                ut2.updateTime -= 8;
                s.radius += ut[0].power * 8;
                po.updateReveal = 1;
                s.reset = 1;
            }

            if (s.reset==1)
            {
                s.reset = 0;
                b.activated = 0;
            }
            if (beacon[index].activated==0)
            {
                float radius = s.radius;
                if (s.boosted==1)
                {
                    radius *= 2;
                }

                if (s.level>0)
                {
                    for (int i = 0; i < Length; i++)
                    {
                        float dist = math.distance(pos1[index].Value, pos2[i].Value);
                        if (dist < radius)
                        {
                            RevealTile t = reveal[i];
                            t.reveal = 2;
                            reveal[i] = t;
                        }
                    }
                }
                b.activated = 1;
                
            }
            beacon[index] = b;
            status[index] = s;
            ut[0] = ut2;
            other[0] = po;
        }
    }
}
