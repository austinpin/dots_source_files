﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;
using TMPro;

[UpdateAfter(typeof(SDoPlayerAction))]
[UpdateAfter(typeof(SPlayerSelect))]
//[UpdateAfter(typeof(SPlayerSubAction))]
public class SQuest : ComponentSystem
{

    struct _PlayerSelect
    {
        [ReadOnly] public SharedComponentDataArray<UIRes> uiRes;
        [ReadOnly] public ComponentDataArray<PlayerData> data;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<UserQuests> quests;
    }

    [Inject] _PlayerSelect _player;
    [Inject] _User _user;

    protected override void OnUpdate()
    {
        //Quest
        string mainx = "";
        string side1 = "";
        string side2 = "";

        if (_user.quests[0].placedStart == 0)
        {
            mainx = "Choose a location to spawn";
        }
        else
        {
            if (_user.quests[0]._UpgradeGrandBeacon == 0)
            {
                mainx = "You cant see much, you will need to upgrade your grand beacon to continue,";
                side1 = "It is the tall thin structure next to your base";
            }
            if (_user.quests[0]._1photiteMinerLevel1 == 0)
            {
                side2 = "right click the blue glowing objects to collect photite, they are finite";
            }
        }

        if (_user.quests[0]._UpgradeGrandBeacon==1)
        {
            if (_user.quests[0]._1baseLevel1 == 0)
            {
                mainx = "Before you can upgrade your base, follow the below quests";
            }
            if (_user.quests[0]._1irradiatedGeneratorLevel1 == 0)
            {
                side1 = "First, build the irradiated generator to gain power.";
            }
            if (_user.quests[0]._1photiteMinerLevel1 == 0)
            {
                side2 = "Second, build the photite mine to gain photite";
            }
        }

        if (_user.quests[0]._1baseLevel1 == 1)
        {
            if (_user.quests[0]._transmitterFactoryTech == 0)
            {
                mainx = "Your beacon needs more range, the transmitter factory increses its range over time";
                side1 = "Unlock the transmitter factory tech from readvancements";
            }
            if (_user.quests[0]._1readvancementCentreLevel1 == 0)
            {
                side2 = "build menu > monuments, readvancement centre";
            }
        }

        if (_user.quests[0]._transmitterFactoryTech == 1)
        {
            if (_user.quests[0]._1transmitterFactoryLevel1 == 0)
            {
                mainx = "You need to actually build it construct a transmitter factory";
                side1 = "Remember it will increse your vision over time";
                side2 = "TAKE NOTE: you can only use 1, as you upgrade it the beacon will expand faster";
            }
        }

        //print text
        _player.uiRes[0].objMain.text = mainx;
        _player.uiRes[0].obj1.text = side1;
        _player.uiRes[0].obj2.text = side2;

        _player.uiRes[0].VMLevelText.text = _user.quests[0].TotalVMLevel.ToString();
        //Quest

        _player.uiRes[0].allocate1.text = "";
        _player.uiRes[0].has1.text = "";
        _player.uiRes[0].Icon1.sprite = null;

        _player.uiRes[0].allocate2.text = "";
        _player.uiRes[0].has2.text = "";
        _player.uiRes[0].Icon2.sprite = null;

        _player.uiRes[0].allocate3.text = "";
        _player.uiRes[0].has3.text = "";
        _player.uiRes[0].Icon3.sprite = null;

        _player.uiRes[0].comp1.text = "";
        _player.uiRes[0].Icon4.sprite = null;

        _player.uiRes[0].power1.text = "";
        _player.uiRes[0].descriptionText.text = "";

        //Display numbers and data for every single upgrade
        Entity e = _player.data[0].selected;
        if (EntityManager.HasComponent<Upgradeable>(e))
        {
            Status s = EntityManager.GetComponentData<Status>(e);
            _player.uiRes[0].levelText.text = s.level.ToString();
            DynamicBuffer<Upgradeable> du = EntityManager.GetBuffer<Upgradeable>(e);

            if (s.iAm == Building.Base)
            {
                _player.uiRes[0].name.text = "Base";
            }

            if (s.iAm == Building.IrradiatedGenerator)
            {
                _player.uiRes[0].name.text = "Irradiated Generator";
            }

            if (s.iAm == Building.WindPowerPlant)
            {
                _player.uiRes[0].name.text = "Wind Power Plant";
            }

            if (s.iAm == Building.GeothermalGenerator)
            {
                _player.uiRes[0].name.text = "Geothermal Generator";
            }

            if (s.iAm == Building.FissionGenerator)
            {
                _player.uiRes[0].name.text = "Fission generator";
            }

            if (s.iAm == Building.Beacon)
            {
                _player.uiRes[0].name.text = "Beacon";
            }

            if (s.iAm == Building.M_GrandBeacon)
            {
                _player.uiRes[0].name.text = "Grand Beacon";
            }

            if (s.iAm == Building.PhotiteMiner)
            {
                _player.uiRes[0].name.text = "Photite Miner";
            }

            if (s.iAm == Building.NaniteMaker)
            {
                _player.uiRes[0].name.text = "Nanite Maker";
            }

            if (s.iAm == Building.DuriumMiner)
            {
                _player.uiRes[0].name.text = "Durium Miner";
            }

            if (s.iAm == Building.ZoriumMaker)
            {
                _player.uiRes[0].name.text = "Zorium Maker";
            }

            if (s.iAm == Building.VionMiner)
            {
                _player.uiRes[0].name.text = "Vion Miner";
            }

            if (s.iAm == Building.NornMaker)
            {
                _player.uiRes[0].name.text = "Norn Maker";
            }

            if (s.iAm == Building.TransmitterFactory)
            {
                _player.uiRes[0].name.text = "Transmitter Factory";
            }

            if (s.iAm == Building.TechCentre)
            {
                _player.uiRes[0].name.text = "Tech Centre";
            }

            if (s.iAm == Building.M_ReadvancementCentre)
            {
                _player.uiRes[0].name.text = "Readvancement Centre";
            }

            if (s.iAm == Building.VictoryMonument)
            {
                _player.uiRes[0].name.text = "Victory Monument";
            }

            if (s.level + 1 > du.Length - 1)
            {
                //index too big
            }
            else
            {
                Upgradeable u = du[s.level + 1];

                if (s.iAm == Building.Base)
                {
                    _player.uiRes[0].name.text = "Base";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "This upgrade allows you to build the monument readvancement centre";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                        //_player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                    }
                    if (s.level == 1)
                    {
                        _player.uiRes[0].descriptionText.text = "2 Research points";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                }

                if (s.iAm == Building.IrradiatedGenerator)
                {
                    _player.uiRes[0].name.text = "Irradiated Generator";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Generates 1 base power";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                }

                if (s.iAm == Building.WindPowerPlant)
                {
                    _player.uiRes[0].name.text = "Wind Power Plant";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Generates 3 base power";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                }

                if (s.iAm == Building.WindPowerPlant)
                {
                    _player.uiRes[0].name.text = "Wind Power Plant";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Generates 5 base power";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                }

                if (s.iAm == Building.GeothermalGenerator)
                {
                    _player.uiRes[0].name.text = "Geothermal generator";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Generates 11 base power";
                        _player.uiRes[0].has1.text = u.itemsNeeded.duriumHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Durium;
                    }
                }

                if (s.iAm == Building.FissionGenerator)
                {
                    _player.uiRes[0].name.text = "Fission generator";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Generates 17 base power";
                        _player.uiRes[0].has1.text = u.itemsNeeded.vionsHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Vions;
                    }
                }

                if (s.iAm == Building.Beacon)
                {
                    _player.uiRes[0].name.text = "Beacon";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Reveals the surrounding area and any zones underneath";
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                }

                if (s.iAm == Building.M_GrandBeacon)
                {
                    _player.uiRes[0].name.text = "Grand Beacon";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Reveals the surrounding area and any zones underneath";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 1)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 200 range. starts at 100";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 2)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 300 range";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 3)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 400 range";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.nanitesHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Nanite;
                    }

                    if (s.level == 4)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 500 range";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.duriumHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Durium;
                    }

                    if (s.level == 5)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 600 range";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.zoriumHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Zorium;
                    }

                    if (s.level == 6)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 700 range";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.vionsHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Vions;
                    }

                    if (s.level == 7)
                    {
                        _player.uiRes[0].descriptionText.text = "upgrade to add 800 range";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.nornAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Norn;
                    }
                }

                if (s.iAm == Building.PhotiteMiner)
                {
                    _player.uiRes[0].name.text = "Photite Miner";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Gives 4 allocated photite when upgraded and slowly gives accumulated photite as well";
                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }

                if (s.iAm == Building.NaniteMaker)
                {
                    _player.uiRes[0].name.text = "Nanite Maker";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Primary source of Nanites, produced directly a the source";

                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;

                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }

                if (s.iAm == Building.DuriumMiner)
                {
                    _player.uiRes[0].name.text = "Durium Miner";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Primary source of durium";

                        _player.uiRes[0].has1.text = u.itemsNeeded.nanitesHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Nanite;

                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }

                if (s.iAm == Building.ZoriumMaker)
                {
                    _player.uiRes[0].name.text = "Zorium Maker";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Primary source of Zorium, produced directly a the source";

                        _player.uiRes[0].has1.text = u.itemsNeeded.duriumHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Durium;

                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }

                if (s.iAm == Building.VionMiner)
                {
                    _player.uiRes[0].name.text = "Vions Miner";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "primary source of Vions,";

                        _player.uiRes[0].has1.text = u.itemsNeeded.zoriumHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Zorium;

                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }

                if (s.iAm == Building.NornMaker)
                {
                    _player.uiRes[0].name.text = "Norn Maker";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Primary source of Norn, produced directly a the source";

                        _player.uiRes[0].has1.text = u.itemsNeeded.vionsHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Vions;

                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }

                if (s.iAm == Building.TransmitterFactory)
                {
                    _player.uiRes[0].name.text = "Transmitter Factory";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "gradually increases beacon vision,";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 1)
                    {
                        _player.uiRes[0].descriptionText.text = "gradually increases beacon vision, faster";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        //_player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 2)
                    {
                        _player.uiRes[0].descriptionText.text = "gradually increases beacon vision, faster";
                        _player.uiRes[0].has1.text = u.itemsNeeded.nanitesHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Nanite;
                    }
                }

                if (s.iAm == Building.TechCentre)
                {
                    _player.uiRes[0].name.text = "Tech Centre";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Grants 1 research point";
                        //_player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;

                        _player.uiRes[0].has2.text = u.itemsNeeded.nanitesHas.ToString();
                        _player.uiRes[0].Icon2.sprite = _player.uiRes[0].Nanite;
                    }
                }

                if (s.iAm == Building.M_ReadvancementCentre)
                {
                    _player.uiRes[0].name.text = "Readvancement Centre";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Gives 1 Tech point .Unlocks readvancements for the first time, reccomended to get the transmitter factory tech first costing 1 TP. ";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level==1)
                    {
                        _player.uiRes[0].descriptionText.text = "Gives 2 Tech points. ";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level==2)
                    {
                        _player.uiRes[0].descriptionText.text = "Gives 4 Tech points. ";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.nanitesAllo.ToString();
                        //_player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Nanite;
                    }
                    if (s.level == 3)
                    {
                        _player.uiRes[0].descriptionText.text = "Gives 6 Tech points. ";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.duriumAllo.ToString();
                        //_player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Durium;
                    }
                    if (s.level == 4)
                    {
                        _player.uiRes[0].descriptionText.text = "Gives 8 Tech points. ";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.zoriumAllo.ToString();
                        //_player.uiRes[0].has1.text = u.itemsNeeded.photiteHas.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Zorium;
                    }
                }

                if (s.iAm == Building.VictoryMonument)
                {
                    _player.uiRes[0].name.text = "Victory Monument";
                    if (s.level == 0)
                    {
                        _player.uiRes[0].descriptionText.text = "Needed to win";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 1)
                    {
                        _player.uiRes[0].descriptionText.text = "6 levels to win";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.photiteAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Photite;
                    }
                    if (s.level == 2)
                    {
                        _player.uiRes[0].descriptionText.text = "5 levels to win";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.duriumAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Durium;
                    }
                    if (s.level == 3)
                    {
                        _player.uiRes[0].descriptionText.text = "4 levels to win";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.zoriumAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Zorium;
                    }
                    if (s.level == 4)
                    {
                        _player.uiRes[0].descriptionText.text = "3 levels to win";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.vionsAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Vions;
                    }
                    if (s.level == 5)
                    {
                        _player.uiRes[0].descriptionText.text = "2 levels to win";
                        _player.uiRes[0].allocate1.text = u.itemsNeeded.nornAllo.ToString();
                        _player.uiRes[0].Icon1.sprite = _player.uiRes[0].Norn;
                    }
                    if (s.level == 6)
                    {
                        _player.uiRes[0].descriptionText.text = "1 level to victory";
                        _player.uiRes[0].power1.text = u.itemsNeeded.energyAllo.ToString();
                    }
                }
            }
        }
    }
}
