﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(STransferDrone))]
public class SUI2 : JobComponentSystem
{
    public static JobHandle buildHandle;

    // Start is called before the first frame update
    void Start()
    {
        Enabled = false;
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        UIJob1 job = new UIJob1
        {
        };
        //buildHandle = job.Schedule(_user.Length, 1, inputDeps);

        return buildHandle;
    }

    //[BurstCompile]
    struct UIJob1 : IJobParallelFor
    {
        public void Execute(int index)
        {
        }
    }
}
