﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

//previous time at 1 hour
//todolist
//go over all idle zones, go over all drones, if they are idle make they do work. calculate distance from idle point
//OR
//If drone is just born, give it work. calculate from birthplace, if no work, make idle or make infinate work.

    /*
[UpdateAfter(typeof(SDoPlayerAction))]
[UpdateAfter(typeof(SSpawnBuilding))]
public class SDroneFindWork : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _DronePoints
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;
        [ReadOnly] public ComponentDataArray<Position> dPos;
    }

    struct _IdleDronePoints
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<IdleDroneBuffer> idleDroneBuffer;
        [ReadOnly] public ComponentDataArray<Position> iPos;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        //public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _PlayerDroneJob
    {
        public readonly int Length;
        public EntityArray self;
        //[ReadOnly] public ComponentDataArray<PlayerDroneJob> DJ;
    }

    [Inject] _Drones _drones;
    [Inject] _PlayerDroneJob _playerDroneJob;
    [Inject] _DronePoints _dronePoints;
    [Inject] _IdleDronePoints _idlePoints;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        DroneFindWorkJob job = new DroneFindWorkJob
        {
            Length0 = _drones.Length,
            self = _drones.self,
            drone = _drones.drone,
            pos = _drones.pos,

            Length1 = _dronePoints.Length,
            self1 = _dronePoints.self,
            droneBuffer = _dronePoints.droneBuffer,
            droneBufferData = _dronePoints.droneBufferData,
            dPos = _dronePoints.dPos,

            Length2 = _idlePoints.Length,
            self2 = _idlePoints.self,
            idleDroneBuffer = _idlePoints.idleDroneBuffer,

            //DJ = _playerDroneJob.DJ,
        };
        buildHandle = job.Schedule(inputDeps);

        ClearDataComing job2 = new ClearDataComing
        {
            droneBufferData = _dronePoints.droneBufferData,
        };
        buildHandle = job2.Schedule(_dronePoints.Length,1,buildHandle);

        return buildHandle;
    }

    [BurstCompile]
    struct DroneFindWorkJob : IJob
    {
        public int Length0;
        public EntityArray self;
        //public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
        [ReadOnly] public ComponentDataArray<Position> pos;

        public int Length1;
        public EntityArray self1;
        [ReadOnly] public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;
        [ReadOnly] public ComponentDataArray<Position> dPos;

        public int Length2;
        public EntityArray self2;
        public BufferArray<IdleDroneBuffer> idleDroneBuffer;

        //[ReadOnly] public ComponentDataArray<PlayerDroneJob> DJ;

        public void Execute()
        {
            //Iterate over all drones and find it work
            //if we want to focus on a specific resource check those first then fallback to default

            if (Length1 == 0)
            {
                return;
            }

            //AffinityType target = DJ[0].affinity;

            for (int index = 0; index < Length0; index++)
            {
                Drone d = drone[index];

                if (d.employed == 1)
                {
                    continue;
                }

                //Store distances so we can return to those later and not have to recalculate
                int iterate = 0;
                NativeArray<float> dist = new NativeArray<float>(Length1, Allocator.Temp);
                NativeArray<int> ind = new NativeArray<int>(Length1, Allocator.Temp);

                float maxDist = 300;
                int otherCloseTarget = -1;
                bool next = false;

                for (int i = 0; i < Length1; i++)
                {
                    DroneBufferData dbd = droneBufferData[i];
                    int neededDrones = droneBufferData[i].maxDrones - (droneBuffer[i].Length + dbd.comingDrones);

                    if (neededDrones > 0)
                    {
                        float distance = math.distance(pos[index].Value, dPos[i].Value);

                        if (distance < maxDist)
                        {
                            //If its affinity is what i want, good, target it then continue
                            if (droneBufferData[i].affinity == target)
                            {
                                d.specificTarget = self1[i];
                                d.doChangeJob = 1;
                                drone[index] = d;
                                RemoveFromIdle(self[index]);
                                dbd.comingDrones++;
                                droneBufferData[i] = dbd;
                                next = true;
                                break;
                            }
                            else
                            {
                                //if its not, but still close enough, save index for fallback, if we have no affinity focus, use it as normal
                                otherCloseTarget = i;
                                if (target == AffinityType.NONE)
                                {
                                    d.specificTarget = self1[i];
                                    d.doChangeJob = 1;
                                    drone[index] = d;
                                    RemoveFromIdle(self[index]);
                                    dbd.comingDrones++;
                                    droneBufferData[i] = dbd;
                                    next = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            //store distances
                            dist[iterate] = distance;
                            ind[iterate] = i;
                            iterate++;
                        }
                    }
                    droneBufferData[i] = dbd;
                }

                if (next)
                {
                    continue;
                }

                if (iterate == 0)
                {
                    //if we have stored no data, no need to proceed
                    //Check fallback variable, if its been assgined use it as we have stored no other data, there is no target affinity in this case
                    if (otherCloseTarget != -1)
                    {
                        d.specificTarget = self1[otherCloseTarget];
                        d.doChangeJob = 1;
                        drone[index] = d;
                        RemoveFromIdle(self[index]);

                        DroneBufferData dbd = droneBufferData[otherCloseTarget];
                        dbd.comingDrones++;
                        droneBufferData[otherCloseTarget] = dbd;

                        continue;
                    }
                    continue;
                }

                NativeSlice<float> sdist = dist.Slice(0, iterate);
                NativeSlice<int> sind = ind.Slice(0, iterate);

                //NP non power closet saved just in case
                int smallestInd = sind[0];
                float smallestDist = sdist[0];
                int NPsmallestInd = sind[0];
                float NPsmallestDist = sdist[0];
                bool powerFound = false;

                for (int i = 1; i < sdist.Length; i++)
                {
                    if (droneBufferData[i].affinity == target)
                    {
                        if (sdist[i] < smallestDist)
                        {
                            smallestDist = sdist[i];
                            smallestInd = sind[i];
                            powerFound = true;
                        }
                    }
                    else
                    {
                        if (sdist[i] < NPsmallestDist)
                        {
                            NPsmallestDist = sdist[i];
                            NPsmallestInd = sind[i];
                        }
                    }
                }

                if (powerFound)
                {
                    d.specificTarget = self1[smallestInd];
                    d.doChangeJob = 1;
                    drone[index] = d;
                    RemoveFromIdle(self[index]);

                    DroneBufferData dbd = droneBufferData[smallestInd];
                    dbd.comingDrones++;
                    droneBufferData[smallestInd] = dbd;
                }
                else
                {
                    d.specificTarget = self1[NPsmallestInd];
                    d.doChangeJob = 1;
                    drone[index] = d;
                    RemoveFromIdle(self[index]);

                    DroneBufferData dbd = droneBufferData[NPsmallestInd];
                    dbd.comingDrones++;
                    droneBufferData[NPsmallestInd] = dbd;
                }

                /*
                //find and remove from idle buffer
                bool go = false;
                for (int j=0; j< Length2; j++)
                {
                    for (int k=0;k<idleDroneBuffer[j].Length;k++)
                    {
                        if (idleDroneBuffer[j][k].entity==self[index])
                        {
                            idleDroneBuffer[j].RemoveAt(k);
                            go = true;
                            break;
                        }
                    }

                    if (go)
                    {
                        break;
                    }
                }
                ////
            }
        }

        void RemoveFromIdle(Entity toRemove)
        {
            //find and remove from idle buffer
            bool go = false;
            for (int j = 0; j < Length2; j++)
            {
                for (int k = 0; k < idleDroneBuffer[j].Length; k++)
                {
                    if (idleDroneBuffer[j][k].entity == toRemove)
                    {
                        idleDroneBuffer[j].RemoveAt(k);
                        go = true;
                        break;
                    }
                }

                if (go)
                {
                    break;
                }
            }
        }
    }

    //[BurstCompile]
    struct ClearDataComing : IJobParallelFor
    {
        public ComponentDataArray<DroneBufferData> droneBufferData;

        public void Execute(int index)
        {
            DroneBufferData d = droneBufferData[index];
            d.comingDrones = 0;
            droneBufferData[index] = d;
        }
    }
}
*/