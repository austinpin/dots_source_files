﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(STransferDrone))]
public class SScanDroneScan : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Unscanned
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _ScanDrones
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Movement> move;
        public ComponentDataArray<ScanDrone> scanDrone;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    [Inject] _ScanDrones _scanDrones;
    [Inject] _Unscanned _unscanned;

    // Start is called before the first frame update
    void Start()
    {
        Enabled = false;
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ScanJob job = new ScanJob
        {
            Length = _scanDrones.Length,
            droneSelf = _scanDrones.self,
            move = _scanDrones.move,
            scanDrone = _scanDrones.scanDrone,
            dronePos = _scanDrones.pos,

            scanSelf = _unscanned.self,
            unscanned = _unscanned.unscanned,
            unscannedPos = _unscanned.pos,

            deltaTime = Time.deltaTime,
        };
        buildHandle = job.Schedule(_unscanned.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct ScanJob : IJobParallelFor
    {
        
        public EntityArray scanSelf;
        public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> unscannedPos;

        public int Length;
        public EntityArray droneSelf;
        [ReadOnly] public ComponentDataArray<Movement> move;
        public ComponentDataArray<ScanDrone> scanDrone;
        [ReadOnly] public ComponentDataArray<Position> dronePos;

        public float deltaTime;

        public void Execute(int index)
        {
            for (int i = 0; i < Length; i++)
            {
                if (move[i].target == scanSelf[index])
                {
                    if (move[i].arrive == 1)
                    {
                        UnscannedNode unscan = unscanned[index];
                        unscan.currentScanTime += deltaTime;

                        if (unscan.currentScanTime>=unscan.maxScanTime)
                        {
                            unscan.scanned = 1;
                        }

                        unscanned[index] = unscan;
                    }
                }
            }
        }
    }
}
