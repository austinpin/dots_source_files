﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(STransferDrone))]
public class SUnscannedNode : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Unscanned
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserEletricity> power;
    }

    struct _Affect
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<PlayerAffectSelected> affect;
        [ReadOnly] public ComponentDataArray<PlayerData> data;
    }

    [Inject] _Unscanned _unscanned;
    [Inject] _User _user;
    [Inject] _Affect _affect;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        UnscannedJob job = new UnscannedJob
        {
            userSelf = _user.self,
            power = _user.power,

            Length2 = _unscanned.Length,
            scanSelf = _unscanned.self,
            unscanned = _unscanned.unscanned,
            unscannedPos = _unscanned.pos,

            Length3 = _affect.Length,
            playerSelf=_affect.self,
            affect=_affect.affect,
            data=_affect.data,

            deltaTime = Time.deltaTime,
        };
        buildHandle = job.Schedule(_user.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct UnscannedJob : IJobParallelFor
    {
        public EntityArray userSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<UserEletricity> power;

        public int Length2;
        public EntityArray scanSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> unscannedPos;

        public int Length3;
        public EntityArray playerSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<PlayerAffectSelected> affect;
        [ReadOnly] public ComponentDataArray<PlayerData> data;

        public float deltaTime;

        public void Execute(int index)
        {
            UserEletricity ue = power[0];

            for (int i = 0; i < Length2; i++)
            {
                UnscannedNode un = unscanned[i];

                if (affect[0].howToAffect == HowToAffect.Unscanned_ActivateScan)
                {
                    if (scanSelf[i] == data[0].selected)
                    {
                        if (ue.hasCurrent>=un.powerNeeded)
                        {
                            if (un.active==0)
                            {
                                un.active = 1;
                                ue.hasCurrent -= un.powerNeeded;
                            }
                        }

                        affect[0] = new PlayerAffectSelected { howToAffect = HowToAffect.None };
                    }
                }

                if (un.active == 1)
                {
                    un.currentScanTime += deltaTime;
                    if (un.currentScanTime >= un.maxScanTime)
                    {
                        un.scanned = 1;
                    }
                }
                unscanned[i] = un;
            }
            power[0] = ue;
        }
    }
}
