﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(SFinishScan))]
public class BFinishScan : BarrierSystem { }

[UpdateAfter(typeof(SScanDroneFindTarget))]
[UpdateAfter(typeof(SMovement))]
public class SFinishScan : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Unscanned
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _NodeData
    {
        //public readonly int Length;
        //public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<NodeData> NodeData;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserEletricity> power;
    }

    [Inject] _Unscanned _unscanned;
    [Inject] _NodeData _nodeData;
    [Inject] _User _user;

    [Inject] BFinishScan barrier;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        PreScanJob job1 = new PreScanJob
        {
            power = _user.power,

            Lenth = _unscanned.Length,
            unscanned = _unscanned.unscanned,
        };
        JobHandle handle = job1.Schedule(_user.Length, 1, inputDeps);

        ScanJob job = new ScanJob
        {
            scanSelf = _unscanned.self,
            unscanned = _unscanned.unscanned,
            unscannedPos = _unscanned.pos,

            NodeData=_nodeData.NodeData,
            
            power=_user.power,

            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),

            node = Data.node,
        };
        buildHandle = job.Schedule(_unscanned.Length, 1, handle);

        return buildHandle;
    }

    [BurstCompile]
    struct PreScanJob : IJobParallelFor
    {
        public ComponentDataArray<UserEletricity> power;

        public int Lenth;
        //public EntityArray scanSelf;
        [ReadOnly] public ComponentDataArray<UnscannedNode> unscanned;
        
        public void Execute(int index)
        {
            /*
            for (int i=0; i <Lenth;i++)
            {
                if (unscanned[i].scanned==1)
                {
                    UserEletricity ue = power[index];
                    ue.current -= unscanned[i].powerNeeded;
                    power[index] = ue;
                }
            }
            */
        }
    }

    //[BurstCompile]
    struct ScanJob : IJobParallelFor
    {
        public EntityArray scanSelf;
        [ReadOnly] public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> unscannedPos;

        [ReadOnly] public SharedComponentDataArray<NodeData> NodeData;

        [ReadOnly] public ComponentDataArray<UserEletricity> power;

        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;

        public EntityArchetype node;

        public void Execute(int index)
        {
            UnscannedNode s = unscanned[index];
            if (s.scanned == 1)
            {
                //im assuming the scan drone find targets system allows drones to deallocate safely
                //as they have already incremented tick in scan drone scan system,
                //so scan drone find targets systems can check and allows all drones to deallocate from it
                buffer.DestroyEntity(scanSelf[index]);
            }
        }
    }
}
