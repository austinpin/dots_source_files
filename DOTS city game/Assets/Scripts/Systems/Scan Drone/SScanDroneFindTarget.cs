﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(SScanDroneScan))]
[UpdateBefore(typeof(SMovement))]
public class SScanDroneFindTarget : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Unscanned
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _ScanDrones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<ScanDrone> scanDrone;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _Dump
    {
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<ScanDroneDump> scanDroneDump;
    }

    [Inject] _ScanDrones _scanDrones;
    [Inject] _Unscanned _unscanned;
    [Inject] _Dump _dump;

    [Inject][ReadOnly] ComponentDataFromEntity<UnscannedNode> allUnscannedNode;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ScanFindTargetsJob job = new ScanFindTargetsJob
        {
            droneSelf = _scanDrones.self,
            move = _scanDrones.move,
            scanDrone = _scanDrones.scanDrone,
            dronePos = _scanDrones.pos,

            Length = _unscanned.Length,
            scanSelf = _unscanned.self,
            unscanned = _unscanned.unscanned,
            unscannedPos = _unscanned.pos,

            dumpSelf=_dump.self,
            scanDroneDump=_dump.scanDroneDump,

            allUnscannedNode=allUnscannedNode,

            deltaTime = Time.deltaTime,
        };
        buildHandle = job.Schedule(_scanDrones.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct ScanFindTargetsJob : IJobParallelFor
    {
        public EntityArray droneSelf;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<ScanDrone> scanDrone;
        [ReadOnly] public ComponentDataArray<Position> dronePos;

        public int Length;
        public EntityArray scanSelf;
        [ReadOnly] public ComponentDataArray<UnscannedNode> unscanned;
        [ReadOnly] public ComponentDataArray<Position> unscannedPos;

        public EntityArray dumpSelf;
        [ReadOnly] public ComponentDataArray<ScanDroneDump> scanDroneDump;

        [ReadOnly] public ComponentDataFromEntity<UnscannedNode> allUnscannedNode;

        public float deltaTime;

        public void Execute(int index)
        {
            ScanDrone d = scanDrone[index];
            Movement m = move[index];

            if (d.state == ScanDroneState.Working)
            {
                UnscannedNode s = allUnscannedNode[d.scanTarget];

                if (s.scanned==1)
                {
                    d.state = ScanDroneState.NewTarget;
                }
            }


            if (d.state == ScanDroneState.NewTarget)
            {
                /*
                float shortestDistance = -1;
                int shortestindex = -1;
                int shortestindex2 = -1;
                int shortestindex3 = -1;

                for (int i = 0; i < Length; i++)
                {
                    //Debug.Log(shortestindex +";1;");
                    //Debug.Log(shortestindex2 +";2;");
                    //Debug.Log(shortestindex3 + ";3;");
                    if (unscanned[i].scanned==1)
                    {
                        continue;
                    }
                    float distance = math.distance(dronePos[index].Value, unscannedPos[i].Value);
                    //Debug.Log(distance +";"+i);

                    if (shortestDistance == -1)
                    {
                        shortestDistance = distance;
                        shortestindex = i;
                        continue;
                    }

                    if (distance < shortestDistance)
                    {
                        shortestindex3 = shortestindex2;
                        shortestindex2 = shortestindex;

                        shortestDistance = distance;
                        shortestindex = i;
                    }
                }
                */

                /*
                byte complete = 0;
                Unity.Mathematics.Random rand = new Unity.Mathematics.Random(d.partition);
                byte ran = (byte) rand.NextInt(1,3);

                if (ran==3)
                {
                    if (shortestindex3 != -1)
                    {
                        d.scanTarget = scanSelf[shortestindex3];
                        d.state = ScanDroneState.Working;
                        m.target = scanSelf[shortestindex3];
                        m.arrive = 0;

                        complete = 1;
                    }
                    else
                    {
                        ran -= 1;
                    }
                }

                if (ran == 2)
                {
                    if (shortestindex2 != -1)
                    {
                        d.scanTarget = scanSelf[shortestindex2];
                        d.state = ScanDroneState.Working;
                        m.target = scanSelf[shortestindex2];
                        m.arrive = 0;

                        complete = 1;
                    }
                }

                if (shortestindex != -1 && complete==0)
                {
                    d.scanTarget = scanSelf[shortestindex];
                    d.state = ScanDroneState.Working;
                    m.target = scanSelf[shortestindex];
                    m.arrive = 0;

                    complete = 1;
                }

                if (complete==0)
                {
                    //m.arrive = 1; //stops errors when movement system tries to do things (might need a location for them to dump to instead)
                    d.state = ScanDroneState.NoMoreTargets;
                    m.arrive = 0;
                    m.target = dumpSelf[0];
                }
                */

                //d.partition = rand.NextUInt();

                //m.arrive = 1; //stops errors when movement system tries to do things (might need a location for them to dump to instead)
                d.state = ScanDroneState.NoMoreTargets;
                m.arrive = 0;
                m.target = dumpSelf[0];

                scanDrone[index] = d;
                move[index] = m;
            }
        }
    }
}
