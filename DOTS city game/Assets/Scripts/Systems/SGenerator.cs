﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(STransferDrone))]
public class SGenerator : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Generator
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Generator> generator;
        public ComponentDataArray<Position> pos;
        public BufferArray<DroneBuffer> db;
    }

    struct _Power
    {
        public ComponentDataArray<UserEletricity> power;
    }

    [Inject] _Generator _generator;
    [Inject] _Power _power;

    struct _Time
    {
        [ReadOnly] public ComponentDataArray<GD_Time> time;
    }

    [Inject] _Time _time;

    [ReadOnly] [Inject] BufferFromEntity<NeededItemsBuffer> allNeededItems;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        GeneratorJob job = new GeneratorJob
        {
            self = _generator.self,
            generator = _generator.generator,
            db = _generator.db,

            power = _power.power,

            deltaTime = Time.deltaTime * _time.time[0].timeSpeed,
        };
        buildHandle = job.Schedule(_generator.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct GeneratorJob : IJobParallelFor
    {
        public EntityArray self;
        public ComponentDataArray<Generator> generator;
        public BufferArray<DroneBuffer> db;

        [NativeDisableParallelForRestriction] public ComponentDataArray<UserEletricity> power;

        public float deltaTime;

        public void Execute(int index)
        {
            Generator g = generator[index];

            /*
            g.currentTime += deltaTime;
            if (g.currentTime >= g.maxTime)
            {
                g.currentTime = 0;
                UserEletricity ue = power[0];
                ue.current += g.production;
                if (ue.current > ue.Max)
                {
                    ue.current = ue.Max;
                }
                power[0] = ue;
            }
            */

            generator[index] = g;
        }
    }
}
