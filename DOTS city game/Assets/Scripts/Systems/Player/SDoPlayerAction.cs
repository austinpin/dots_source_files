﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(SDoPlayerAction))]
public class BDoPlayerAction : BarrierSystem { }

public class SDoPlayerAction : JobComponentSystem
{
    public static JobHandle DoHandle;
    public static byte doUpdate;

    struct _DronePoints
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<DroneBuffer> droneBuffer;
        [ReadOnly] public ComponentDataArray<DroneBufferData> droneBufferData;
        [ReadOnly] public ComponentDataArray<Position> dPos;
    }

    struct _IdleDronePoints
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<IdleDroneBuffer> idleDroneBuffer;
        [ReadOnly] public ComponentDataArray<Position> iPos;
    }

    struct _Player
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<PlayerData> playerData;
        public ComponentDataArray<PlayerIncrement> playerIncrement;
        public ComponentDataArray<PlayerActionList> playerAction;
    }


    [Inject] _DronePoints _drones;
    [Inject] _IdleDronePoints _idleDrones;
    [Inject] _Player _player;

    [Inject] BDoPlayerAction barrier;

    [ReadOnly] [Inject] ComponentDataFromEntity<Movement> allMove;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        doUpdate = 0;
        if (_player.playerAction[0].playerAction!=PlayerAction.Nothing)
        {
            TransferDroneJob setJob = new TransferDroneJob
            {
                self = _drones.self,
                droneBuffer = _drones.droneBuffer,
                droneBufferData=_drones.droneBufferData,
                dPos= _drones.dPos,

                Length = _idleDrones.Length,
                idleSelf = _idleDrones.self,
                idleDroneBuffer = _idleDrones.idleDroneBuffer,
                iPos=_idleDrones.iPos,

                pA = _player.playerAction[0],
                pD = _player.playerData[0],
                pC = _player.playerIncrement[0],

                deltaTime = Time.deltaTime,
                ECB = barrier.CreateCommandBuffer().ToConcurrent(),

                allMove=allMove,
            };
            DoHandle = setJob.Schedule(_drones.Length, 1, inputDeps);
            //setJob.Run(_drones.Length);

            PlayerActionList p = _player.playerAction[0];
            p.playerAction = PlayerAction.Nothing;
            _player.playerAction[0] = p;
            doUpdate = 1;
        }
        return DoHandle;
    }

    //[BurstCompile]
    struct TransferDroneJob : IJobParallelFor
    {
        public EntityArray self;
        public BufferArray<DroneBuffer> droneBuffer;
        [ReadOnly] public ComponentDataArray<DroneBufferData> droneBufferData;
        [ReadOnly] public ComponentDataArray<Position> dPos;

        public int Length;
        public EntityArray idleSelf;
        [NativeDisableParallelForRestriction] public BufferArray<IdleDroneBuffer> idleDroneBuffer;
        [ReadOnly] public ComponentDataArray<Position> iPos;

        public PlayerActionList pA;
        public PlayerData pD;
        public PlayerIncrement pC;

        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer.Concurrent ECB;

        [ReadOnly] public ComponentDataFromEntity<Movement> allMove;

        public void Execute(int index)
        {
            //entity we have selected
            if (self[index]==pD.selected)
            {
                ushort currentIncrement=0;
                if (pA.playerAction == PlayerAction.AddDrone)
                {
                    NativeArray<Entity> e = new NativeArray<Entity>(Length, Allocator.Temp);
                    NativeArray<float> f = new NativeArray<float>(Length, Allocator.Temp);

                    for (int i = 0; i < Length; i++)
                    {
                        //skip if it dosent have any so we dont have to calculate distance for it
                        if (idleDroneBuffer[i].Length == 0)
                        {
                            e[i] = idleSelf[i];
                            f[i] = -2;

                            continue;
                        }

                        float dist = math.distance(dPos[index].Value, iPos[i].Value);

                        e[i] = idleSelf[i];
                        f[i] = dist;
                    }

                    byte active = 0;
                    float shortestDistance = -1;
                    int shortestIndex = -1;
                    //go though each idle zone, get each drone 1 by 1 and transfer it until we reach the increment limit defined in playerincrement pC 
                    for (int j = 0; j < Length; j++)
                    {

                        if (active==0)
                        {
                            shortestDistance = -1;
                            shortestIndex = -1;

                            for (int q = 0; q < Length; q++)
                            {
                                if (f[q] == -2)
                                {
                                    continue;
                                }

                                if (shortestDistance == -1)
                                {
                                    shortestDistance = f[q];
                                    shortestIndex = q;
                                }

                                if (f[q] < shortestDistance)
                                {
                                    shortestDistance = f[q];
                                    shortestIndex = q;
                                }
                            }
                            if (shortestIndex == -1)
                            {
                                break;
                            }
                        }

                        if (droneBuffer[index].Length + currentIncrement < droneBufferData[index].maxDrones)
                        {
                            if (idleDroneBuffer[shortestIndex].Length > 0)
                            {
                                active = 1;
                                Entity et = idleDroneBuffer[shortestIndex][0];
                                idleDroneBuffer[shortestIndex].RemoveAt(0);

                                ECB.SetComponent(index, et, new Drone { specificTarget = pD.selected, doChangeJob = 1 });

                                currentIncrement++;
                                j--;

                                if (currentIncrement >= pC.increment)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                active = 0;
                                f[shortestIndex] = -2;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    e.Dispose();
                    f.Dispose();
                }
                else if (pA.playerAction == PlayerAction.SubtractDrone)
                {
                    if (droneBuffer[index].Length > 0)
                    {
                        for (int i = 0; i < Length; i++)
                        {
                            Entity e = droneBuffer[index][0];

                            ECB.SetComponent(index,e, new Drone { nextJob = NextJobType.Idle , doChangeJob=1 });

                            droneBuffer[index].RemoveAt(0);

                            currentIncrement++;
                            i--;

                            if (currentIncrement>=pC.increment)
                            {
                                break;
                            }
                            if (droneBuffer[index].Length==0)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
