﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

public class SPlayerSelect : JobComponentSystem
{
    public static JobHandle selectHandle;

    struct AllPos
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;
    }

    struct _PlayerInteract
    {
        public readonly int Length;
        public ComponentDataArray<PlayerData> data;
        public ComponentDataArray<PlayerInteractionMode> pim;
        public ComponentDataArray<PlayerSubInteractionMode> psim;
    }

    [Inject] _PlayerInteract _playerInteract;
    [Inject] AllPos _allPos;

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        byte click = 0;
        if (Input.GetMouseButtonDown(1))
        {
            click = 1;
        }
        Ray ray;
        Vector3 pos = Input.mousePosition;
        ray = Camera.main.ScreenPointToRay(pos);

        PlayerSelectJob playerJob = new PlayerSelectJob
        {
            data = _playerInteract.data,
            pim = _playerInteract.pim,
            psim = _playerInteract.psim,

            Length = _allPos.Length,
            posSelf = _allPos.self,
            pos = _allPos.pos,
            radius = _allPos.radius,

            rayOrigin = ray.origin,
            rayDirection = ray.direction,

            click = click,
        };
        selectHandle = playerJob.Schedule(_playerInteract.Length, 1, inputDeps);

        return selectHandle;
    }

    //thanks to (1)
    //https://answers.unity.com/questions/869869/method-of-finding-point-in-3d-space-that-is-exactl.html

    //[BurstCompile]
    struct PlayerSelectJob : IJobParallelFor
    {
        public ComponentDataArray<PlayerData> data;
        public ComponentDataArray<PlayerInteractionMode> pim;
        public ComponentDataArray<PlayerSubInteractionMode> psim;

        public int Length;
        public EntityArray posSelf;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;

        public float3 rayDirection;
        public float3 rayOrigin;

        public byte click;

        public void Execute(int index)
        {
            PlayerData d = data[index];

            d.highlighted = Entity.Null;
            
            for (int i=0; i<Length;i++)
            {
                float3 centreToStart = rayOrigin - pos[i].Value;

                //see (1) above for credits and explanation, not sure how this works but its a mathmatical formula for solving for a variable
                //i seen it in maths classes long ago in a different form for solving for x
                float a = math.dot(rayDirection, rayDirection);
                float b = 2 * math.dot(centreToStart, rayDirection);
                var x = math.mul(radius[i].clickRadius, 2);
                float c = math.dot(centreToStart, centreToStart) - ((radius[i].clickRadius) * (radius[i].clickRadius));
                //float c = math.dot(centreToStart, centreToStart) - ((1.2f * 2) * (1.2f * 2));

                //if the ray is over the object, then the command effects this current object
                float discriminant = (b * b) - (4 * a * c);

                if (discriminant > 0)
                {
                    if (click==1)
                    {
                        if (psim[index].playerSubInteractMode == PlayerSubInteractionModes.None)
                        {
                            d.selected = posSelf[i];

                            PlayerInteractionMode _pim = pim[index];
                            _pim.playerInteractMode = PlayerInteractionModes.Selected;
                            pim[index] = _pim;
                        }
                        if (psim[index].playerSubInteractMode == PlayerSubInteractionModes.AssignOrder)
                        {
                            d.subSelected = posSelf[i];
                        }
                    }

                    d.highlighted = posSelf[i];
                    break;
                }
            }
            data[index] = d;
        }
    }
}
