﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//sends items to where they are needed

    [UpdateAfter(typeof(SPlayerSelect))]
public class SMouseToWorldPos : JobComponentSystem
{
    public static JobHandle pointHandle;

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        Ray ray;
        Vector3 pos = Input.mousePosition;
        ray = Camera.main.ScreenPointToRay(pos);

        GetWorldPointJob playerJob = new GetWorldPointJob
        {
            cameraRay = ray,
        };
        pointHandle = playerJob.Schedule(this, inputDeps);

        return pointHandle;
    }

    //[BurstCompile]
    struct GetWorldPointJob : IJobProcessComponentDataWithEntity<PlayerBuild>
    {
        public Ray cameraRay;

        public void Execute(Entity self, int index, ref PlayerBuild player)
        {
            Plane p = new Plane(new Vector3(1000, 25, 0), new Vector3(0, 25, 1000), new Vector3(-1000, 25, 0));
            float enter = 0.0f;

            //mostly from unity documentation, plane intersection, very nice that these are structs.
            if (p.Raycast(cameraRay, out enter))
            {
                //Get the point
                Vector3 hitPoint = cameraRay.GetPoint(enter);

                player.worldPos = hitPoint;
            }
        }
    }
}
