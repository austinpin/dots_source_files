﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//sends items to where they are needed

    /*
[UpdateAfter(typeof(SMouseToWorldPos))]
public class SPlayerInteractUnrevealed : JobComponentSystem
{
    public static JobHandle pointHandle;

    struct _RevealTiles
    {
        public readonly int Length;
        public ComponentDataArray<RevealTile> reveal;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _WorldPos
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<PlayerBuild> pos;
    }

    [Inject] _RevealTiles _revealTiles;
    [Inject] _WorldPos _worldPos;

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        byte click = 0;
        if (Input.GetMouseButtonDown(1))
        {
            click = 1;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            click = 2;
        }

        Ray ray;
        Vector3 pos = Input.mousePosition;
        ray = Camera.main.ScreenPointToRay(pos);

        InteractUnrevealedJob playerJob = new InteractUnrevealedJob
        {
            reveal=_revealTiles.reveal,
            pos = _revealTiles.pos,

            tPos = _worldPos.pos,

            cameraRay = ray,
            click=click,
        };
        pointHandle = playerJob.Schedule(_revealTiles.Length,1, inputDeps);

        return pointHandle;
    }

    [BurstCompile]
    struct InteractUnrevealedJob : IJobParallelFor
    {
        public ComponentDataArray<RevealTile> reveal;
        [ReadOnly] public ComponentDataArray<Position> pos;

        [ReadOnly] public ComponentDataArray<PlayerBuild> tPos;

        public Ray cameraRay;
        public byte click;

        public void Execute(int index)
        {
            if (click != 0)
            {
                float distance = -1;

                distance = math.distance(tPos[0].worldPos, pos[index].Value);

                //mostly from unity documentation, plane intersection, very nice that these are structs.
                if (distance < 99.95f)
                {
                    //only executes if its on a point inside it
                    //theirfore reveal it
                    RevealTile t = reveal[index];
                    t.reveal = 1;
                    reveal[index] = t;
                    //buff[index].RemoveAt(i);
                    //buff[index].Insert(i, t);
                    return;
                }
            }
        }
    }
}
*/