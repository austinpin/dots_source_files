﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(STransferDrone))]
public class BTransferDrone: BarrierSystem { }

[UpdateAfter(typeof(BDoPlayerAction))]
//[UpdateAfter(typeof(SDroneFindWork))]
//[UpdateAfter(typeof(STransportParkingItemTransfer))]
public class STransferDrone : JobComponentSystem
{
    public static JobHandle DoHandle;

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _DronePoints
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> data;
    }

    struct _IdleDronePoints
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<IdleDroneBuffer> idleDroneBuffer;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    [Inject] _Drones _drones;
    [Inject] _DronePoints _dronePoints;
    [Inject] _IdleDronePoints _idleDrones;

    [Inject] BTransferDrone barrier;

    [ReadOnly] [Inject] ComponentDataFromEntity<Movement> allMove;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        if (_drones.Length!=0)
        {
            PrepareIdleJob idleJob = new PrepareIdleJob
            {
                droneSelf = _drones.self,
                move = _drones.move,
                drone = _drones.drone,
                dronePos = _drones.pos,

                Length = _idleDrones.Length,
                idleSelf = _idleDrones.self,
                idleDroneBuffer = _idleDrones.idleDroneBuffer,
                idlePos = _idleDrones.pos,
            };
            inputDeps = idleJob.Schedule(_drones.Length, 1, inputDeps);
        }
        //idleJob.Run(_drones.Length);

        MakeIdleJob makeIdle = new MakeIdleJob
        {
            Length = _drones.Length,
            droneSelf = _drones.self,
            move = _drones.move,
            drone = _drones.drone,
            
            idleSelf = _idleDrones.self,
            idleDroneBuffer = _idleDrones.idleDroneBuffer,
        };
        inputDeps = makeIdle.Schedule(_idleDrones.Length, 1, inputDeps);
        //makeIdle.Run(_idleDrones.Length);

        TransferDroneJob setJob = new TransferDroneJob
        {
            droneLength=_drones.Length,
            droneSelf=_drones.self,
            move=_drones.move,
            drone=_drones.drone,

            self = _dronePoints.self,
            droneBuffer = _dronePoints.droneBuffer,
            data = _dronePoints.data,
        };
        DoHandle = setJob.Schedule(_dronePoints.Length, 1, inputDeps);
        //setJob.Run(_dronePoints.Length);

        return DoHandle;
    }

    [BurstCompile]
    //prepares idle set data
    struct PrepareIdleJob : IJobParallelFor
    {
        public EntityArray droneSelf;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
        [ReadOnly] public ComponentDataArray<Position> dronePos;

        public int Length;
        public EntityArray idleSelf;
        public BufferArray<IdleDroneBuffer> idleDroneBuffer;
        [ReadOnly] public ComponentDataArray<Position> idlePos;

        //[ReadOnly] public EntityCommandBuffer ECB;

        public void Execute(int index)
        {
            if (drone[index].nextJob == NextJobType.Idle && drone[index].doChangeJob==1)
            {
                float shortestDistance = -1;
                int shortestIndex = -1;

                for (int i = 0; i < Length; i++)
                {
                    float dist = math.distance(dronePos[index].Value, idlePos[i].Value);

                    if (shortestDistance == -1)
                    {
                        shortestDistance = dist;
                        shortestIndex = i;
                    }

                    if (dist < shortestDistance)
                    {
                        shortestDistance = dist;
                        shortestIndex = i;
                    }
                }
                Drone d = drone[index];
                d.closestReassignmentTarget = idleSelf[shortestIndex];
                //Debug.Log(idleSelf[shortestIndex]);
                //Debug.Log(droneSelf[index]);
                drone[index] = d;
            }
        }
    }

    [BurstCompile]
    //prepares idle set data
    struct MakeIdleJob : IJobParallelFor
    {
        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;
        
        public EntityArray idleSelf;
        public BufferArray<IdleDroneBuffer> idleDroneBuffer;

        //[ReadOnly] public EntityCommandBuffer ECB;

        public void Execute(int index)
        {
            for (int i=0; i<Length;i++)
            {
                if (drone[i].nextJob == NextJobType.Idle && drone[i].doChangeJob==1)
                {
                    if (drone[i].closestReassignmentTarget==idleSelf[index])
                    {
                        idleDroneBuffer[index].Insert(0,droneSelf[i]);

                        Movement m = move[i];
                        m.arrive = 0;
                        m.target = idleSelf[index];
                        move[i] = m;

                        drone[i] = new Drone { };
                    }
                }
            }
        }
    }

    [BurstCompile]
    struct TransferDroneJob : IJobParallelFor
    {
        //check all drones then add as needed
        public int droneLength;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        public EntityArray self;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> data;

        public void Execute(int index)
        {
            for (int i = 0; i < droneLength;i++)
            {
                if (drone[i].doChangeJob==1)
                {
                    if (drone[i].specificTarget == self[index])
                    {
                        droneBuffer[index].Insert(0,droneSelf[i]);

                        Movement m = move[i];
                        m.arrive = 0;
                        m.target = self[index];
                        move[i] = m;

                        Drone d = drone[i];
                        d.workEntity = self[index];
                        d.doChangeJob = 0;
                        d.specificTarget = Entity.Null;
                        d.myJob = data[index].myJob;
                        d.employed = 1;
                        drone[i] = d;

                        DroneBufferData dbd = data[index];
                        dbd.comingDrones = 0;
                        data[index] = dbd;
                    }
                }
            }
        }
    }
}
