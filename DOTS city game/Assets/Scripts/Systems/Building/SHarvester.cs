﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class SHarvester : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Harvest
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Harvester> harvest;
        public ComponentDataArray<Status> status;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;

        public ComponentDataArray<UserPhotite> photite;
        public ComponentDataArray<UserNanites> nanites;

        public ComponentDataArray<UserDurium> durium;
        public ComponentDataArray<UserZorium> zorium;

        public ComponentDataArray<UserVions> vions;
        public ComponentDataArray<UserNorn> norn;
    }

    [Inject] _User _user;
    [Inject] _Harvest _harvest;

    struct _Time
    {
        [ReadOnly] public ComponentDataArray<GD_Time> time;
    }

    [Inject] _Time _time;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            photite = _user.photite,
            nanites = _user.nanites,
            durium = _user.durium,
            zorium = _user.zorium,
            vions = _user.vions,
            norn = _user.norn,

            Length = _harvest.Length,
            salvageSelf = _harvest.self,
            harvest = _harvest.harvest,
            status = _harvest.status,

            deltaTime = Time.deltaTime * _time.time[0].timeSpeed
        };
        arrivalHandle = job.Schedule(_user.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public ComponentDataArray<UserPhotite> photite;
        public ComponentDataArray<UserNanites> nanites;

        public ComponentDataArray<UserDurium> durium;
        public ComponentDataArray<UserZorium> zorium;

        public ComponentDataArray<UserVions> vions;
        public ComponentDataArray<UserNorn> norn;

        public int Length;
        public EntityArray salvageSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Harvester> harvest;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Status> status;

        public float deltaTime;

        public void Execute(int index)
        {
            for (int i=0; i<Length;i++)
            {
                Harvester h = harvest[i];

                if (status[i].level>0)
                {
                    h.currentTime += deltaTime;

                    if (h.currentTime > h.maxTime)
                    {
                        h.currentTime = 0;
                        //h.harvestingAmmount++;

                        if (h.type == ResType.Photite)
                        {
                            UserPhotite up = photite[index];
                            up.hasCurrent += (uint)h.gain;
                            if (up.hasCurrent > 9999)
                            {
                                up.hasCurrent = 9999;
                            }
                            photite[index] = up;
                        }
                        if (h.type == ResType.Durium)
                        {
                            UserDurium ud = durium[index];
                            ud.hasCurrent += (uint)h.gain;
                            if (ud.hasCurrent > 9999)
                            {
                                ud.hasCurrent = 9999;
                            }
                            durium[index] = ud;
                        }
                        if (h.type == ResType.Vions)
                        {
                            UserVions ud = vions[index];
                            ud.hasCurrent += (uint)h.gain;
                            if (ud.hasCurrent > 9999)
                            {
                                ud.hasCurrent = 9999;
                            }
                            vions[index] = ud;
                        }

                        if (h.type == ResType.Nanite)
                        {
                            UserNanites ud = nanites[index];
                            ud.hasCurrent += (uint)h.gain;
                            if (ud.hasCurrent > 9999)
                            {
                                ud.hasCurrent = 9999;
                            }
                            nanites[index] = ud;
                        }
                        if (h.type == ResType.Zorium)
                        {
                            UserZorium ud = zorium[index];
                            ud.hasCurrent += (uint)h.gain;
                            if (ud.hasCurrent > 9999)
                            {
                                ud.hasCurrent = 9999;
                            }
                            zorium[index] = ud;
                        }
                        if (h.type == ResType.Norn)
                        {
                            UserNorn ud = norn[index];
                            ud.hasCurrent += (uint)h.gain;
                            if (ud.hasCurrent > 9999)
                            {
                                ud.hasCurrent = 9999;
                            }
                            norn[index] = ud;
                        }
                    }
                }

                harvest[i] = h;
            }
        }
    }
}
