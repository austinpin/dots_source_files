﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(SBuildManager))]
[UpdateAfter(typeof(STransferDrone))]
public class SReplaceSalvageWithBuild : ComponentSystem
{
    //public static JobHandle buildHandle;

    struct _Salvages
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Salvage> salvage;
        public ComponentDataArray<StorageTarget> storage;
        public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;
        public BufferArray<DroneBuffer> droneBuffer;
    }

    struct _BuildData
    {
        //public readonly int Length;
        //public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<BuildingData> buildData;
    }


    struct _Player
    {
        //public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<PlayerDroneLimits> limits;
    }

    [Inject] _Player _player;
    [Inject] _Salvages _salvages;
    [Inject] _BuildData _buildData;

    [Inject] [ReadOnly] ComponentDataFromEntity<Square> allSquare;

    [Inject] [ReadOnly] ComponentDataFromEntity<TransportParking> allTP;
    [Inject] [ReadOnly] ComponentDataFromEntity<Science> allScience;
    [Inject] [ReadOnly] ComponentDataFromEntity<DroneFactory> allDF;
    [Inject] [ReadOnly] ComponentDataFromEntity<Salvage> allSalvage;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        for (int i=0; i<_salvages.Length;i++)
        {
            Salvage s = _salvages.salvage[i];
            StorageTarget t = _salvages.storage[i];

            if (s.change==1)
            {
                if (_salvages.droneBuffer[i].Length == 0)
                {
                    /*
                    if (EntityManager.HasComponent<TransportParking>(t.storageTarget))
                    {
                        TransportParking p = EntityManager.GetComponentData<TransportParking>(t.storageTarget);
                        //p.active = 0;
                        //p.metalComing = 0;
                        //EntityManager.SetComponentData(t.storageTarget, p);
                        PostUpdateCommands.SetComponent(t.storageTarget, p);
                    }
                    */

                    PostUpdateCommands.DestroyEntity(_salvages.self[i]);
                }
            }
        }
    }
}
