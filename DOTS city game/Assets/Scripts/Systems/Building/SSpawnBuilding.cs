﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(SBuildManager))]
[UpdateAfter(typeof(STransferDrone))]
[UpdateAfter(typeof(BPlaceBuilding))]
[UpdateAfter(typeof(SBuildingZoneTest))]
[UpdateAfter(typeof(SReplaceSalvageWithBuild))]
public class SSpawnBuilding : ComponentSystem
{
    //public static JobHandle buildHandle;

    struct _Builds
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Build> build;
        public ComponentDataArray<StorageTarget> storage;
        public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;
        public BufferArray<DroneBuffer> droneBuffer;
    }

    struct _BuildData
    {
        //public readonly int Length;
        //public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<BuildingData> buildData;
    }


    struct _Player
    {
        //public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<PlayerDroneLimits> limits;
        public ComponentDataArray<PlayerOther> other;
        [ReadOnly] public SharedComponentDataArray<UIScreens> screens;
    }

    struct _UnitData
    {
        [ReadOnly] public SharedComponentDataArray<UnitData> unitData;
    }

    struct _User
    {
        public ComponentDataArray<UserEletricity> power;
        public ComponentDataArray<UserQuests> quests;
        public ComponentDataArray<UserUnlocks> unlocks;
        public ComponentDataArray<UserPrefabs> prefabs;
    }

    struct _Home
    {
        public ComponentDataArray<Home> home;
    }

    [Inject] _User _user;

    [Inject] _Player _player;
    [Inject] _Builds _builds;
    [Inject] _BuildData _buildData;
    [Inject] _UnitData _unitData;
    [Inject] _Home _home;

    [Inject] [ReadOnly] ComponentDataFromEntity<Square> allSquare;
    [Inject] [ReadOnly] ComponentDataFromEntity<TransportParking> allTP;
    [Inject] [ReadOnly] ComponentDataFromEntity<Science> allScience;
    [Inject] [ReadOnly] ComponentDataFromEntity<DroneFactory> allDF;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        for (int i=0; i<_builds.Length;i++)
        {
            Build b = _builds.build[i];
            StorageTarget t = _builds.storage[i];
            UserUnlocks uu = _user.unlocks[0];
            UserPrefabs upre = _user.prefabs[0];

            if (b.spawnBuilding==2)
            {
                if (_builds.droneBuffer[i].Length == 0)
                {
                    PostUpdateCommands.DestroyEntity(_builds.self[i]);

                    if (_player.other[0].placedStart == 1)
                    {
                        if (b.toBuild == Building.Idle)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_Idle);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].idleR);

                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(new RadiusSet { clickRadius = 3, buildingRadius = 3 });
                        }

                        if (b.toBuild == Building.DroneFactory)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_DroneFactory);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].droneFactoryR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            //PostUpdateCommands.SetComponent(allDF[_buildData.buildData[0].droneFactory]);
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                            PostUpdateCommands.SetComponent(new DroneBufferData { maxDrones = 4 });
                            //PostUpdateCommands.SetComponent(t);

                            DynamicBuffer<NeededItemsBuffer> nib = PostUpdateCommands.SetBuffer<NeededItemsBuffer>();
                            nib.Add(new Storage { itemStoring = Item.Materials, maxContains = 8 });

                            //DynamicBuffer<DroneFactoryBuffer> dfb = PostUpdateCommands.SetBuffer<DroneFactoryBuffer>();
                            //dfb.Add(new Storage { itemStoring = Item.Metal, maxContains = 2 });
                        }

                        if (b.toBuild == Building.VictoryMonument)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_VictoryMonument);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].VMR);
                            PostUpdateCommands.SetComponent(new Square { length = 7 });
                            PostUpdateCommands.SetComponent(new Status { iAm = Building.VictoryMonument});
                            //PostUpdateCommands.SetComponent(_buildData.buildData[0].VictoryMonument);
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            //up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { prefab = PrefabType.VM } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo=1} });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo=40} });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { duriumAllo=30} });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { zoriumAllo=30} });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { vionsAllo=20} });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { nornAllo=20} });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo=100} });
                        }

                        if (b.toBuild == Building.Farm)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_Farm);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetComponent(new Farm { maxTime = 1, gain = 1, });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].BeaconR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);
                        }
                        if (b.toBuild == Building.DiamondMine)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_Farm);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetComponent(new Farm { maxTime = 1, gain = 8, });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].BeaconR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);
                        }
                        if (b.toBuild == Building.IrradiatedGenerator)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_Generator);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetComponent(new Generator { maxTime = 1, production = 1 });
                            PostUpdateCommands.SetComponent(new Status { iAm = Building.IrradiatedGenerator , autoUpgrade=1  });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].IrradiatedR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);


                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteHas = 5 } });
                        }
                        if (b.toBuild == Building.WindPowerPlant)
                        {
                            if (uu.unlockWindTurbine==1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Generator);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Generator { maxTime = 1, production = 4 });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.WindPowerPlant, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].WindR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteHas = 10 } });
                            }
                        }
                        if (b.toBuild == Building.GeothermalGenerator)
                        {
                            if (uu.unlockGeothermalGenerator == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Generator);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Generator { maxTime = 1, production = 12 });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.GeothermalGenerator, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].GeothermalR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { duriumHas = 10 } });
                            }
                        }
                        if (b.toBuild == Building.FissionGenerator)
                        {
                            if (uu.unlockGeothermalGenerator == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Generator);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Generator { maxTime = 1, production = 20 });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.FissionGenerator, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].FissionR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { vionsHas = 10 } });
                            }
                        }
                        if (b.toBuild == Building.Beacon)
                        {
                            /*
                            PlayerOther o = _player.other[0];
                            o.updateReveal = 1;
                            _player.other[0] = o;
                            //create beacon
                            PostUpdateCommands.CreateEntity(Data.building_Beacon);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            //PostUpdateCommands.SetComponent(new Beacon{ });
                            PostUpdateCommands.SetComponent(new Status { boosted = _builds.build[i].boosted, radius = 124, iAm = Building.Beacon , autoUpgrade=1 });
                            PostUpdateCommands.SetSharedComponent(new RadiusSet { buildingRadius = 3, clickRadius = 3 });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].BeaconR);

                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { transmitters = 1 } });
                            */
                        }
                        if (b.toBuild == Building.TransmitterFactory)
                        {
                            if (uu.techTransmitterFactory==1 && upre.transmitterFactories>=1)
                            {
                                upre.transmitterFactories -= 1;
                                PostUpdateCommands.CreateEntity(Data.building_TransmitterFactory);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new TransmitterFactory { maxTime = 10, gain = 1, type = 1 });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.TransmitterFactory });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].TransmitterFactoryR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteHas = 20 } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo = 2} });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { nanitesHas = 5} });
                            }
                        }
                        if (b.toBuild == Building.LevetationFactory)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_TransmitterFactory);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetComponent(new TransmitterFactory { maxTime = 10, gain = 1, type = 2 });
                            PostUpdateCommands.SetComponent(new Status { iAm = Building.LevetationFactory });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].BeaconR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { prefab = PrefabType.LevetationsFactory } });
                        }
                        if (b.toBuild == Building.TechCentre)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_Harvester);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetComponent(new Harvester { type = ResType.Tech  });
                            PostUpdateCommands.SetComponent(new Status { iAm = Building.TechCentre , autoUpgrade = 1 });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].TechR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo = 1 , nanitesHas = 5 } });
                        }

                        //resource set
                        if (b.toBuild == Building.PhotiteMiner)
                        {
                            PostUpdateCommands.CreateEntity(Data.building_Harvester);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetComponent(new Harvester { maxTime =6, gain = 1, type = ResType.Photite });
                            PostUpdateCommands.SetComponent(new Status { iAm = Building.PhotiteMiner , autoUpgrade=1 });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].PhotiteMinerR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);


                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo = 1 } });

                        }
                        if (b.toBuild == Building.DuriumMiner)
                        {
                            if (uu.unlockDuriumMine == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Harvester);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Harvester { maxTime = 4, gain = 1, type = ResType.Durium });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.DuriumMiner, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].DuriumMinerR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo = 6 , nanitesHas = 2 } });
                            }
                        }

                        if (b.toBuild == Building.VionMiner)
                        {
                            if (uu.unlockVionsMine == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Harvester);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Harvester { maxTime = 2, gain = 1, type = ResType.Vions });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.VionMiner, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].VionsMinerR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo = 15, zoriumHas = 5 } });
                            }
                        }

                        if (b.toBuild == Building.NaniteMaker)
                        {
                            if (uu.unlockNaniteMaker == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Harvester);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Harvester { maxTime = 6, gain = 1, type = ResType.Nanite });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.NaniteMaker, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].NaniteMakerR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo = 3 , photiteHas = 10 } });
                            }
                        }

                        if (b.toBuild == Building.ZoriumMaker)
                        {
                            if (uu.unlockZoriumMaker == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Harvester);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Harvester { maxTime = 4, gain = 1, type = ResType.Zorium });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.ZoriumMaker, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].ZoriumMakerR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo = 9, duriumHas = 5 } });
                            }
                        }
                        if (b.toBuild == Building.NornMaker)
                        {
                            if (uu.unlockNornMaker == 1)
                            {
                                PostUpdateCommands.CreateEntity(Data.building_Harvester);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Harvester { maxTime = 2, gain = 1, type = ResType.Norn });
                                PostUpdateCommands.SetComponent(new Status { iAm = Building.NornMaker, autoUpgrade = 1 });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].NornMakerR);
                                PostUpdateCommands.SetComponent(new Square { length = 5 });
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { energyAllo = 25, vionsHas = 5 } });
                            }
                        }

                        //monuments 
                        if (b.toBuild == Building.M_ReadvancementCentre)
                        {
                            if (uu.unlockReadvancementsBuilding==1)
                            {
                                PostUpdateCommands.CreateEntity();
                                PostUpdateCommands.AddComponent(_builds.pos[i]);
                                PostUpdateCommands.AddComponent(new ReadvancementFacility { });
                                PostUpdateCommands.AddComponent(new Status { iAm = Building.M_ReadvancementCentre });
                                PostUpdateCommands.AddSharedComponent(_buildData.buildData[0].ReadvancementCentreR);
                                PostUpdateCommands.AddComponent(new Square { length = 5 });
                                PostUpdateCommands.AddSharedComponent(_builds.radius[0]);

                                PostUpdateCommands.AddBuffer<Upgradeable>();
                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteHas = 5 } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo = 1 , photiteHas = 10} });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { nanitesAllo = 5} });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { duriumAllo = 5} });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { zoriumAllo = 5} });
                            }
                        }
                        if (b.toBuild == Building.M_GrandBeacon)
                        {
                            /*
                            if (uu.unlockGrandBeacon == 1)
                            {
                                PlayerOther o = _player.other[0];
                                o.updateReveal = 1;
                                _player.other[0] = o;
                                //create beacon
                                PostUpdateCommands.CreateEntity(Data.building_Beacon);
                                PostUpdateCommands.SetComponent(_builds.pos[i]);
                                PostUpdateCommands.SetComponent(new Beacon { }); //1200 max
                                PostUpdateCommands.SetComponent(new Status { boosted = 0, radius = 0, iAm = Building.M_GrandBeacon });
                                PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].BeaconR);
                                PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                                DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { prefab = PrefabType.GrandBeacon } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteHas  = 25 } });
                                up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo = 3 } });
                            }
                            */
                        }
                    }

                    if (_player.other[0].placedStart==0)
                    {
                        if (b.toBuild == Building.Start)
                        {
                            PlayerOther po = _player.other[0];
                            po.placedStart = 1;
                            po.updateReveal = 1;
                            _player.other[0] = po;

                            _player.screens[0].Click.SetActive(true);
                            _player.screens[0].Stats.SetActive(true);
                            _player.screens[0].PlaceStart.SetActive(false);

                            UserQuests uq = _user.quests[0];
                            uq.placedStart = 1;
                            _user.quests[0] = uq;

                            Home h = _home.home[0];
                            h.actualPos = _builds.pos[i].Value + new float3(0,400,0);
                            _home.home[0] = h;

                            PostUpdateCommands.CreateEntity();
                            PostUpdateCommands.AddComponent(_builds.pos[i]);
                            PostUpdateCommands.AddSharedComponent(_buildData.buildData[0].BaseR);
                            PostUpdateCommands.AddSharedComponent(new RadiusSet { buildingRadius = 10f , clickRadius = 9f });
                            PostUpdateCommands.AddComponent(new Status { iAm = Building.Base });
                            PostUpdateCommands.AddComponent(new UserBase {  });


                            PostUpdateCommands.AddBuffer<Upgradeable>();
                            DynamicBuffer<Upgradeable> up = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo=1 } });
                            up.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo=1 , photiteHas=20 } });

                            /*
                            //Create Centre idle point
                            PostUpdateCommands.CreateEntity(Data.building_Idle);
                            PostUpdateCommands.SetComponent(_builds.pos[i]);
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].idleR);
                            PostUpdateCommands.SetComponent(new Square { length = 5 });
                            */

                            //create beacon
                            PostUpdateCommands.CreateEntity(Data.building_Beacon);
                            float3 pos = _builds.pos[i].Value;
                            pos += new float3(21, 0, 21);
                            PostUpdateCommands.SetComponent(new Position { Value = pos });
                            PostUpdateCommands.SetComponent(new Beacon { });
                            PostUpdateCommands.SetComponent(new Status { boosted = 0, radius = 200 , iAm = Building.M_GrandBeacon , level = 1 });
                            PostUpdateCommands.SetSharedComponent(_buildData.buildData[0].BeaconR);
                            PostUpdateCommands.SetSharedComponent(_builds.radius[0]);

                            DynamicBuffer<Upgradeable> up2 = PostUpdateCommands.SetBuffer<Upgradeable>();
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteHas = 5 } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { photiteAllo = 1 , photiteHas = 10 } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { nanitesHas = 5 } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { duriumHas = 5 } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { zoriumHas = 5 } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { vionsHas = 5 } });
                            up2.Add(new Upgradeable { itemsNeeded = new UpgradeItems { nornHas = 5 } });
                        }
                    }
                }
            }
            else
            {
                PostUpdateCommands.DestroyEntity(_builds.self[i]);
            }

            _user.unlocks[0] = uu;
            _user.prefabs[0] = upre;
        }
    }
}
