﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(SPlaceBuilding))]
public class BPlaceBuilding : BarrierSystem { }

[UpdateAfter(typeof(STransferDrone))]
[UpdateAfter(typeof(SMouseToWorldPos))]
public class SPlaceBuilding : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Player
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<PlayerBuild> playerBuild;
    }

    struct _Ghost
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<MeshInstanceRenderer> rend;
        public ComponentDataArray<GhostBuilding> ghostBuilding;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;
    }

    struct _BuildingData
    {
        [ReadOnly] public SharedComponentDataArray<BuildingData> data;
    }

    //To ensure buildings dont overlap and to provide feedback
    //this will test to see if the planned building overlaps with any existing "colliders"
    struct _BuildingRadius
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;
    }

    [Inject] _BuildingData _data;
    [Inject] _Player _player;
    [Inject] _Ghost _ghost;
    [Inject] _BuildingRadius _buildingRadius;

    [Inject] BPlaceBuilding barrier;

    [Inject][ReadOnly]  ComponentDataFromEntity<Build> allBuild;
    [Inject][ReadOnly]  BufferFromEntity<NeededItemsBuffer> allNeededItems;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        byte build = 0;
        byte multiple = 0;

        if (Input.GetMouseButtonDown(0))
        {
            build = 1;
        }

        if (Input.GetButton("Multi"))
        {
            multiple = 1;
        }

        PlaceBuildJob job = new PlaceBuildJob
        {
            playerSelf = _player.self,
            playerBuild = _player.playerBuild,

            Length = _ghost.Length,
            ghostself = _ghost.self,
            pos = _ghost.pos,
            rend = _ghost.rend,
            ghostBuilding = _ghost.ghostBuilding,
            radius = _ghost.radius,

            data = _data.data,

            colliderLength = _buildingRadius.Length,
            colliderPos = _buildingRadius.pos,
            colliderRadius = _buildingRadius.radius,

            buffer = barrier.CreateCommandBuffer(),

            allBuild = allBuild,
            allNeededItems = allNeededItems,

            buildSite = Data.buildSite,

            build = build,
            multiple = multiple,
        };
        buildHandle = job.Schedule(_player.Length, 1, inputDeps);

        return buildHandle;
    }

    //[BurstCompile]
    struct PlaceBuildJob : IJobParallelFor
    {
        public EntityArray playerSelf;
        public ComponentDataArray<PlayerBuild> playerBuild;

        public int Length;
        public EntityArray ghostself;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<MeshInstanceRenderer> rend;
        [NativeDisableParallelForRestriction] public ComponentDataArray<GhostBuilding> ghostBuilding;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;

        [ReadOnly] public SharedComponentDataArray<BuildingData> data;

        public int colliderLength;
        [ReadOnly] public ComponentDataArray<Position> colliderPos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> colliderRadius;

        [ReadOnly] public EntityCommandBuffer buffer;

        [ReadOnly] public ComponentDataFromEntity<Build> allBuild;
        [ReadOnly] public BufferFromEntity<NeededItemsBuffer> allNeededItems;

        public EntityArchetype buildSite;

        public byte build;
        public byte multiple;

        public void Execute(int index)
        {
            Position p = pos[0];
            MeshInstanceRenderer rend = new MeshInstanceRenderer { material = data[0].invalidHologramMat };
            GhostBuilding b = ghostBuilding[0];

            if (playerBuild[index].toBuild!=Building.Null)
            {
                //MeshInstanceRenderer r = rend[0];

                if (playerBuild[index].toBuild == Building.Idle)
                {
                    rend.mesh = data[0].idleR.mesh;
                }
                if (playerBuild[index].toBuild == Building.DroneFactory)
                {
                    rend.mesh = data[0].droneFactoryR.mesh;
                }
                /*
                if (playerBuild[index].toBuild == Building.WorkerDroneFacility)
                {
                    //Debug.Log("1");
                    rend.mesh = data[0].WDFR.mesh;
                }
                */
                if (playerBuild[index].toBuild == Building.VictoryMonument)
                {
                    rend.mesh = data[0].VMR.mesh;
                }
                if (playerBuild[index].toBuild == Building.Start)
                {
                    rend.mesh = data[0].StartR.mesh;
                }
                if (playerBuild[index].toBuild == Building.Beacon)
                {
                    rend.mesh = data[0].BeaconR.mesh;
                }
                if (playerBuild[index].toBuild == Building.Beacon)
                {
                    rend.mesh = data[0].BeaconR.mesh;
                }

                if (playerBuild[index].toBuild == Building.IrradiatedGenerator)
                {
                    rend.mesh = data[0].IrradiatedR.mesh;
                }
                if (playerBuild[index].toBuild == Building.GeothermalGenerator)
                {
                    rend.mesh = data[0].GeothermalR.mesh;
                }
                if (playerBuild[index].toBuild == Building.WindPowerPlant)
                {
                    rend.mesh = data[0].WindR.mesh;
                }
                if (playerBuild[index].toBuild == Building.FissionGenerator)
                {
                    rend.mesh = data[0].FissionR.mesh;
                }

                if (playerBuild[index].toBuild == Building.PhotiteMiner)
                {
                    rend.mesh = data[0].PhotiteMinerR.mesh;
                }
                if (playerBuild[index].toBuild == Building.NaniteMaker)
                {
                    rend.mesh = data[0].NaniteMakerR.mesh;
                }

                if (playerBuild[index].toBuild == Building.DuriumMiner)
                {
                    rend.mesh = data[0].DuriumMinerR.mesh;
                }
                if (playerBuild[index].toBuild == Building.ZoriumMaker)
                {
                    rend.mesh = data[0].ZoriumMakerR.mesh;
                }

                if (playerBuild[index].toBuild == Building.VionMiner)
                {
                    rend.mesh = data[0].VionsMinerR.mesh;
                }
                if (playerBuild[index].toBuild == Building.NornMaker)
                {
                    rend.mesh = data[0].NornMakerR.mesh;
                }

                if (playerBuild[index].toBuild == Building.TechCentre)
                {
                    rend.mesh = data[0].TechR.mesh;
                }
                if (playerBuild[index].toBuild == Building.TransmitterFactory)
                {
                    rend.mesh = data[0].TransmitterFactoryR.mesh;
                }
                if (playerBuild[index].toBuild == Building.M_ReadvancementCentre)
                {
                    rend.mesh = data[0].ReadvancementCentreR.mesh;
                }

                p.Value = playerBuild[0].worldPos;

                //see if it can be placed in viable location, try to check player input and spawn here or other system
                byte cantPlace = 1;

                for (int i = 0; i < colliderLength; i++)
                {
                    //stop self comparison
                    if (radius[0].manualID==colliderRadius[i].manualID)
                    {
                        continue;
                    }

                    float dist = math.distance(p.Value, colliderPos[i].Value);
                    float collisionRadius = radius[0].buildingRadius + colliderRadius[i].buildingRadius;

                    //Debug.Log("x");

                    if (collisionRadius >= dist)
                    {
                        //Debug.Log(colliderPos[i].Value);
                        //Debug.Log(dist);
                        //Debug.Log(collisionRadius);
                        //Debug.Log("x");

                        cantPlace = 2;
                        break;
                        //turn red, fail place
                        //else allow place
                    }
                }

                if (cantPlace == 1)
                {
                    /*
                    //set material only on change, compare and change previous state
                    if (cantPlace != b.stateValidOrInvalid)
                    {
                        //Debug.Log("placeable");
                        b.stateValidOrInvalid = 1;
                        rend.material = data[0].validHologramMat;
                        buffer.SetSharedComponent(ghostself[0], rend);
                        ghostBuilding[0] = b;
                    }
                    */
                    b.stateValidOrInvalid = 1;
                    rend.material = data[0].validHologramMat;
                    buffer.SetSharedComponent(ghostself[0], rend);
                    ghostBuilding[0] = b;

                    if (build==1)
                    {
                        buffer.CreateEntity(buildSite);
                        buffer.SetComponent(p);
                        buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = data[0].buildSiteMesh , material = data[0].buildSiteMaterial});

                        buffer.SetComponent(new Square { length =  data[0].buildSiteSquareRadius });

                        Build tBuild;
                        DynamicBuffer<NeededItemsBuffer> nib;

                        if (playerBuild[0].toBuild == Building.Start)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1 , toBuild = Building.Start  });
                        }

                        if (playerBuild[0].toBuild == Building.IrradiatedGenerator)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1 , toBuild = Building.IrradiatedGenerator });
                        }
                        if (playerBuild[0].toBuild == Building.WindPowerPlant)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.WindPowerPlant, affinityDemand = RevealType.Hill});
                        }
                        if (playerBuild[0].toBuild == Building.GeothermalGenerator)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1 , toBuild = Building.GeothermalGenerator, affinityDemand = RevealType.GeothermalZone });
                        }
                        if (playerBuild[0].toBuild == Building.FissionGenerator)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.FissionGenerator });
                        }
                        if (playerBuild[0].toBuild == Building.Farm)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.Farm });
                        }
                        if (playerBuild[0].toBuild == Building.DiamondMine)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.DiamondMine /*, affinityDemand = RevealType.DroneMaterial*/});
                        }
                        if (playerBuild[0].toBuild == Building.Beacon)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.Beacon , affinityNormal = RevealType.BoostZone});
                        }
                        if (playerBuild[0].toBuild == Building.TransmitterFactory)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.TransmitterFactory});
                        }
                        if (playerBuild[0].toBuild == Building.LevetationFactory)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.LevetationFactory});
                        }

                        if (playerBuild[0].toBuild == Building.TechCentre)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.TechCentre  , affinityDemand = RevealType.TechZone});
                        }

                        //res miners
                        if (playerBuild[0].toBuild == Building.PhotiteMiner)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.PhotiteMiner , affinityDemand = RevealType.PhotiteZone });
                        }
                        if (playerBuild[0].toBuild == Building.DuriumMiner)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.DuriumMiner , affinityDemand = RevealType.DuriumZone});
                        }
                        if (playerBuild[0].toBuild == Building.VionMiner)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.VionMiner , affinityDemand = RevealType.VionsZone });
                        }
                        //res upgrades
                        if (playerBuild[0].toBuild == Building.NaniteMaker)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.NaniteMaker, affinityDemand = RevealType.PhotiteZone });
                        }
                        if (playerBuild[0].toBuild == Building.ZoriumMaker)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.ZoriumMaker, affinityDemand = RevealType.DuriumZone });
                        }
                        if (playerBuild[0].toBuild == Building.NornMaker)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.NornMaker, affinityDemand = RevealType.VionsZone });
                        }

                        //monuments
                        if (playerBuild[0].toBuild == Building.M_ReadvancementCentre)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.M_ReadvancementCentre});
                        }
                        if (playerBuild[0].toBuild == Building.M_GrandBeacon)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.M_GrandBeacon });
                        }

                        if (playerBuild[0].toBuild == Building.Idle)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 2, toBuild = Building.Idle });
                        }
                        else if (playerBuild[0].toBuild == Building.DroneFactory)
                        {
                        }
                        /*
                        else if (playerBuild[0].toBuild == Building.WorkerDroneFacility)
                        {
                            tBuild = allBuild[data[0].workerDroneFacility];
                            buffer.SetComponent(tBuild);

                            nib = buffer.SetBuffer<NeededItemsBuffer>();

                            for (int t = 0; t < allNeededItems[data[0].workerDroneFacility].Length; t++)
                            {
                                nib.Add(allNeededItems[data[0].workerDroneFacility][t]);
                            }
                        }
                        */
                        else if (playerBuild[0].toBuild == Building.VictoryMonument)
                        {
                            buffer.SetComponent(new DroneBufferData { maxDrones = 0 });
                            buffer.SetComponent(new Build { spawnBuilding = 1, toBuild = Building.VictoryMonument});
                        }

                        //buffer.SetComponent(new DroneBufferData { maxDrones = 10 });
                        buffer.SetSharedComponent(new RadiusSet {buildingRadius=6f , clickRadius=5f });

                        //if its not multiple then disable build move after placement
                        PlayerBuild pb = playerBuild[0];
                        if (multiple == 0)
                        {
                            pb.toBuild = Building.Null;
                        }
                        playerBuild[0] = pb;
                    }
                }
                else if (cantPlace == 2)
                {
                    b.stateValidOrInvalid = 2;
                    rend.material = data[0].invalidHologramMat;
                    buffer.SetSharedComponent(ghostself[0], rend);
                    ghostBuilding[0] = b;
                    /*
                    //set material only on change
                    if (cantPlace != b.stateValidOrInvalid)
                    {
                        //Debug.Log("cantplace");
                        b.stateValidOrInvalid = 2;
                        rend.material = data[0].invalidHologramMat;
                        buffer.SetSharedComponent(ghostself[0], rend);
                        ghostBuilding[0] = b;
                    }
                    */
                }
            }
            else
            {
                p.Value = new float3(0, 0, 0);
            }
            //pos[0] = p;
            buffer.SetComponent(ghostself[0],p);
        }
    }
}
