﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class SFarm : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Harvest
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Farm> harvest;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserMoney> money;
    }

    struct _Time
    {
        [ReadOnly] public ComponentDataArray<GD_Time> time;
    }

    [Inject] _Time _time;

    [Inject] _User _user;
    [Inject] _Harvest _harvest;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            money = _user.money,

            Length = _harvest.Length,
            salvageSelf = _harvest.self,
            harvest = _harvest.harvest,

            deltaTime = Time.deltaTime * _time.time[0].timeSpeed,
        };
        arrivalHandle = job.Schedule(_user.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public ComponentDataArray<UserMoney> money;

        public int Length;
        public EntityArray salvageSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Farm> harvest;

        public float deltaTime;

        public void Execute(int index)
        {
            for (int i= 0; i<Length;i++)
            {
                Farm h = harvest[i];

                h.currentTime += deltaTime;

                if (h.currentTime > h.maxTime)
                {
                    h.currentTime = 0;
                    //h.harvestingAmmount++;

                    UserMoney um = money[index];
                    um.current += harvest[i].gain;
                    money[index] = um;
                }

                harvest[i] = h;
            }
        }
    }
}
