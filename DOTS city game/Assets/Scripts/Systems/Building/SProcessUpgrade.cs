﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class SProcessUpgrade : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Builds
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Status> status;
        [ReadOnly] public BufferArray<Upgradeable> upgradeable;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserEletricity> power;
        public ComponentDataArray<UserMoney> money;
        public ComponentDataArray<UserTransmitters> transmitters;
        public ComponentDataArray<UserLevetations> levetations;

        public ComponentDataArray<UserPhotite> photite;
        public ComponentDataArray<UserNanites> nanites;

        public ComponentDataArray<UserDurium> durium;
        public ComponentDataArray<UserZorium> zorium;

        public ComponentDataArray<UserVions> vions;
        public ComponentDataArray<UserNorn> norn;

        public ComponentDataArray<UserPrefabs> prefabs;
        public ComponentDataArray<UserQuests> quests;
        public ComponentDataArray<UserUnlocks> unlocks;
        public ComponentDataArray<UserReadvancementPoints> points;
    }
    
    struct _Player
    {
        public ComponentDataArray<PlayerAffectSelected> affect;
        [ReadOnly] public ComponentDataArray<PlayerData> data;
        public ComponentDataArray<PlayerOther> other;
    }

    [Inject] _User _user;
    [Inject] _Builds _builds;
    [Inject] _Player _player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            power = _user.power,
            money = _user.money,
            transmitters = _user.transmitters,
            levetations = _user.levetations,
            photite = _user.photite,
            nanites = _user.nanites,
            durium = _user.durium,
            zorium = _user.zorium,
            vions = _user.vions,
            norn = _user.norn,

            prefabs = _user.prefabs,
            quests = _user.quests,
            unlocks = _user.unlocks,
            points = _user.points,

            Length = _builds.Length,
            self = _builds.self,
            status = _builds.status,
            upgradeable = _builds.upgradeable,

            affect = _player.affect,
            data = _player.data,
            other = _player.other,
        };
        arrivalHandle = job.Schedule(_user.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public ComponentDataArray<UserEletricity> power;
        public ComponentDataArray<UserMoney> money;
        public ComponentDataArray<UserTransmitters> transmitters;
        public ComponentDataArray<UserLevetations> levetations;

        public ComponentDataArray<UserPhotite> photite;
        public ComponentDataArray<UserNanites> nanites;

        public ComponentDataArray<UserDurium> durium;
        public ComponentDataArray<UserZorium> zorium;

        public ComponentDataArray<UserVions> vions;
        public ComponentDataArray<UserNorn> norn;

        public ComponentDataArray<UserPrefabs> prefabs;
        public ComponentDataArray<UserQuests> quests;
        public ComponentDataArray<UserUnlocks> unlocks;
        public ComponentDataArray<UserReadvancementPoints> points;

        public int Length;
        public EntityArray self;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Status> status;
        [ReadOnly] public BufferArray<Upgradeable> upgradeable;

        [NativeDisableParallelForRestriction] public ComponentDataArray<PlayerAffectSelected> affect;
        [ReadOnly] public ComponentDataArray<PlayerData> data;
        [NativeDisableParallelForRestriction] public ComponentDataArray<PlayerOther> other;

        public void Execute(int index)
        {
            UserEletricity ue = power[index];
            UserMoney um = money[index];
            UserTransmitters ut = transmitters[index];
            UserLevetations ul = levetations[index];
            UserPhotite up = photite[index];
            UserNanites un = nanites[index];
            UserDurium ud = durium[index];
            UserZorium uz = zorium[index];
            UserVions uv = vions[index];
            UserNorn unorn = norn[index];

            UserPrefabs upre = prefabs[index];
            UserQuests uq = quests[index];
            UserUnlocks uu = unlocks[index];
            UserReadvancementPoints urp = points[index];

            for (int i = 0; i < Length; i++)
            {
                Status s = status[i];
                if (s.level + 1 > upgradeable[i].Length - 1)
                {
                    //index too big
                }
                else
                {
                    PlayerAffectSelected pas = affect[0];
                    if (pas.howToAffect == HowToAffect.LevelUp)
                    {
                        if (data[0].selected==self[i])
                        {
                            s.doUpgradeOnce = 1;
                            pas.howToAffect = HowToAffect.None;
                            affect[0] = pas;
                        }
                    }

                    if (s.autoUpgrade==1 || s.doUpgradeOnce==1)
                    {
                        s.doUpgradeOnce = 0;
                        Upgradeable u = upgradeable[i][s.level + 1];

                        //this is long but check if the upgrade is valid for all items then take
                        //if i wanted to do 1 by 1 then each upgrade would be limited to 1 item
                        if (u.itemsNeeded.prefab == PrefabType.None)
                        {
                            if (
                                ue.allocationCurrent >= u.itemsNeeded.energyAllo &&
                                ut.current >= u.itemsNeeded.transmitters &&
                                ul.current >= u.itemsNeeded.levetations &&

                                up.hasCurrent >= u.itemsNeeded.photiteHas &&
                                un.hasCurrent >= u.itemsNeeded.nanitesHas &&
                                ud.hasCurrent >= u.itemsNeeded.duriumHas &&
                                uz.hasCurrent >= u.itemsNeeded.zoriumHas &&
                                uv.hasCurrent >= u.itemsNeeded.vionsHas &&
                                unorn.hasCurrent >= u.itemsNeeded.nornHas &&

                                up.allocationCurrent >= u.itemsNeeded.photiteAllo &&
                                un.allocationCurrent >= u.itemsNeeded.nanitesAllo &&
                                ud.allocationCurrent >= u.itemsNeeded.duriumAllo &&
                                uz.allocationCurrent >= u.itemsNeeded.zoriumAllo &&
                                uv.allocationCurrent >= u.itemsNeeded.vionsAllo &&
                                unorn.allocationCurrent >= u.itemsNeeded.nornAllo
                                )
                            {
                                ue.allocationCurrent -= u.itemsNeeded.energyAllo;
                                ut.current -= u.itemsNeeded.transmitters;
                                ul.current -= u.itemsNeeded.levetations;

                                //has
                                up.hasCurrent -= u.itemsNeeded.photiteHas;
                                un.hasCurrent -= u.itemsNeeded.nanitesHas;
                                ud.hasCurrent -= u.itemsNeeded.duriumHas;
                                uz.hasCurrent -= u.itemsNeeded.zoriumHas;
                                uv.hasCurrent -= u.itemsNeeded.vionsHas;
                                unorn.hasCurrent -= u.itemsNeeded.nornHas;

                                //allo
                                up.allocationCurrent -= u.itemsNeeded.photiteAllo;
                                un.allocationCurrent -= u.itemsNeeded.nanitesAllo;
                                ud.allocationCurrent -= u.itemsNeeded.duriumAllo;
                                uz.allocationCurrent -= u.itemsNeeded.zoriumAllo;
                                uv.allocationCurrent -= u.itemsNeeded.vionsAllo;
                                unorn.allocationCurrent -= u.itemsNeeded.nornAllo;

                                s.level++;
                                s.processUpgraded = 1;
                            }
                        }

                        if (u.itemsNeeded.prefab == PrefabType.TransmitterFactory && upre.transmitterFactories > 0)
                        {
                            upre.transmitterFactories -= 1;
                            s.level++;
                            s.processUpgraded = 1;
                        }

                        if (u.itemsNeeded.prefab == PrefabType.LevetationsFactory && upre.levetationFactories > 0)
                        {
                            upre.levetationFactories -= 1;
                            s.level++;
                            s.processUpgraded = 1;
                        }


                        if (u.itemsNeeded.prefab == PrefabType.GrandBeacon && upre.GrandBeacon > 0)
                        {
                            upre.GrandBeacon-= 1;
                            s.level++;
                            s.processUpgraded = 1;
                        }

                        if (u.itemsNeeded.prefab == PrefabType.VM && upre.VM > 0)
                        {
                            upre.VM -= 1;
                            s.level++;
                            s.processUpgraded = 1;
                        }

                        if (u.itemsNeeded.prefab == PrefabType.ReadvancementCentre && upre.ReadvancementFacility > 0)
                        {
                            upre.ReadvancementFacility -= 1;
                            s.level++;
                            s.processUpgraded = 1;
                        }
                    }
                }

                if (s.processUpgraded == 1)
                {
                    s.processUpgraded = 0;

                    if (s.iAm == Building.Base)
                    {
                        if (s.level == 1)
                        {
                            ut.current += 1;
                            //upre.transmitterFactories += 1;
                            uu.unlockReadvancementsBuilding = 1;
                            upre.ReadvancementFacility += 1;

                            uq._1baseLevel1 = 1;
                        }
                        if (s.level==2)
                        {
                            ut.current += 1;
                            urp.points += 2;
                        }
                    }

                    if (s.iAm == Building.IrradiatedGenerator)
                    {
                        if (s.level == 1)
                        {
                            ue.allocationCurrent += 2;

                            uq._1irradiatedGeneratorLevel1 = 1;
                        }
                    }
                    if (s.iAm == Building.WindPowerPlant)
                    {
                        if (s.level == 1)
                        {
                            ue.allocationCurrent += 5;
                        }
                    }

                    if (s.iAm == Building.GeothermalGenerator)
                    {
                        if (s.level == 1)
                        {
                            ue.allocationCurrent += 11;
                        }
                    }
                    if (s.iAm == Building.FissionGenerator)
                    {
                        if (s.level == 1)
                        {
                            ue.allocationCurrent += 17;
                        }
                    }

                    if (s.iAm == Building.Beacon)
                    {
                        if (s.level == 1)
                        {
                            s.radius = 124;
                            s.reset = 1;

                            PlayerOther po = other[0];
                            po.updateReveal = 1;
                            other[0] = po;
                        }
                    }

                    if (s.iAm == Building.M_GrandBeacon)
                    {
                        PlayerOther po = other[0];
                        po.updateReveal = 1;
                        other[0] = po;

                        s.reset = 1;
                        if (s.level == 1)
                        {
                            //s.radius = 100;
                        }
                        if (s.level==2)
                        {
                            s.radius += 200;
                            uq._UpgradeGrandBeacon = 1;
                        }
                        if (s.level == 3)
                        {
                            s.radius += 300;
                        }
                        if (s.level == 4)
                        {
                            s.radius += 400;
                        }
                        if (s.level == 5)
                        {
                            s.radius += 500;
                        }
                        if (s.level == 6)
                        {
                            s.radius += 600;
                        }
                        if (s.level == 7)
                        {
                            s.radius += 700;
                        }
                        if (s.level == 8)
                        {
                            s.radius += 800;
                        }
                    }

                    if (s.iAm == Building.PhotiteMiner)
                    {
                        if (s.level == 1)
                        {
                            up.allocationCurrent += 4;
                            uq._1photiteMinerLevel1 = 1;
                        }
                    }
                    if (s.iAm == Building.NaniteMaker)
                    {
                        if (s.level == 1)
                        {
                            un.allocationCurrent += 4;
                        }
                    }

                    if (s.iAm == Building.DuriumMiner)
                    {
                        if (s.level == 1)
                        {
                            ud.allocationCurrent += 6;
                        }
                    }
                    if (s.iAm == Building.ZoriumMaker)
                    {
                        if (s.level == 1)
                        {
                            uz.allocationCurrent += 6;
                        }
                    }

                    if (s.iAm == Building.VionMiner)
                    {
                        if (s.level == 1)
                        {
                            uv.allocationCurrent += 8;
                        }
                    }
                    if (s.iAm == Building.NornMaker)
                    {
                        if (s.level == 1)
                        {
                            unorn.allocationCurrent += 8;
                        }
                    }

                    if (s.iAm == Building.TransmitterFactory)
                    {
                        if (s.level == 1)
                        {
                            //ut.current += 5;
                            uq._1transmitterFactoryLevel1 = 1;
                            ut.power += 2f;
                        }
                        if (s.level==2)
                        {
                            //ut.current += 5;
                            ut.power += 3.5f;
                        }
                        if (s.level == 3)
                        {
                            //ut.current += 5;
                            ut.power += 5f;
                        }
                    }

                    if (s.iAm == Building.TechCentre)
                    {
                        if (s.level == 1)
                        {
                            urp.points++;
                        }
                    }

                    if (s.iAm == Building.M_ReadvancementCentre)
                    {
                        if (s.level==1)
                        {
                            uu.unlockReadvancements = 1;
                            uq._1readvancementCentreLevel1 = 1;
                            urp.points += 1;
                        }
                        if (s.level==2)
                        {
                            urp.points += 2;
                        }
                        if (s.level == 3)
                        {
                            urp.points += 3;
                        }
                        if (s.level == 4)
                        {
                            urp.points += 4;
                        }
                        if (s.level == 5)
                        {
                            urp.points += 5;
                        }
                    }

                    if (s.iAm == Building.VictoryMonument)
                    {
                        if (s.level == 1)
                        {
                            uq._1VMLevel1 = 1;
                            uq.TotalVMLevel = 1;
                        }
                        if (s.level == 2)
                        {
                            uq.TotalVMLevel = 2;
                        }
                        if (s.level == 3)
                        {
                            uq.TotalVMLevel = 3;
                        }
                        if (s.level == 4)
                        {
                            uq.TotalVMLevel = 4;
                        }
                        if (s.level == 5)
                        {
                            uq.TotalVMLevel = 5;
                        }
                        if (s.level == 6)
                        {
                            uq.TotalVMLevel = 6;
                        }
                        if (s.level == 7)
                        {
                            uq.TotalVMLevel = 7;
                        }
                    }
                }

                status[i] = s;
            }

            power[index] = ue;
            money[index] = um;
            transmitters[index] = ut;
            levetations[index] = ul;
            photite[index] = up;
            nanites[index] = un;
            durium[index] = ud;
            zorium[index] = uz;
            vions[index] = uv;
            norn[index] = unorn;

            prefabs[index] = upre;
            quests[index] = uq;
            unlocks[index] = uu;
            points[index] = urp;
        }
    }
}
