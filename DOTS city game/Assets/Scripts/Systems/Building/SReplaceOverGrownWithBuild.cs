﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(SBuildManager))]
[UpdateAfter(typeof(STransferDrone))]
public class SReplaceOverGrownWithBuild : ComponentSystem
{
    //public static JobHandle buildHandle;

    struct _OverGrown
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<OverGrown> overGrown;
        public ComponentDataArray<StorageTarget> storage;
        public ComponentDataArray<Position> pos;
        [ReadOnly] public SharedComponentDataArray<RadiusSet> radius;
        public BufferArray<DroneBuffer> droneBuffer;
    }

    struct _BuildData
    {
        //public readonly int Length;
        //public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<BuildingData> buildData;
    }


    struct _Player
    {
        //public readonly int Length;
        //public EntityArray self;
        public ComponentDataArray<PlayerDroneLimits> limits;
    }

    [Inject] _Player _player;
    [Inject] _OverGrown _overGrown;
    [Inject] _BuildData _buildData;

    [Inject] [ReadOnly] ComponentDataFromEntity<Square> allSquare;

    [Inject] [ReadOnly] ComponentDataFromEntity<TransportParking> allTP;
    [Inject] [ReadOnly] ComponentDataFromEntity<Science> allScience;
    [Inject] [ReadOnly] ComponentDataFromEntity<DroneFactory> allDF;
    [Inject] [ReadOnly] ComponentDataFromEntity<Salvage> allSalvage;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        for (int i=0; i<_overGrown.Length;i++)
        {
            OverGrown og = _overGrown.overGrown[i];
            StorageTarget t = _overGrown.storage[i];

            if (og.change==1)
            {
                if (_overGrown.droneBuffer[i].Length == 0)
                {
                    PostUpdateCommands.DestroyEntity(_overGrown.self[i]);
                }
            }
        }
    }
}
