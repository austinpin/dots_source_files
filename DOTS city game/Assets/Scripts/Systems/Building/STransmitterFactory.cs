﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class STransmitterFactory : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _TransmitterFactory
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<TransmitterFactory> factory;
        public ComponentDataArray<Status> status;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserTransmitters> transmitters;
        public ComponentDataArray<UserLevetations> levetations;
    }

    [Inject] _User _user;
    [Inject] _TransmitterFactory _transmitterFactory;

    struct _Time
    {
        [ReadOnly] public ComponentDataArray<GD_Time> time;
    }

    [Inject] _Time _time;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            transmitters = _user.transmitters,
            levetations=_user.levetations,

            Length = _transmitterFactory.Length,
            self2 = _transmitterFactory.self,
            factory = _transmitterFactory.factory,
            status = _transmitterFactory.status,

            deltaTime = Time.deltaTime * _time.time[0].timeSpeed
        };
        arrivalHandle = job.Schedule(_user.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        //public EntityArray self1;
        public ComponentDataArray<UserTransmitters> transmitters;
        public ComponentDataArray<UserLevetations> levetations;

        public int Length;
        public EntityArray self2;
        public ComponentDataArray<TransmitterFactory> factory;
        public ComponentDataArray<Status> status;

        public float deltaTime;

        public void Execute(int index)
        {
            for (int i = 0; i < Length; i++)
            {
                TransmitterFactory tf = factory[i];

                if (status[index].level>0)
                {
                    tf.currentTime += deltaTime;

                    if (tf.currentTime > tf.maxTime)
                    {
                        tf.currentTime = 0;
                        //h.harvestingAmmount++;

                        if (tf.type == 1)
                        {
                            UserTransmitters ut = transmitters[index];
                            //ut.current += tf.gain;
                            if (ut.current > 9999)
                            {
                                ut.current = 9999;
                            }
                            transmitters[index] = ut;
                        }
                        if (tf.type == 2)
                        {
                            UserLevetations ul = levetations[index];
                            //ul.current += tf.gain;
                            if (ul.current > 9999)
                            {
                                ul.current = 9999;
                            }
                            levetations[index] = ul;
                        }
                    }
                }

                factory[i] = tf;
            }
        }
    }
}
