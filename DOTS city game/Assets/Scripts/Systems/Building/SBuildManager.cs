﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(SMovement))]
//[UpdateBefore(typeof(SArrival))]
public class SBuildManager: JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Builds
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Build> build;
        public ComponentDataArray<Status> status;
        [ReadOnly] public BufferArray<NeededItemsBuffer> neededItemsBuffer;
        public BufferArray<DroneBuffer> droneBuffer;
        //public BufferArray<TaskBuffer> taskBuffer;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    [Inject] _Drones _drones;
    [Inject] _Builds _builds;

    [Inject] [ReadOnly] ComponentDataFromEntity<StorageTarget> allStorageTarget;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        BuildJob job = new BuildJob
        {
            buildSelf = _builds.self,
            build = _builds.build,
            status=_builds.status,
            neededItemsBuffer=_builds.neededItemsBuffer,
            droneBuffer = _builds.droneBuffer,
            //taskBuffer = _builds.taskBuffer,

            Length = _drones.Length,
            droneSelf = _drones.self,
            move = _drones.move,
            drone = _drones.drone,

            allStorageTarget=allStorageTarget,

            deltaTime = Time.deltaTime,
        };
        buildHandle = job.Schedule(_builds.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct BuildJob : IJobParallelFor
    {

        public EntityArray buildSelf;
        public ComponentDataArray<Build> build;
        public ComponentDataArray<Status> status;
        [ReadOnly] public BufferArray<NeededItemsBuffer> neededItemsBuffer;
        public BufferArray<DroneBuffer> droneBuffer;
        //public BufferArray<TaskBuffer> taskBuffer;

        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        [ReadOnly] public ComponentDataFromEntity<StorageTarget> allStorageTarget;

        public float deltaTime;

        public void Execute(int index)
        {
            if (status[index].active==0)
            {
                return;
            }

            for (int i = 0; i < Length; i++)
            {
                //Debug.Log("error");
                if (move[i].target == buildSelf[index])
                {
                    Drone d = drone[i];
                    Movement m = move[i];

                    Build b = build[index];

                    //reallocate drones and build structure
                    if (b.buildTime >= b.maxBuildTime)
                    {
                        for (int j = 0; j < droneBuffer[index].Length; j++)
                        {
                            if (droneBuffer[index][j] == droneSelf[i])
                            {
                                d.doChangeJob = 1;
                                d.nextJob = NextJobType.Idle;
                                droneBuffer[index].RemoveAt(j);
                                break;
                            }
                        }
                    }

                    if (move[i].arrive == 1)
                    {
                        int total = neededItemsBuffer[index].Length;
                        int current = 0;

                        for (int j = 0; j < neededItemsBuffer[index].Length; j++)
                        {
                            Storage s = neededItemsBuffer[index][j];
                            if (s.containsAmmount==s.maxContains)
                            {
                                current++;
                            }
                        }

                        if (current==total)
                        {
                            b.buildTime += deltaTime;

                            if (b.buildTime >= b.maxBuildTime)
                            {
                                b.spawnBuilding = 1;
                            }
                        }
                        /*
                        //Logic note
                        //iterate over each item in needed items, grab the first free and ask drone to get

                        if (d.carrying == Item.Metal)
                        {
                            b.metalHas++;
                            d.carrying = Item.Empty;

                            t.taskIndex++;
                            if (t.taskIndex >= tB.Length)
                            {
                                t.taskIndex = 0;
                            }
                        }
                        else
                        {
                            if (b.metalComing < b.metalNeeded)
                            {
                                d.toGet = Item.Metal;
                                b.metalComing++;

                                m.target = st.storageTarget;
                                m.arrive = 0;
                                d.getOrStore = 1;

                                t.taskIndex++;
                                if (t.taskIndex >= tB.Length)
                                {
                                    t.taskIndex = 0;
                                }
                            }
                        }

                        if (b.metalHas==b.metalNeeded)
                        {

                            b.buildTime += deltaTime;

                            if (b.buildTime >= b.maxBuildTime)
                            {
                                b.spawnBuilding = 1;
                            }
                        }
                        */
                    }
                    build[index] = b;
                    move[i] = m;
                    drone[i] = d;
                }
            }
        }
    }
}
