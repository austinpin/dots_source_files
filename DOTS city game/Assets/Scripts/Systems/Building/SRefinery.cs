﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class SRefinery : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Harvest
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Harvester> harvest;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> st;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    [Inject] _Drones _drones;
    [Inject] _Harvest _harvest;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            salvageSelf = _harvest.self,
            harvest = _harvest.harvest,
            status = _harvest.status,
            st = _harvest.st,
            droneBuffer = _harvest.droneBuffer,
            droneBufferData = _harvest.droneBufferData,

            Length = _drones.Length,
            droneSelf = _drones.self,
            move = _drones.move,
            drone = _drones.drone,

            deltaTime = Time.deltaTime,
        };
        arrivalHandle = job.Schedule(_harvest.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public EntityArray salvageSelf;
        public ComponentDataArray<Harvester> harvest;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> st;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;

        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        public float deltaTime;

        public void Execute(int index)
        {

        }
    }
}
