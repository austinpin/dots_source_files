﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

//[UpdateAfter(typeof(STransportParkingItemTransfer))]
//[UpdateAfter(typeof(SStorageTransportTransfer))]
public class STransportDroneSwitch : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _TransportDrones
    {
        public readonly int Length;
        public EntityArray droneSelf;
        public ComponentDataArray<TransportDrone> transportDrone;
        public ComponentDataArray<Movement> move;
    }

    struct _Storages
    {
        public readonly int Length;
        public EntityArray storageSelf;
        [ReadOnly] public ComponentDataArray<Position> storagePos;
        [ReadOnly] public BufferArray<StorageBuffer> storageBuffer;
    }

    [Inject] _TransportDrones _transportDrones;
    [Inject] _Storages _storages;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        SwitchTransportDroneParking job = new SwitchTransportDroneParking
        {
            transportDrone=_transportDrones.transportDrone,
            move = _transportDrones.move,
        };
        buildHandle = job.Schedule(_transportDrones.Length, 1, inputDeps);

        return buildHandle;
    }

    //[BurstCompile]
    struct SwitchTransportDroneParking : IJobParallelFor
    {
        public ComponentDataArray<TransportDrone> transportDrone;
        public ComponentDataArray<Movement> move;

        public void Execute(int index)
        {
            TransportDrone d = transportDrone[index];
            Movement m = move[index];

            if (m.arrive==1)
            {
                if (m.target == d.storageHome)
                {
                    d.parkedWhere = 1;
                }
                if (m.target==d.ParkingTo)
                {
                    d.parkedWhere = 2;
                }

            }

            if (d.switchParked == 1)
            {
                d.switchParked = 0;
                m.arrive = 0;
                /*
                if (d.parkedWhere == 1)
                {
                    m.target = d.ParkingTo;
                }
                if (d.parkedWhere == 2)
                {
                    m.target = d.storageHome;
                }
                */
                m.target = d.storageHome;
                d.parkedWhere = 0;
            }

            transportDrone[index] = d;
            move[index] = m;
        }
    }
}
