﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(STransferDrone))]
public class SGetItems : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _DroneFactory
    {
        public readonly int Length;
        public EntityArray self;
        //public ComponentDataArray<DroneFactory> droneFactory;
        [ReadOnly] public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> storageTarget;
        public BufferArray<NeededItemsBuffer> neededItemsBuffer;
        //[ReadOnly] public ComponentDataArray<Position> pos;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray droneSelf;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    [Inject] _Drones _drones;
    [Inject] _DroneFactory _droneFactory;

    // Start is called before the first frame update
    void Start()
    {
        Enabled = false;
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        GetItemsJob job = new GetItemsJob
        {
            self = _droneFactory.self,
            //droneFactory = _droneFactory.droneFactory,
            status=_droneFactory.status,
            storageTarget=_droneFactory.storageTarget,
            neededItemsBuffer = _droneFactory.neededItemsBuffer,

            Length=_drones.Length,
            droneSelf=_drones.droneSelf,
            move=_drones.move,
            drone=_drones.drone,

            deltaTime = Time.deltaTime,
        };
        buildHandle = job.Schedule(_droneFactory.Length, 1, inputDeps);

        return buildHandle;
    }

    //[BurstCompile]
    struct GetItemsJob : IJobParallelFor
    {
        public EntityArray self;
        //[ReadOnly] public ComponentDataArray<DroneFactory> droneFactory;
        [ReadOnly] public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> storageTarget;
        [ReadOnly] public BufferArray<NeededItemsBuffer> neededItemsBuffer;

        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        public float deltaTime;

        public void Execute(int index)
        {
            if (status[index].active == 0)
            {
                return;
            }

            for (int i = 0; i < Length; i++)
            {
                if (move[i].arrive == 1)
                {
                    if (move[i].target == self[index])
                    {
                        Drone d = drone[i];
                        Movement m = move[i];

                        Item item = drone[i].carrying;

                        for (int j = 0; j < neededItemsBuffer[index].Length; j++)
                        {
                            if (neededItemsBuffer[index][j].storage.itemStoring == item)
                            {
                                Storage s = neededItemsBuffer[index][j];
                                if (s.containsAmmount < s.maxContains)
                                {
                                    s.containsAmmount++;
                                    s.itemsComing--;
                                    neededItemsBuffer[index].RemoveAt(j);
                                    neededItemsBuffer[index].Insert(j, s);

                                    d.carrying = Item.Empty;
                                    d.getOrStore = 0;

                                    //m.target = t.target;
                                    //m.arrive = 0;

                                    /*
                                    //do next task
                                    t.taskIndex++;
                                    if (t.taskIndex >= tB.Length)
                                    {
                                        t.taskIndex = 0;
                                    }
                                    */
                                }
                                break;
                            }
                        }

                        //Tell drone to get the items i need
                        //iterate over each storage item that i need going over each item after the last.

                        for (int j = 0; j < neededItemsBuffer[index].Length; j++)
                        {
                            Storage s = neededItemsBuffer[index][j];
                            if ((s.containsAmmount + s.itemsComing)<s.maxContains)
                            {
                                d.toGet = s.itemStoring;
                                m.arrive = 0;
                                d.getOrStore = 1;
                                m.target = storageTarget[index].storageTarget;

                                s.itemsComing++;
                                neededItemsBuffer[index].RemoveAt(j);
                                neededItemsBuffer[index].Insert(j, s);

                                break;
                            }
                        }

                        move[i] = m;
                        drone[i] = d;
                    }
                }
            }
        }
    }
}
