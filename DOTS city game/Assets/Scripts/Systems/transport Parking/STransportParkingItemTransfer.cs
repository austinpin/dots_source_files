﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(STransferDrone))]
[UpdateAfter(typeof(SMovement))]
[UpdateBefore(typeof(STransportDroneSwitch))]
public class STransportParkingItemTransfer : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Drones
    {
        public readonly int Length;
        public EntityArray droneSelf;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    struct _TransportDrones
    {
        public readonly int Length;
        public EntityArray droneSelf;
        public ComponentDataArray<TransportDrone> transportDrone;
        [ReadOnly] public ComponentDataArray<Movement> move;
        public BufferArray<TransportDroneBuffer> transportDroneStorage;
    }

    struct _TransportParking
    {
        public readonly int Length;
        public EntityArray transportSelf;
        public BufferArray<TransportParkingBuffer> transportParkingStorage;
        public ComponentDataArray<TransportParking> transportParking;
        public BufferArray<TPNeededItemsBuffer> neededItemsBuffer;
    }

    [Inject] _Drones _drones;
    [Inject] _TransportDrones _transportDrones;
    [Inject] _TransportParking _transportParking;

    [Inject] [ReadOnly] ComponentDataFromEntity<Build> allBuild;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        TransportParkingItemTransferJob job = new TransportParkingItemTransferJob
        {
            droneLength = _drones.Length,
            droneSelf = _drones.droneSelf,
            droneMove = _drones.move,
            drone = _drones.drone,

            transportSelf = _transportParking.transportSelf,
            transportParkingStorage = _transportParking.transportParkingStorage,
            transportParking = _transportParking.transportParking,
        };
        JobHandle Handle = job.Schedule(_transportParking.Length, 1, inputDeps);

        TransportParkingItemTransfer2Job job2 = new TransportParkingItemTransfer2Job
        {
            transportDroneLength = _transportDrones.Length,
            transportDroneSelf = _transportDrones.droneSelf,
            transportDrone = _transportDrones.transportDrone,
            transportDronemove = _transportDrones.move,
            transportDroneStorage = _transportDrones.transportDroneStorage,

            transportSelf = _transportParking.transportSelf,
            transportParkingStorage = _transportParking.transportParkingStorage,
            transportParking = _transportParking.transportParking,
            neededItemsBuffer=_transportParking.neededItemsBuffer,

            allBuild = allBuild,
        };
        buildHandle = job2.Schedule(_transportParking.Length, 1, Handle);

        return buildHandle;
    }

    [BurstCompile]
    struct TransportParkingItemTransferJob : IJobParallelFor
    {

        public int droneLength;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> droneMove;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        public EntityArray transportSelf;
        public BufferArray<TransportParkingBuffer> transportParkingStorage;
        public ComponentDataArray<TransportParking> transportParking;

        public void Execute(int index)
        {
            //parking will take items from drone
            //parking will also give items to transport drones
            //as there can be 2 transports per parking as well

            if (transportParking[index].active == 0)
            {
                return;
            }

            for (int i = 0; i < droneLength; i++)
            {
                if (droneMove[i].target == transportSelf[index])
                {
                    if (droneMove[i].arrive == 1)
                    {
                        if (drone[i].getOrStore == 2)
                        {
                            Drone d = drone[i];
                            Movement m = droneMove[i];

                            Item item = drone[i].carrying;

                            bool valid = false;

                            for (int j = 0; j < transportParkingStorage[index].Length; j++)
                            {

                                if (transportParkingStorage[index][j].storage.itemStoring == item)
                                {
                                    TransportParking p = transportParking[index];
                                    Storage s = transportParkingStorage[index][j];

                                    valid = true;

                                    if (p.currentItems < p.maxItems)
                                    {
                                        p.currentItems++;
                                        s.containsAmmount++;
                                        transportParkingStorage[index].RemoveAt(j);
                                        transportParkingStorage[index].Insert(j, s);

                                        d.carrying = Item.Empty;
                                        d.getOrStore = 0;

                                        m.target = d.workEntity;
                                        m.arrive = 0;

                                        droneMove[i] = m;
                                        drone[i] = d;

                                        transportParking[index] = p;

                                        break;
                                    }
                                }
                            }

                            //add storage type
                            if (valid == false)
                            {
                                Storage s2 = new Storage { itemStoring = item, containsAmmount = 1 };

                                TransportParking p = transportParking[index];
                                p.currentItems++;
                                transportParking[index] = p;

                                transportParkingStorage[index].Add(s2);
                            }
                        }
                        //
                        //Segment
                        //
                        if (drone[i].getOrStore == 1)
                        {
                            for (int j = 0; j < transportParkingStorage[index].Length; j++)
                            {
                                if (transportParkingStorage[index][j].storage.itemStoring == drone[i].toGet)
                                {
                                    Storage s = transportParkingStorage[index][j];
                                    if (s.containsAmmount > 0)
                                    {
                                        Drone d = drone[i];
                                        Movement m = droneMove[i];
                                        TransportParking p = transportParking[index];

                                        p.currentItems--;
                                        s.containsAmmount--;
                                        transportParkingStorage[index].RemoveAt(j);
                                        transportParkingStorage[index].Insert(j, s);

                                        d.carrying = s.itemStoring;
                                        d.getOrStore = 0;
                                        d.toGet = Item.Empty;

                                        m.arrive = 0;
                                        m.target = d.workEntity;

                                        droneMove[i] = m;
                                        drone[i] = d;
                                        transportParking[index] = p;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    [BurstCompile]
    struct TransportParkingItemTransfer2Job : IJobParallelFor
    {
        public int transportDroneLength;
        public EntityArray transportDroneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<TransportDrone> transportDrone;
        [ReadOnly] public ComponentDataArray<Movement> transportDronemove;
        [NativeDisableParallelForRestriction] public BufferArray<TransportDroneBuffer> transportDroneStorage;

        public EntityArray transportSelf;
        public BufferArray<TransportParkingBuffer> transportParkingStorage;
        public ComponentDataArray<TransportParking> transportParking;
        public BufferArray<TPNeededItemsBuffer> neededItemsBuffer;

        [ReadOnly] public ComponentDataFromEntity<Build> allBuild;

        public void Execute(int index)
        {
            if (transportParking[index].active == 0)
            {
                return;
            }

            TransportParking p = transportParking[index];
            for (int i = 0; i < transportDroneLength; i++)
            {
                if (transportDronemove[i].target == transportSelf[index])
                {
                    TransportDrone td = transportDrone[i];
                    td.switchParked = 0;
                    if (transportDronemove[i].arrive == 1)
                    {

                        p.itemsAboutToBeTaken -= td.maxItems; //Do this once otherwise it will take away too many,
                        td.switchParked = 1; //also switch instantly, dont need to nest in due to optimal item collection
                        td.StoreOrGet = 1; //For storage system to work based on segment 

                        if (p.StoreOrGet == 1)
                        {
                            for (int j = 0; j < transportParkingStorage[index].Length; j++)
                            {
                                if (transportParkingStorage[index][j].storage.containsAmmount == 0)
                                {
                                    continue;
                                }
                                bool valid = false;
                                for (int k = 0; k < transportDroneStorage[i].Length; k++)
                                {
                                    //check to see if it has an item type and increment that
                                    //if it does not have the same item then add this item type
                                    if (transportParkingStorage[index][j].storage.itemStoring == transportDroneStorage[i][k].storage.itemStoring)
                                    {
                                        Storage s0 = transportDroneStorage[i][k].storage;
                                        Storage s1 = transportParkingStorage[index][j].storage;

                                        valid = true;

                                        if (td.currentItems < td.maxItems)
                                        {
                                            int diff = td.maxItems - td.currentItems;

                                            if (s1.containsAmmount <= diff)
                                            {
                                                s0.containsAmmount += s1.containsAmmount;
                                                td.currentItems += s1.containsAmmount;
                                                p.currentItems -= s1.containsAmmount;
                                                s1.containsAmmount = 0;
                                            }
                                            else
                                            {
                                                s0.containsAmmount += (ushort)diff;
                                                td.currentItems += diff;
                                                p.currentItems -= diff;
                                                s1.containsAmmount -= (ushort)diff;
                                            }
                                            transportDroneStorage[i].RemoveAt(k);
                                            transportDroneStorage[i].Insert(k, s0);

                                            transportParkingStorage[index].RemoveAt(j);
                                            transportParkingStorage[index].Insert(j, s1);
                                        }

                                        break;
                                    }
                                }

                                //add storage type
                                if (valid == false)
                                {
                                    Storage s2 = transportParkingStorage[index][j].storage;

                                    transportDroneStorage[i].Add(new Storage { itemStoring = s2.itemStoring });

                                    j--;
                                    /*
                                    td.currentItems += s2.containsAmmount;
                                    p.currentItems -= s2.containsAmmount;

                                    s2.containsAmmount = 0;

                                    transportParkingStorage[index].RemoveAt(j);
                                    transportParkingStorage[index].Insert(j, s2);

                                    td.StoreOrGet = 1;
                                    td.switchParked = 1;
                                    p.itemsAboutToBeTaken -= td.maxItems;
                                    */
                                }
                            }
                            transportParking[index] = p;
                        }
                        if (p.StoreOrGet == 2)
                        {
                            for (int k = 0; k < transportDroneStorage[i].Length; k++)
                            {
                                bool valid = false;

                                //Give items the drone has
                                for (int j = 0; j < transportParkingStorage[index].Length; j++)
                                {
                                    //check to see if it has an item type an increment that
                                    //if it does not have the same item then add this item type
                                    if (transportParkingStorage[index][j].storage.itemStoring == transportDroneStorage[i][k].storage.itemStoring)
                                    {
                                        Storage s0 = transportDroneStorage[i][k].storage;
                                        Storage s1 = transportParkingStorage[index][j].storage;

                                        valid = true;

                                        //add items to needed items buffer first then main inventory as normal after
                                        for (int n = 0; n < neededItemsBuffer[index].Length; n++)
                                        {
                                            Storage s = neededItemsBuffer[index][n];
                                            if (s.itemStoring == transportDroneStorage[i][k].storage.itemStoring)
                                            {
                                                s.itemsComing -= s0.containsAmmount;
                                                s.maxContains -= s0.containsAmmount;
                                                //s.containsAmmount += s0.containsAmmount;

                                                neededItemsBuffer[index].RemoveAt(n);
                                                neededItemsBuffer[index].Insert(n, s);
                                            }
                                        }

                                        //add to main inventory
                                        if (p.currentItems < p.maxItems)
                                        {
                                            int diff = p.maxItems - p.currentItems;

                                            if (s0.containsAmmount <= diff)
                                            {
                                                s1.containsAmmount += s0.containsAmmount;
                                                p.currentItems += s0.containsAmmount;
                                                td.currentItems -= s0.containsAmmount;
                                                //p.metalComing -= s0.containsAmmount;
                                                s0.containsAmmount = 0;
                                            }
                                            else
                                            {
                                                s1.containsAmmount += (ushort)diff;
                                                p.currentItems += diff;
                                                td.currentItems -= diff;
                                                //p.metalComing -= (ushort)diff;
                                                s0.containsAmmount -= (ushort)diff;
                                            }

                                            transportDroneStorage[i].RemoveAt(k);
                                            transportDroneStorage[i].Insert(k, s0);

                                            transportParkingStorage[index].RemoveAt(j);
                                            transportParkingStorage[index].Insert(j, s1);
                                        }
                                    }
                                }
                                //add storage type
                                if (valid == false)
                                {
                                    Storage s2 = transportDroneStorage[i][k].storage;

                                    transportParkingStorage[index].Add(new Storage { itemStoring = s2.itemStoring });

                                    k--;

                                    /*
                                    td.currentItems -= s2.containsAmmount;
                                    p.currentItems += s2.containsAmmount;

                                    s2.containsAmmount = 0;

                                    transportDroneStorage[i].RemoveAt(k);
                                    transportDroneStorage[i].Insert(k, s2);
                                    */
                                }
                            }

                            /*
                            //ask drones to get items
                            for (int j = 0; j < neededItemsBuffer[index].Length; j++)
                            {
                                Storage s = neededItemsBuffer[index][j];
                                int dif = s.maxContains - (s.containsAmmount + s.itemsComing);
                                if (dif>0)
                                {
                                    td.switchParked = 1; //enabled here
                                    td.itemToGet = s.itemStoring;
                                    td.howManyOfItemToGet += (ushort) dif;

                                    s.itemsComing += (ushort) dif;
                                    neededItemsBuffer[index].RemoveAt(j);
                                    neededItemsBuffer[index].Insert(j, s);

                                    break;
                                }
                            }
                            */

                            td.StoreOrGet = 2;
                            td.switchParked = 1;
                            transportParking[index] = p;
                        }
                    }
                    transportDrone[i] = td;
                }
            }
        }
    }
}