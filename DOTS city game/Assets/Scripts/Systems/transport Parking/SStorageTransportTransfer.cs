﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(STransportParkingItemTransfer))]
[UpdateBefore(typeof(STransportDroneSwitch))]
public class SStorageTransportTransfer: JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _Storages
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<StorageBuffer> storage;
    }

    struct _TransportDrones
    {
        public readonly int Length;
        public EntityArray droneSelf;
        public ComponentDataArray<TransportDrone> transportDrone;
        [ReadOnly] public ComponentDataArray<Movement> move;
        public BufferArray<TransportDroneBuffer> transportDroneStorage;
    }

    [Inject] _Storages _storages;
    [Inject] _TransportDrones _transportDrones;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        StorageTransferJob job = new StorageTransferJob
        {
            self = _storages.self,
            storage = _storages.storage,

            Length=_transportDrones.Length,
            transportSelf = _transportDrones.droneSelf,
            transportDrone=_transportDrones.transportDrone,
            move=_transportDrones.move,
            transportDroneStorage=_transportDrones.transportDroneStorage,
        };
        buildHandle = job.Schedule(_storages.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct StorageTransferJob : IJobParallelFor
    {
        public EntityArray self;
        public BufferArray<StorageBuffer> storage;

        public int Length;
        public EntityArray transportSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<TransportDrone> transportDrone;
        [ReadOnly] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public BufferArray<TransportDroneBuffer> transportDroneStorage;

        public void Execute(int index)
        {
            for (int i = 0; i < Length; i++)
            {
                if (transportDrone[i].modeIdle==1)
                {
                    continue;
                }

                if (move[i].target == self[index])
                {
                    if (move[i].arrive == 1)
                    {
                        TransportDrone td = transportDrone[i];
                        td.switchParked = 0;
                        td.modeIdle = 1;

                        if (td.StoreOrGet == 1)
                        {
                            //td.modeIdle = 1;
                            for (int j = 0; j < transportDroneStorage[i].Length; j++)
                            {
                                bool valid = false;
                                for (int k = 0; k < storage[index].Length; k++)
                                {
                                    if (storage[index][k].storage.itemStoring == transportDroneStorage[i][j].storage.itemStoring)
                                    {
                                        valid = true;

                                        Storage s = storage[index][k];
                                        Storage s2 = transportDroneStorage[i][j];

                                        if (s2.containsAmmount==0)
                                        {
                                            break;
                                            //go to next item
                                        }

                                        if (s.containsAmmount<s.maxContains)
                                        {

                                            int diff = s.maxContains - s.containsAmmount;

                                            if (s2.containsAmmount <= diff)
                                            {
                                                s.containsAmmount += s2.containsAmmount;
                                                td.currentItems -= s2.containsAmmount;
                                                s2.containsAmmount = 0;
                                            }

                                            else
                                            {
                                                s.containsAmmount += (ushort)diff;
                                                td.currentItems -= diff;
                                                s2.containsAmmount -= (ushort)diff;
                                            }

                                            storage[index].RemoveAt(k);
                                            storage[index].Insert(k, s);

                                            transportDroneStorage[i].RemoveAt(j);
                                            transportDroneStorage[i].Insert(j, s2);
                                        }

                                        break;
                                    }
                                }
                                //add storage type
                                if (valid == false)
                                {
                                    Storage s2 = transportDroneStorage[i][j];

                                    s2.maxContains = 500;

                                    storage[index].Add(s2);

                                    td.currentItems -= s2.containsAmmount;

                                    s2.containsAmmount = 0;

                                    transportDroneStorage[i].RemoveAt(j);
                                    transportDroneStorage[i].Insert(j, s2);
                                }
                            }
                            //td.switchParked = 1;
                        }
                        /*
                        if (td.StoreOrGet == 2)
                        {
                            for (int k = 0; k < storage[index].Length; k++)
                            {
                                bool valid = false;

                                for (int j = 0; j < transportDroneStorage[i].Length; j++)
                                {
                                    if (storage[index][k].storage.itemStoring == transportDroneStorage[i][j].storage.itemStoring)
                                    {
                                        //ordering is important here, imagine what would happen if this was below the break
                                        //It would be valid false, so even though it has the storage(item) it would add a duplicate which is always a bug
                                        valid = true;

                                        if (storage[index][k].storage.itemStoring != td.itemToGet)
                                        {
                                            break;
                                        }

                                        Storage s = storage[index][k];
                                        Storage s2 = transportDroneStorage[i][j];

                                        if (s.containsAmmount == 0)
                                        {
                                            break;
                                            //go to next item
                                        }
                                        //td.switchParked = 1;

                                        if (td.currentItems < td.maxItems)
                                        {
                                            int diff = td.maxItems - td.currentItems;
                                            if (diff >= td.howManyOfItemToGet)
                                            {
                                                diff = td.howManyOfItemToGet;
                                            }

                                            if (s.containsAmmount <= diff)
                                            {
                                                s2.containsAmmount += s.containsAmmount;
                                                td.howManyOfItemToGet -= s.containsAmmount;
                                                td.currentItems += s.containsAmmount;
                                                s.containsAmmount = 0;
                                            }
                                            else
                                            {
                                                s2.containsAmmount += (ushort)diff;
                                                td.howManyOfItemToGet -= (ushort)diff;
                                                td.currentItems += diff;
                                                s.containsAmmount -= (ushort)diff;
                                            }
                                            storage[index].RemoveAt(k);
                                            storage[index].Insert(k, s);

                                            transportDroneStorage[i].RemoveAt(j);
                                            transportDroneStorage[i].Insert(j, s2);
                                        }

                                        break;
                                    }
                                }

                                //add storage type
                                if (valid == false)
                                {
                                    Storage s2 = storage[index][k].storage;
                                    Storage givenStorage = s2;

                                    //this one is different from other storage adds as taking too many items can cause issues with the other system ("Stransportparkingitemtransfer")

                                    int diff = td.maxItems - td.currentItems;
                                    if (diff >= td.howManyOfItemToGet)
                                    {
                                        diff = td.howManyOfItemToGet;
                                    }

                                    if (s2.containsAmmount<=diff)
                                    {
                                        transportDroneStorage[i].Add(s2);
                                        td.howManyOfItemToGet -= s2.containsAmmount;
                                        td.currentItems += s2.containsAmmount;
                                        s2.containsAmmount = 0;
                                    }
                                    else
                                    {
                                        givenStorage.containsAmmount = (ushort) diff;
                                        transportDroneStorage[i].Add(givenStorage);
                                        td.howManyOfItemToGet -= (ushort)diff;
                                        td.currentItems += diff;
                                        s2.containsAmmount -= (ushort)diff;
                                    }

                                    //td.switchParked = 1;

                                    storage[index].RemoveAt(k);
                                    storage[index].Insert(k, s2);
                                }
                            }
                        }
                        */
                        transportDrone[i] = td;
                    }
                }
            }
        }
    }
}
