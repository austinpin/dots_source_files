﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Rendering;

[UpdateAfter(typeof(SDroneFactory))]
public class STransportParkingAssignNIB : JobComponentSystem
{
    public static JobHandle buildHandle;

    struct _TransportParking
    {
        public readonly int Length;
        public EntityArray transportSelf;
        public BufferArray<TransportParkingBuffer> transportParkingStorage;
        public ComponentDataArray<TransportParking> transportParking;
        public BufferArray<TPNeededItemsBuffer> TPneededItemsBuffer;
    }

    struct _NIB
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<StorageTarget> storageTarget;
        public BufferArray<NeededItemsBuffer> neededItemsBuffer;
    }

    [Inject] _TransportParking _transportParking;
    [Inject] _NIB _nib;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        TransportParkingNeedsMoreItemsJob job = new TransportParkingNeedsMoreItemsJob
        {
            Length=_transportParking.Length,
            transportSelf=_transportParking.transportSelf,
            transportParkingStorage=_transportParking.transportParkingStorage,
            transportParking=_transportParking.transportParking,
            TPneededItemsBuffer=_transportParking.TPneededItemsBuffer,

            Length2=_nib.Length,
            self=_nib.self,
            storageTarget=_nib.storageTarget,
            neededItemsBuffer=_nib.neededItemsBuffer,
        };
        buildHandle = job.Schedule(_transportParking.Length, 1, inputDeps);

        return buildHandle;
    }

    [BurstCompile]
    struct TransportParkingNeedsMoreItemsJob : IJobParallelFor
    {
        public int Length;
        public EntityArray transportSelf;
        public BufferArray<TransportParkingBuffer> transportParkingStorage;
        public ComponentDataArray<TransportParking> transportParking;
        public BufferArray<TPNeededItemsBuffer> TPneededItemsBuffer;

        public int Length2;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<StorageTarget> storageTarget;
        [NativeDisableParallelForRestriction] public BufferArray<NeededItemsBuffer> neededItemsBuffer;

        public void Execute(int index)
        {
            for (int i = 0; i < Length2; i++)
            {
                if (storageTarget[i].storageTarget==transportSelf[index])
                {
                    for (int j=0; j<neededItemsBuffer[i].Length;j++)
                    {
                        if (neededItemsBuffer[i][j].storage.askForMore>0)
                        {
                            for (int k=0;k<TPneededItemsBuffer[index].Length;k++)
                            {
                                if (TPneededItemsBuffer[index][k].storage.itemStoring==neededItemsBuffer[i][k].storage.itemStoring)
                                {
                                    Storage s1 = TPneededItemsBuffer[index][k].storage;
                                    Storage s2 = neededItemsBuffer[i][j].storage;

                                    s1.maxContains += s2.askForMore;
                                    s2.askForMore = 0;

                                    TPneededItemsBuffer[index].RemoveAt(k);
                                    TPneededItemsBuffer[index].Insert(k, s1);

                                    neededItemsBuffer[i].RemoveAt(j);
                                    neededItemsBuffer[i].Insert(j, s2);

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
