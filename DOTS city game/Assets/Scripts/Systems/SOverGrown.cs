﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

//[UpdateAfter(typeof(SOverGrown))]
//public class BOverGrown : BarrierSystem { }

//[UpdateAfter(typeof(SArrival))]
[UpdateAfter(typeof(SSalvageGiveItem))]
public class SOverGrown : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _OverGrown
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<OverGrown> overGrown;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> st;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    [Inject] _Drones _drones;
    [Inject] _OverGrown _overGrown;

    [Inject] [ReadOnly] ComponentDataFromEntity<MineField> allMineField;
    //[Inject] [ReadOnly] ComponentDataFromEntity<StorageTarget> allStorageTarget;
    //[Inject] [ReadOnly] ComponentDataFromEntity<Salvage> allSalvage;

    //[Inject] BOverGrown barrier;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            salvageSelf = _overGrown.self,
            overGrown = _overGrown.overGrown,
            status = _overGrown.status,
            st = _overGrown.st,
            droneBuffer = _overGrown.droneBuffer,
            droneBufferData = _overGrown.droneBufferData,

            Length = _drones.Length,
            droneSelf = _drones.self,
            move = _drones.move,
            drone = _drones.drone,

            allMineField = allMineField,
            //allStorageTarget=allStorageTarget,

            deltaTime = Time.deltaTime,
            //buffer = barrier.CreateCommandBuffer().ToConcurrent(),
        };
        arrivalHandle = job.Schedule(_overGrown.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {

        public EntityArray salvageSelf;
        public ComponentDataArray<OverGrown> overGrown;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> st;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;

        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        [ReadOnly] public ComponentDataFromEntity<MineField> allMineField;

        public float deltaTime;
        //[ReadOnly] public EntityCommandBuffer.Concurrent buffer;

        public void Execute(int index)
        {
            OverGrown og = overGrown[index];
            //if there is no drones and salvage is empty safely destroy this object
            if (droneBuffer[index].Length == 0 && overGrown[index].ammount == 0)
            {
                //buffer.DestroyEntity(salvageSelf[index]);
                og.change = 1;
            }

            /*
            if (status[index].active == 0)
            {
                return;
            }
            */
            if (og.ammount>0)
            {
                for (int i = 0; i < Length; i++)
                {
                    if (move[i].target == salvageSelf[index])
                    {
                        if (move[i].arrive == 1)
                        {
                            og.currentTime += deltaTime;

                            if (og.currentTime > og.maxTime)
                            {
                                og.currentTime = 0;
                                og.ammount -= 1;
                                if (og.ammount <= 0)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (og.ammount <= 0)
            {
                DroneBufferData dbd = droneBufferData[index];
                dbd.maxDrones = 0;
                droneBufferData[index] = dbd;

                for (int i = 0; i < Length; i++)
                {
                    if (move[i].target == salvageSelf[index])
                    {
                        Drone d = drone[i];
                        Movement m = move[i];

                        //mark drones for reallocation
                        d.doChangeJob = 1;
                        d.nextJob = NextJobType.Idle;

                        for (int j = 0; j < droneBuffer[index].Length; j++)
                        {
                            if (droneBuffer[index][j] == droneSelf[i])
                            {
                                droneBuffer[index].RemoveAt(j);
                                break;
                            }
                        }

                        drone[i] = d;
                        move[i] = m;
                    }
                }
            }
            overGrown[index] = og;
        }
    }
}
