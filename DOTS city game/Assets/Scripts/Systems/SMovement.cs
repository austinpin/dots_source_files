﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(STransferDrone))]
public class SMovement : JobComponentSystem
{
    public static JobHandle GoHandle;

    struct _Moveables
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<Rotation> rot;
        public ComponentDataArray<Movement> move;
    }

    [Inject] _Moveables _moveables;

    struct _Time
    {
        [ReadOnly] public ComponentDataArray<GD_Time> time;
    }

    [Inject] _Time _time;

    [Inject] [ReadOnly] ComponentDataFromEntity<Position> allPos;
    [Inject] [ReadOnly] ComponentDataFromEntity<Square> allSquare;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        SetPosJob setJob = new SetPosJob
        {
            self = _moveables.self,
            move = _moveables.move,

            allPos = allPos,
        };
        JobHandle handle = setJob.Schedule(_moveables.Length, 1, inputDeps);

        GoJob job = new GoJob
        {
            self = _moveables.self,
            pos = _moveables.pos,
            move = _moveables.move,
            rot = _moveables.rot,

            allSquare = allSquare,

            deltaTime = Time.deltaTime * _time.time[0].timeSpeed,
        };
        JobHandle handle2 = job.Schedule(_moveables.Length, 1, handle);

        LookJob lookJob = new LookJob
        {
            self = _moveables.self,
            pos = _moveables.pos,
            move = _moveables.move,
            rot = _moveables.rot,
        };
        GoHandle = lookJob.Schedule(_moveables.Length, 1, handle2);

        return GoHandle;
    }


    [BurstCompile]
    struct SetPosJob : IJobParallelFor
    {
        public EntityArray self;
        public ComponentDataArray<Movement> move;

        [ReadOnly] public ComponentDataFromEntity<Position> allPos;

        public void Execute(int index)
        {
            Movement m = move[index];
            if (m.arrive != 1)
            {
                //Debug.Log(self[index]);
                m.targetPos = allPos[move[index].target].Value;

                move[index] = m;
            }
        }
    }

    [BurstCompile]
    struct GoJob : IJobParallelFor
    {
        public EntityArray self;
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Rotation> rot;

        [ReadOnly] public ComponentDataFromEntity<Square> allSquare;

        public float deltaTime;

        public void Execute(int index)
        {
            if (move[index].arrive != 1)
            {
                float3 targetPos = move[index].targetPos;

                Unity.Mathematics.Random rand = new Unity.Mathematics.Random(move[index].seed);

                Square s = allSquare[move[index].target];

                double radian = rand.NextDouble(0, 2 * math.PI);
                float radius = rand.NextFloat(s.length.x,s.length.y);

                float x = targetPos.x + (radius * math.cos((float) radian));

                float z = targetPos.z + (radius * math.sin((float) radian));

                //float3 randOffset = new float3(x,0,y);

                //float3 randPos = rand.NextFloat3(new float3(1, 0, 1), new float3(-1, 0, -1));
                //randPos *= allSquare[move[index].target].length;

                //targetPos += randOffset;

                targetPos.x = x;
                targetPos.z = z;

                Movement m = move[index];
                m.movePos = targetPos;
                move[index] = m;

                float3 toTarget = targetPos - pos[index].Value;
                float3 direction = math.normalize(toTarget);

                float3 moveDistance = direction * move[index].speed * deltaTime;

                if (math.lengthsq(toTarget) <= math.lengthsq((moveDistance * 1.1f)))
                {
                    Movement m2 = move[index];

                    m2.arrive = 1;

                    move[index] = m2;

                    pos[index] = new Position { Value = targetPos };
                }
                else
                {
                    float3 newPos = pos[index].Value + moveDistance;
                    pos[index] = new Position { Value = newPos };
                }
            }
        }
    }

    [BurstCompile]
    struct LookJob : IJobParallelFor
    {
        public EntityArray self;
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Rotation> rot;

        public void Execute(int index)
        {
            if (move[index].arrive == 1)
            {
                float3 lookDirection = math.normalize(move[index].targetPos - pos[index].Value);
                quaternion q = quaternion.LookRotation(lookDirection, math.up());
                rot[index] = new Rotation { Value = q };
            }
            else{
                float3 lookDirection = math.normalize(move[index].movePos - pos[index].Value);
                quaternion q = quaternion.LookRotation(lookDirection, math.up());
                rot[index] = new Rotation { Value = q };
            }
        }
    }
}
