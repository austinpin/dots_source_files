﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Entities;

public class GameMenu : MonoBehaviour
{

    public GameObject Child;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Menu(bool activeness)
    {
        if (activeness)
        {
            Child.SetActive(true);
        }
        else
        {
            Child.SetActive(false);
        }
    }

    public void Quit()
    {
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }
}
