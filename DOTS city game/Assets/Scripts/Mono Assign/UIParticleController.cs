﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIParticleController : MonoBehaviour
{

    public RectTransform particleOrigin;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        particleOrigin.anchoredPosition = Input.mousePosition;
    }
}
