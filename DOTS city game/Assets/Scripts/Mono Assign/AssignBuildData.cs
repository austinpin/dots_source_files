﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Mathematics;

public class AssignBuildData : MonoBehaviour
{
    CBuildingData data;
    EntityManager em;

    [Tooltip("TransportParking")]
    public Build transportParkingRequirements;

    public float2 transportParkingSquareRadius;
    public float2 Radius2;
    public TransportParking transportParking;

    //idle
    [Tooltip("idle")]
    public Build idleRequirements;

    public float2 idleRadius;

    //idle
    [Tooltip("warehouse")]
    public Build WareHouseRequirements;

    public float2 warehouseRadius;

    //Science
    [Tooltip("science")]
    public Build scienceRequirements;

    public float2 scienceSquareRadius;
    public Science science;

    [Tooltip("Drone Factory")]
    public Build DroneFactoryRequirements;

    public float2 droneFactorySquareRadius;

    //housing
    [Tooltip("Worker Drone Facility")]
    public Build workerDroneFacilityRequirements;

    public float2 workerDroneFacilitySquareRadius;
    public DroneLimit workerDroneFacility;

    [Tooltip("VM")]
    public Build victoryMonumentRequirements;

    public float2 victoryMonumentSquareRadius;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        data = GetComponent<CBuildingData>();
        BuildingData d = data.Value;

        /*
        d.beacon = em.CreateEntity();
        em.AddComponentData(d.beacon, new Build { maxBuildTime = 0, toBuild = Building.Beacon});
        em.AddSharedComponentData(d.beacon, d.BeaconR);
        em.AddComponentData(d.beacon, new Square { length = 2 });
        em.AddSharedComponentData(d.beacon, new RadiusSet { clickRadius = Radius2.x, buildingRadius = Radius2.y });
        //
        d.harvester = em.CreateEntity();
        em.AddComponentData(d.harvester, new Build { maxBuildTime = 0, toBuild = Building.WaterHarvester});
        em.AddSharedComponentData(d.harvester, d.HWaterR);
        em.AddComponentData(d.harvester, new Square { length = 5 });
        em.AddSharedComponentData(d.harvester, new RadiusSet { clickRadius = Radius2.x, buildingRadius = Radius2.y });
        //

        d.idle = em.CreateEntity();
        em.AddComponentData(d.idle, new Build { maxBuildTime = 0, toBuild = Building.Idle });
        em.AddSharedComponentData(d.idle, d.idleR);
        em.AddComponentData(d.idle, new Square { length = idleRadius });
        em.AddBuffer<IdleDroneBuffer>(d.idle);
        em.AddSharedComponentData(d.idle, new RadiusSet { clickRadius = Radius2.x, buildingRadius = Radius2.y });

        //

        d.wareHouse = em.CreateEntity();
        em.AddComponentData(d.wareHouse, new Build { maxBuildTime = 8, toBuild = Building.WareHouse });
        em.AddSharedComponentData(d.wareHouse, d.wareHouseR);
        em.AddComponentData(d.wareHouse, new Square { length = warehouseRadius });
        em.AddBuffer<IdleDroneBuffer>(d.wareHouse);
        em.AddSharedComponentData(d.wareHouse, new RadiusSet { clickRadius = Radius2.x, buildingRadius = Radius2.y });

        em.AddBuffer<NeededItemsBuffer>(d.wareHouse);
        DynamicBuffer<NeededItemsBuffer> wareItems = em.GetBuffer<NeededItemsBuffer>(d.wareHouse);
        wareItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 11 });

        //

        d.science = em.CreateEntity();
        em.AddComponentData(d.science, new Build { maxBuildTime = 7, toBuild = Building.ScienceCentre });
        em.AddSharedComponentData(d.science, d.scienceR);
        em.AddComponentData(d.science, new Square { length = scienceSquareRadius });
        em.AddComponentData(d.science, new Science { });

        em.AddBuffer<NeededItemsBuffer>(d.science);
        DynamicBuffer<NeededItemsBuffer> scienceItems = em.GetBuffer<NeededItemsBuffer>(d.science);
        scienceItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 7 });

        //

        d.droneBay = em.CreateEntity();
        em.AddComponentData(d.droneBay, new Build { maxBuildTime = 7, toBuild = Building.DroneBay });
        em.AddSharedComponentData(d.droneBay, d.droneBayR);
        em.AddComponentData(d.droneBay, new Square { length = 5 });

        em.AddComponentData(d.droneBay, new DroneFactory { maxTime = 10, changeOrder = DroneToMake.WorkerDrone });

        em.AddBuffer<NeededItemsBuffer>(d.droneBay);
        DynamicBuffer<NeededItemsBuffer> droneBayItems = em.GetBuffer<NeededItemsBuffer>(d.droneBay);
        droneBayItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 7 });

        //
        /*
        d.abandonedWareHouse = em.CreateEntity();
        em.AddComponentData(d.abandonedWareHouse, new Build { maxBuildTime = 7, toBuild = Building.AbandonedWareHouse });
        em.AddSharedComponentData(d.abandonedWareHouse, d.abandonedWareHouseR);
        em.AddComponentData(d.abandonedWareHouse, new Square { length = 6 });

        em.AddComponentData(d.abandonedWareHouse, new Salvage { reward = Item.EnergyCells , rewardAmmount = 10 , scrap = Item.DroneParts , scrapAmmount = 25 , maxTime=0.1f });

        em.AddBuffer<NeededItemsBuffer>(d.abandonedWareHouse);
        DynamicBuffer<NeededItemsBuffer> awItems = em.GetBuffer<NeededItemsBuffer>(d.abandonedWareHouse);
        awItems.Add(new Storage { itemStoring = Item.BuildingMaterials, maxContains = 7 });
        /
        //

        d.strangeStructure = em.CreateEntity();
        em.AddComponentData(d.strangeStructure, new Build { maxBuildTime = 7, toBuild = Building.StrangeStructure });
        em.AddSharedComponentData(d.strangeStructure, d.strangeStructureR);
        em.AddComponentData(d.strangeStructure, new Square { length = 6 });

        //em.AddComponentData(d.strangeStructure, new Salvage { reward = Item.EnergyCells, rewardAmmount = 10, scrap = Item.DroneParts, scrapAmmount = 25, maxTime = 0.1f });

        em.AddBuffer<NeededItemsBuffer>(d.strangeStructure);
        DynamicBuffer<NeededItemsBuffer> ssItems = em.GetBuffer<NeededItemsBuffer>(d.strangeStructure);
        ssItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 7 });

        //

        d.Generator = em.CreateEntity();
        em.AddComponentData(d.Generator, new Build { maxBuildTime = 7, toBuild = Building.StrangeStructure });
        em.AddSharedComponentData(d.Generator, d.GeneratorR);
        em.AddComponentData(d.Generator, new Square { length = 6 });

        em.AddComponentData(d.Generator, new Generator { maxTime = 3 , production = 1 });

        em.AddBuffer<NeededItemsBuffer>(d.Generator);
        DynamicBuffer<NeededItemsBuffer> gItems = em.GetBuffer<NeededItemsBuffer>(d.Generator);
        gItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 7 });

        //

        d.reactor = em.CreateEntity();
        em.AddComponentData(d.reactor, new Build { maxBuildTime = 7, toBuild = Building.Reactor});
        em.AddSharedComponentData(d.reactor, d.reactorR);
        em.AddComponentData(d.reactor, new Square { length = 5 });

        em.AddBuffer<NeededItemsBuffer>(d.reactor);
        DynamicBuffer<NeededItemsBuffer> reactorItems = em.GetBuffer<NeededItemsBuffer>(d.reactor);
        reactorItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 7 });

        //

        d.droneFactory = em.CreateEntity();
        em.AddComponentData(d.droneFactory, new Build { maxBuildTime = 7, toBuild = Building.DroneFactory });
        em.AddSharedComponentData(d.droneFactory,d.droneFactoryR);
        em.AddComponentData(d.droneFactory, new Square { length = droneFactorySquareRadius });

        //I need to make prefab entities to copy data from, most importantly the resources needed to make a given unit.
        em.AddComponentData(d.droneFactory, new DroneFactory {maxTime = 10 , changeOrder = DroneToMake.WorkerDrone });

        em.AddBuffer<NeededItemsBuffer>(d.droneFactory);
        DynamicBuffer<NeededItemsBuffer> factoryItems = em.GetBuffer<NeededItemsBuffer>(d.droneFactory);
        factoryItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 10 });

        //

        d.victoryMonument = em.CreateEntity();
        em.AddComponentData(d.victoryMonument, new Build { maxBuildTime = 7, toBuild = Building.VictoryMonument });
        em.AddSharedComponentData(d.victoryMonument, d.VMR);
        em.AddComponentData(d.victoryMonument, new Square { length = victoryMonumentSquareRadius });
        //em.AddComponentData(d.victoryMonument, new ScienceCentre { });

        em.AddBuffer<NeededItemsBuffer>(d.victoryMonument);
        DynamicBuffer<NeededItemsBuffer> vmItems = em.GetBuffer<NeededItemsBuffer>(d.victoryMonument);
        vmItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 2 });
        */

        data.Value = d;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
