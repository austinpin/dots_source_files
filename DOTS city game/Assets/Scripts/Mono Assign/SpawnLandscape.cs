﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Mathematics;
using Unity.Transforms;

public class SpawnLandscape : MonoBehaviour
{
    EntityManager em;
    GameObjectEntity self;

    public float area;
    public int ammount1;

    public Mesh unscannedNodeMesh;
    public Material unscannedNodeMaterial;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        self = GetComponent<GameObjectEntity>();

        float3 myPos = em.GetComponentData<Position>(self.Entity).Value;

        Unity.Mathematics.Random rand = new Unity.Mathematics.Random(123456);

        for ( int i=0; i<ammount1; i++)
        {
            float3 randPos = rand.NextFloat3(new float3(1, 0, 1), new float3(-1, 0, -1));
            randPos *= area;
            float3 myNewPos = randPos + myPos;

            Entity x = em.CreateEntity(Data.unscannedNode);
            em.SetComponentData(x, new Position { Value = myNewPos });
            em.SetComponentData(x, new UnscannedNode { maxScanTime = 100 , squareLength = 5 , nodeType = NodeTypes.SalvageZone , powerNeeded = 25  });
            em.SetComponentData(x, new Square {  length = new float2 ( 1, 2) });
            em.SetSharedComponentData(x, new RadiusSet{ buildingRadius = 2 , clickRadius = 2 });
            em.SetSharedComponentData(x, new MeshInstanceRenderer { mesh = unscannedNodeMesh, material = unscannedNodeMaterial, castShadows = UnityEngine.Rendering.ShadowCastingMode.On });
        }

        Destroy(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
