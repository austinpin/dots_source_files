﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WritePlayerAction : MonoBehaviour
{
    public GameObject EnergyBM;
    public GameObject HumansBM;
    public GameObject MiningBM;
    public GameObject UtilityBM;
    public GameObject MonumentBM;

    CPlayerActionList thePlayer;
    CPlayerIncrement thePlayerIncrement;
    CPlayerBuild build;
    CPlayerInteractionMode interact;
    CPlayerSubInteractionMode subInteract;
    CInfo info;
    CPlayerAffectSelected affect;

    CGetOrStoreMode getOrStore;

    // Start is called before the first frame update
    void Start()
    {
        EnergyBM.SetActive(true);
        HumansBM.SetActive(false);
        MiningBM.SetActive(false);
        UtilityBM.SetActive(false);
        MonumentBM.SetActive(false);

        thePlayer = GetComponent<CPlayerActionList>();
        thePlayerIncrement = GetComponent<CPlayerIncrement>();
        build = GetComponent<CPlayerBuild>();
        interact = GetComponent<CPlayerInteractionMode>();
        subInteract = GetComponent<CPlayerSubInteractionMode>();
        info = GetComponent<CInfo>();
        affect = GetComponent<CPlayerAffectSelected>();

        getOrStore = GetComponent<CGetOrStoreMode>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void EnergyMenu()
    {
        EnergyBM.SetActive(true);
        HumansBM.SetActive(false);
        MiningBM.SetActive(false);
        UtilityBM.SetActive(false);
        MonumentBM.SetActive(false);
    }

    public void HumansMenu()
    {
        EnergyBM.SetActive(false);
        HumansBM.SetActive(true);
        MiningBM.SetActive(false);
        UtilityBM.SetActive(false);
        MonumentBM.SetActive(false);
    }

    public void Miningenu()
    {
        EnergyBM.SetActive(false);
        HumansBM.SetActive(false);
        MiningBM.SetActive(true);
        UtilityBM.SetActive(false);
        MonumentBM.SetActive(false);
    }

    public void UtilityMenu()
    {
        EnergyBM.SetActive(false);
        HumansBM.SetActive(false);
        MiningBM.SetActive(false);
        UtilityBM.SetActive(true);
        MonumentBM.SetActive(false);
    }
    
    public void MonumentMenu()
    {
        EnergyBM.SetActive(false);
        HumansBM.SetActive(false);
        MiningBM.SetActive(false);
        UtilityBM.SetActive(false);
        MonumentBM.SetActive(true);
    }

    public void DroneAdd()
    {
        PlayerActionList p = thePlayer.Value;
        p.playerAction = PlayerAction.AddDrone;
        thePlayer.Value = p;
    }

    public void DroneSubract()
    {
        PlayerActionList p = thePlayer.Value;
        p.playerAction = PlayerAction.SubtractDrone;
        thePlayer.Value = p;
    }

    public void SetIncrement(int value)
    {
        PlayerIncrement data = thePlayerIncrement.Value;
        data.increment =(ushort)  value;
        thePlayerIncrement.Value = data;
    }

    public void LevelUp(int n)
    {
        HowToAffect hta = (HowToAffect)n;

        PlayerAffectSelected s = affect.Value;
        s.howToAffect = hta;
        affect.Value = s;
    }

    public void DroneFactoryCreateDrone(int n)
    {
        HowToAffect hta = (HowToAffect)n;

        PlayerAffectSelected s = affect.Value;
        s.howToAffect = hta;
        affect.Value = s;
    }

    public void BuildingInfo(int n)
    {
        PlayerInfo i = info.Value;

        i.showing = (Info) n;

        info.Value = i;

        PlayerInteractionMode mode = interact.Value;
        mode.playerInteractMode = PlayerInteractionModes.Info;
        interact.Value = mode;
    }

    public void Build_Anything(int index)
    {
        Building bi = (Building)index;

        PlayerBuild b = build.Value;
        b.toBuild = bi;
        build.Value = b;
    }

    public void ViewMode()
    {
        PlayerInteractionMode mode = interact.Value;
        mode.playerInteractMode = PlayerInteractionModes.View;
        interact.Value = mode;
    }

    public void BuildMode()
    {
        PlayerInteractionMode mode = interact.Value;
        mode.playerInteractMode = PlayerInteractionModes.Build;
        interact.Value = mode;
    }

    public void TradeMode()
    {
        PlayerInteractionMode mode = interact.Value;
        mode.playerInteractMode = PlayerInteractionModes.Trade;
        interact.Value = mode;
    }

    public void ReadvancementMode()
    {
        PlayerInteractionMode mode = interact.Value;
        mode.playerInteractMode = PlayerInteractionModes.Readvancement;
        interact.Value = mode;
    }

    public void NoOrder()
    {
        PlayerSubInteractionMode sMode = subInteract.Value;
        sMode.playerSubInteractMode = PlayerSubInteractionModes.None;
        subInteract.Value = sMode;
    }

    public void AssignOrder()
    {
        PlayerSubInteractionMode sMode = subInteract.Value;
        sMode.playerSubInteractMode = PlayerSubInteractionModes.AssignOrder;
        subInteract.Value = sMode;
    }

    public void MoveOrder()
    {
        PlayerSubInteractionMode sMode = subInteract.Value;
        sMode.playerSubInteractMode = PlayerSubInteractionModes.MoveOrder;
        subInteract.Value = sMode;
    }

    public void UnscannedActivate()
    {
        PlayerAffectSelected s = affect.Value;
        s.howToAffect = HowToAffect.Unscanned_ActivateScan;
        affect.Value = s;
    }

    /*
    public void GetFromInv()
    {
        GetOrStoreMode mode = getOrStore.Value;
        mode.getOrStoreMode = 1;
        getOrStore.Value = mode;
    }

    
    public void StoreToInv()
    {
        GetOrStoreMode mode = getOrStore.Value;
        mode.getOrStoreMode = 2;
        getOrStore.Value = mode;
    }
    */
}
