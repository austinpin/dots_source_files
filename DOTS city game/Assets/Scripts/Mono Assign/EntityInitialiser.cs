﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;
using Unity.Collections;

//this script initalises the entities
//entities cannot be fully initialised with gameobjects
//normally entities are created with scripts but gameobjects offer a path to create them from the scene and visible in the scene.

public class EntityInitialiser : MonoBehaviour
{
    Unity.Mathematics.Random rand = new Unity.Mathematics.Random((uint)System.DateTime.Now.Ticks);

    public Mesh tileMesh;
    public Material tileMat1White;
    public Material tileMat2Blue;
    public Material tileMat3Green;
    public Material tileMat4Purple;
    public Material tileMat5Yellow;
    public Material tileMat6Cyan;
    public Material tileMat7gold;
    public Material floorTile;

    public Material hillMat;
    public Material geothermalMat;
    public Material photiteMat;
    public Material duriumMat;
    public Material vionsMat;

    [Header("generaL")]
    public GameObjectEntity storeTarget;
    public GameObjectEntity transportparkingTarget;
    public bool iniStatic;

    [Header("Storage")]
    public int testingDroneAmmount;
    public int testingScanDroneAmmount;
    public int testingTransportAmmount;
    public bool iniMinorItemStorage;
    public GameObjectEntity targetIdle;

    [Header("Drone Buffer")]
    public bool iniNormalDroneBuffer;
    public bool iniIdleDroneBuffer;
    public bool iniArrivalBuffer;

    [Header("Data")]
    public Mesh DroneMesh;
    public Material DroneMat;

    public Mesh TransportMesh;
    public Material TransportMat;

    [Header("transportdrone")]
    public GameObjectEntity storage;
    public GameObjectEntity parking;

    [Header("Mine")]
    public bool iniMine;
    public bool iniSalvage;
    public bool iniBuild;
    public bool iniStoreTarget;

    public bool iniTransportDrone;
    public bool iniTransportParking;

    public bool iniTileMaster;

    public bool iniUser;

    EntityManager em;
    GameObjectEntity self;

    private void Awake()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        self = GetComponent<GameObjectEntity>();

        if (iniNormalDroneBuffer)
        {
            em.AddBuffer<DroneBuffer>(self.Entity);
        }
        if (iniIdleDroneBuffer)
        {
            em.AddBuffer<IdleDroneBuffer>(self.Entity);
        }

        if (iniStoreTarget)
        {
            StorageTarget t = em.GetComponentData<StorageTarget>(self.Entity);
            t.storageTarget = storeTarget.Entity;
            em.SetComponentData(self.Entity, t);
        }

        if (iniUser)
        {
            //em.AddBuffer<UserToScanBuffer>(self.Entity);
        }

        if (iniMine)
        {

            /*
            MineField f = em.GetComponentData<MineField>(self.Entity);
            f.storeTarget = storeTarget.Entity;
            em.SetComponentData(self.Entity, f);
            */
        }

        if (iniSalvage)
        {

            /*
            Salvage s = em.GetComponentData<Salvage>(self.Entity);
            s.storeTarget = storeTarget.Entity;
            em.SetComponentData(self.Entity, s);
            */
        }

        if (iniBuild)
        {
            em.AddBuffer<NeededItemsBuffer>(self.Entity);
            DynamicBuffer<NeededItemsBuffer> buildItems = em.GetBuffer<NeededItemsBuffer>(self.Entity);

            buildItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 10 });
            //buildItems.Add(new Storage { itemStoring = Item.ComputerParts , containsAmmount = 2 });

            Build b = em.GetComponentData<Build>(self.Entity);
            //b.storeTarget = storeTarget.Entity;
            //b.toBuild = Building.ScienceCentre;
            em.SetComponentData(self.Entity, b);
        }

        if (iniTransportDrone)
        {
            em.AddBuffer<TransportDroneBuffer>(self.Entity);
            DynamicBuffer<TransportDroneBuffer> stores = em.GetBuffer<TransportDroneBuffer>(self.Entity);

            //stores.Add(new Storage { itemStoring = Item.Metal});
            //stores.Add(new Storage { itemStoring = Item.ComputerParts });

            TransportDrone td = em.GetComponentData<TransportDrone>(self.Entity);
            td.storageHome = storage.Entity;
            td.ParkingTo = parking.Entity;
            em.SetComponentData(self.Entity, td);
        }

        if (iniTransportParking)
        {
            em.AddBuffer<TransportParkingBuffer>(self.Entity);
            DynamicBuffer<TransportParkingBuffer> stores = em.GetBuffer<TransportParkingBuffer>(self.Entity);

            em.AddBuffer<TPNeededItemsBuffer>(self.Entity);

            TransportParking p = em.GetComponentData<TransportParking>(self.Entity);
            p.storageTarget = storage.Entity;
            em.SetComponentData(self.Entity, p);
        }

        if (iniStatic)
        {
            em.AddComponentData(self.Entity, new Static { });
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (iniMinorItemStorage)
        {
            //add buffer, get buffer to add what it should store
            em.AddBuffer<StorageBuffer>(self.Entity);
            DynamicBuffer<StorageBuffer> stores = em.GetBuffer<StorageBuffer>(self.Entity);

            //stores.Add(new Storage {itemStoring=Item.Metal, maxContains=25 });
            //stores.Add(new Storage {itemStoring= Item.ComputerParts, maxContains=25 });

            //get my position to spawn drones
            float3 myPos = em.GetComponentData<Position>(self.Entity).Value;

            //Unity.Mathematics.Random rand = new Unity.Mathematics.Random();

            //worker drones
            //spawn drones from archetype, set their data
            for (int i = 0; i < testingDroneAmmount; i++)
            {
                float3 randPos = rand.NextFloat3(new float3(1, 0, 1), new float3(-1, 0, -1));
                randPos *= 5;

                float3 myNewPos = randPos + myPos;
                Entity x = em.CreateEntity(Data.drone);
                em.SetComponentData(x, new Position { Value = myNewPos });
                em.SetComponentData(x, new Movement { speed = 14, seed = rand.NextUInt(), target = targetIdle.Entity });
                em.SetSharedComponentData(x, new MeshInstanceRenderer { mesh = DroneMesh, material = DroneMat, castShadows = UnityEngine.Rendering.ShadowCastingMode.Off });

                //store into rally
                DynamicBuffer<IdleDroneBuffer> rally = em.GetBuffer<IdleDroneBuffer>(targetIdle.Entity);
                rally.Add(x);
            }

            for (int i = 0; i < testingTransportAmmount; i++)
            {
                float3 randPos = rand.NextFloat3(new float3(1, 0, 1), new float3(-1, 0, -1));
                randPos *= 5;

                float3 myNewPos = randPos + myPos;
                Entity x = em.CreateEntity(Data.transportDrone);
                em.SetComponentData(x, new Position { Value = myNewPos });

                em.SetComponentData(x, new Movement { speed = 25, seed = rand.NextUInt(), target = targetIdle.Entity, arrive = 1 });
                em.SetComponentData(x, new Square { length = new float2 { x = 0.5f, y = 1.5f } });
                em.SetSharedComponentData(x, new RadiusSet { clickRadius = 1 });
                em.SetComponentData(x, new TransportDrone { maxItems = 30, modeIdle = 1, switchParked = 1, parkedWhere = 2, StoreOrGet = 1, storageHome = storeTarget.Entity, ParkingTo = transportparkingTarget.Entity });
                em.SetSharedComponentData(x, new MeshInstanceRenderer { mesh = TransportMesh, material = TransportMat, castShadows = UnityEngine.Rendering.ShadowCastingMode.Off });
            }

            //scan drones
            for (int i = 0; i < testingScanDroneAmmount; i++)
            {
                float3 randPos = rand.NextFloat3(new float3(1, 0, 1), new float3(-1, 0, -1));
                randPos *= 5;

                float3 myNewPos = randPos + myPos;
                Entity x = em.CreateEntity(Data.scanDrone);
                em.SetComponentData(x, new Position { Value = myNewPos });
                em.SetComponentData(x, new ScanDrone { state = ScanDroneState.NewTarget, partition = rand.NextUInt() });
                em.SetComponentData(x, new Movement { speed = 11, seed = rand.NextUInt(), target = targetIdle.Entity });
                em.SetSharedComponentData(x, new MeshInstanceRenderer { mesh = DroneMesh, material = DroneMat, castShadows = UnityEngine.Rendering.ShadowCastingMode.Off });
            }
        }


        if (iniTileMaster)
        {
            int size = 64;
            for (int i = 1; i < size; i++)
            {
                for (int k = 1; k < size; k++)
                {
                    int xo = i - 2;
                    int yo = k - 2;

                    int x = 200 * xo;
                    int y = 200 * yo;

                    float3 pos2 = new float3(x, 25, y);

                    Entity are = em.CreateEntity();
                    em.AddComponentData(are, new Position { Value = pos2 });
                    em.AddComponentData(are, new Scale { Value = new float3(200, 200, 200) });
                    em.AddComponentData(are, new Rotation { Value = new quaternion(0.7071068f, 0, 0, 0.7071068f) });
                    em.AddSharedComponentData(are, new MeshInstanceRenderer { mesh = tileMesh, material = floorTile });
                }
            }

            //int size2 = 500;
            int size2 = PersistantData.worldSize;
            byte smallSize = 10;
            byte largeSize = 10;

            Entity template = em.CreateEntity(Data.tileE);
            em.SetComponentData(template, new Position {});
            em.SetComponentData(template, new Scale { Value = new float3(6, 6, 6) });
            em.SetComponentData(template, new Rotation { Value = new quaternion(0.7071068f, 0, 0, 0.7071068f) });
            em.SetSharedComponentData(template, new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
            em.SetComponentData(template, new WorldMeshRenderBounds { Radius = 6 });
            em.SetComponentData(template, new RevealTile { powerNeeded = 0, type = RevealType.None , ammount = 0 });

            int size0 = math.mul(size2, size2);
            NativeArray<Entity> tilE = new NativeArray<Entity>(size0, Allocator.Temp);
            NativeArray<int> refs = new NativeArray<int>(size0, Allocator.Temp);
            //em.CreateEntity(Data.tileE, tilE);
            em.Instantiate(template , tilE);
            em.DestroyEntity(template);
            int iterator = 0;

            for (int i = 0; i < size2; i++)
            {
                for (int k = 0; k < size2; k++)
                {

                    float x = 6 * i;
                    float y = 6 * k;

                    //x += offset;
                    //y += offset;

                    float3 pos = new float3(x, 25.01f, y);

                    em.SetComponentData(tilE[iterator], new Position { Value = pos });
                    //em.SetComponentData(tilE[iterator], new Scale { Value = new float3(6, 6, 6) });
                    //em.SetComponentData(tilE[iterator], new Rotation { Value = new quaternion(0.7071068f, 0, 0, 0.7071068f) });
                    //em.SetSharedComponentData(tilE[iterator], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                    em.SetComponentData(tilE[iterator], new WorldMeshRenderBounds { Center = pos, Radius = 6 });
                    //em.SetComponentData(tilE[iterator], new RevealTile { powerNeeded = 0, type = RevealType.None , ammount = 0 });

                    iterator++;
                }
            }

            /*
            for (int j = 0; j < 3; j++)
            {
                //int2 b = rand.NextInt2(new int2(3, size2 - 3), new int2(3, size2 - 3));
                int i = rand.NextInt(3, size2 - 3);
                int k = rand.NextInt(3, size2 - 3);

                refs[i * size2 + k] = -1;
                em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = photiteMat });
                em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.PhotiteZone, ammount = 0, size = largeSize });
            }
            for (int j = 0; j < 3; j++)
            {
                //int2 b = rand.NextInt2(new int2(3, size2 - 3), new int2(3, size2 - 3));
                int i = rand.NextInt(3, size2 - 3);
                int k = rand.NextInt(3, size2 - 3);

                refs[i * size2 + k] = -1;
                em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.DuriumZone, ammount = 0, size = largeSize });
            }

            for (int j = 0; j < 3; j++)
            {
                //int2 b = rand.NextInt2(new int2(3, size2 - 3), new int2(3, size2 - 3));
                int i = rand.NextInt(3, size2 - 3);
                int k = rand.NextInt(3, size2 - 3);

                refs[i * size2 + k] = -1;
                em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.VionsZone, ammount = 0, size = largeSize });
            }
            */

            int fact = 3;
            int range = 9;
            int range2 = 12;
            byte reveal = 2;

            for (int i = 0; i < size2; i++)
            {
                for (int k = 0; k < size2; k++)
                {
                    if (i < range+4 || i > size2 - range-4 || k < range+4|| k > size2 - range-4)
                    {

                    }
                    else
                    {
                        if (i % fact == 0 && k % fact == 0)
                        {

                            Material m = tileMat1White;
                            RevealType rType = RevealType.None;

                            if (refs[i * size2 + k] == 0)
                            {
                                int v = rand.NextInt(1, 6000);
                                refs[i * size2 + k] = v;

                                /*
                                if (v == 146 || v == 1654)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.BoostZone, ammount = 0, size = smallSize });
                                }
                                */

                                if (v == 291 || v == 543 || v== 1256)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = photiteMat });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.SmallPhotiteZone, ammount = 0, size = smallSize , reveal = reveal });

                                    for (int z=i- range2; z<i+ range2; z++)
                                    {
                                        for (int b = k - range2; b < k + range2; b++)
                                        {
                                            refs[z * size2 + b] = 1;
                                            if (z % fact == 0 && b % fact == 0)
                                            {
                                                int g = rand.NextInt(1, 6);
                                                if (g==2)
                                                {
                                                    em.SetSharedComponentData(tilE[z * size2 + b], new MeshInstanceRenderer { mesh = tileMesh, material = photiteMat });
                                                    em.SetComponentData(tilE[z * size2 + b], new RevealTile { type = RevealType.SmallPhotiteZone, ammount = 0, size = smallSize, reveal = reveal });
                                                }
                                                if (g==4)
                                                {
                                                    em.SetSharedComponentData(tilE[z * size2 + b], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat7gold });
                                                    em.SetComponentData(tilE[z * size2 + b], new RevealTile { type = RevealType.photiteClickable, ammount = 0, size = smallSize, reveal = reveal });
                                                }
                                            }
                                        }
                                    }
                                }
                                if (v == 230 || v == 564)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.GeothermalZone, ammount = 0, size = smallSize, reveal = reveal });

                                    for (int z = i - range2; z < i + range2; z++)
                                    {
                                        for (int b = k - range2; b < k + range2; b++)
                                        {
                                            refs[z * size2 + b] = 1;
                                            if (z % fact == 0 && b % fact == 0)
                                            {
                                                int g = rand.NextInt(1, 6);
                                                if (g == 3)
                                                {
                                                    em.SetSharedComponentData(tilE[z * size2 + b], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                                    em.SetComponentData(tilE[z * size2 + b], new RevealTile { type = RevealType.GeothermalZone, ammount = 0, size = smallSize, reveal = reveal });
                                                }
                                            }
                                        }
                                    }
                                }
                                if (v == 678 || v == 330 || v==975 || v==1635 || v==3253 || v==4324 || v==3587 || v==2603 || v==3862 || v==2009 || v==2008 || v==1009 || v==3009 || v==4009 || v==4008 || v==4001 || v==3001 || v==1001 || v==1098 || v==2098 || v==3098)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.TechZone, ammount = 0, size = smallSize, reveal = reveal });
                                }
                                if (v == 640 || v == 540)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.SmallDuriumZone, ammount = 0, size = smallSize, reveal = reveal });

                                    for (int z = i - range2; z < i + range2; z++)
                                    {
                                        for (int b = k - range2; b < k + range2; b++)
                                        {
                                            refs[z * size2 + b] = 1;
                                            if (z % fact == 0 && b % fact == 0)
                                            {
                                                int g = rand.NextInt(1,6);
                                                if (g == 2)
                                                {
                                                    em.SetSharedComponentData(tilE[z * size2 + b], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                                    em.SetComponentData(tilE[z * size2 + b], new RevealTile { type = RevealType.SmallDuriumZone, ammount = 0, size = smallSize, reveal = reveal });
                                                }
                                            }
                                        }
                                    }
                                }
                                if (v == 740 || v==2235)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.SmallVionsZone, ammount = 0, size = smallSize, reveal = reveal });

                                    for (int z = i - range2; z < i + range2; z++)
                                    {
                                        for (int b = k - range2; b < k + range2; b++)
                                        {
                                            refs[z * size2 + b] = 1;
                                            if (z % fact == 0 && b % fact == 0)
                                            {
                                                int g = rand.NextInt(1, 7);
                                                if (g == 2)
                                                {
                                                    em.SetSharedComponentData(tilE[z * size2 + b], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                                    em.SetComponentData(tilE[z * size2 + b], new RevealTile { type = RevealType.SmallVionsZone, ammount = 0, size = smallSize, reveal = reveal });
                                                }
                                            }
                                        }
                                    }
                                }
                                if (v == 199 || v == 222 || v==1357)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.Hill, ammount = 0, size = smallSize, reveal = reveal });

                                    for (int z = i - range2; z < i + range2; z++)
                                    {
                                        for (int b = k - range2; b < k + range2; b++)
                                        {
                                            refs[z * size2 + b] = 1;
                                            if (z % fact == 0 && b % fact == 0)
                                            {
                                                int g = rand.NextInt(1, 7);
                                                if (g == 6)
                                                {
                                                    em.SetSharedComponentData(tilE[z * size2 + b], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat1White });
                                                    em.SetComponentData(tilE[z * size2 + b], new RevealTile { type = RevealType.Hill, ammount = 0, size = smallSize, reveal = reveal });
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (v < 8)
                                {
                                    //em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat7gold });
                                    //em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.Pickup, ammount = 0, size = smallSize });
                                }
                                /*
                                else if (v == 12 || v == 165)
                                {
                                    em.SetSharedComponentData(tilE[i * size2 + k], new MeshInstanceRenderer { mesh = tileMesh, material = tileMat7gold });
                                    em.SetComponentData(tilE[i * size2 + k], new RevealTile { type = RevealType.photiteClickable, ammount = 0, size = smallSize });
                                }
                                */
                            }
                            else
                            {

                            }
                        }
                    }
                }
            }
        }

        Destroy(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
