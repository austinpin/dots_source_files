﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.Entities;

public class WriteTradeMenu : MonoBehaviour
{
    public GameObject buttonRef;
    public Transform parent;
    public RectTransform[] buttons;

    public GameObjectEntity userUnlocks;
    EntityManager em;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        buttons = new RectTransform[9];
        Vector3 pos;
        for (int i = 1; i < 10; i++)
        {
            int diff = (i * -60)+25;
            pos = new Vector3(-190,diff,0);
            GameObject g = Instantiate(buttonRef,pos,Quaternion.identity,parent) as GameObject;
            RectTransform rt = g.GetComponent<RectTransform>();
            rt.anchoredPosition = pos;
            buttons[i - 1] = rt;
            Transform t2 = g.transform.GetChild(1);
            TextMeshProUGUI tmp = t2.GetComponent<TextMeshProUGUI>();

            if (i==1)
            {
                tmp.text = "Transmitter factory: nessesary to create additional beacons and other unique structures";
                g.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(Buy_TransmitterFactory);
                //g.GetComponent<Button>().onClick.AddListener(Buy_TransmitterFactory);
            }
        }
    }

    public void Buy_TransmitterFactory()
    {
        UserUnlocks unlocks = em.GetComponentData<UserUnlocks>(userUnlocks.Entity);
        if (unlocks.techTransmitterFactory==0)
        {
            unlocks.techTransmitterFactory = 1;
        }
        em.SetComponentData<UserUnlocks>(userUnlocks.Entity, unlocks);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
