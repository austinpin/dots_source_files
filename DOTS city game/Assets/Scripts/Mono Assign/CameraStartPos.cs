﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityTemplateProjects;
using Unity.Entities;

public class CameraStartPos : MonoBehaviour
{
    public EntityManager em;
    public GameObjectEntity ehome;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetExistingManager<EntityManager>();

        Vector3 pos;
        int middle = (PersistantData.worldSize / 2) * 6;
        pos = new Vector3(middle, 300, middle);
        transform.position = pos;
        GetComponent<SimpleCameraController>().enabled = true;

        Home h = em.GetComponentData<Home>(ehome.Entity);
        h.actualPos = transform.position;
        em.SetComponentData(ehome.Entity, h);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoHome()
    {
        Home h = em.GetComponentData<Home>(ehome.Entity);
        SimpleCameraController c = GetComponent<SimpleCameraController>();
        c.m_TargetCameraState.x = h.actualPos.x;
        c.m_TargetCameraState.y = h.actualPos.y;
        c.m_TargetCameraState.z = h.actualPos.z;
    }

    public void NewHome()
    {
        Home h = em.GetComponentData<Home>(ehome.Entity);
        h.actualPos = transform.position;
        em.SetComponentData(ehome.Entity, h);
    }
}
