﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Mathematics;

public class AssignUnitData : MonoBehaviour
{
    CUnitData udata;
    EntityManager em;

    public MeshInstanceRenderer Wd;
    public MeshInstanceRenderer td;
    public MeshInstanceRenderer sd;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        udata = GetComponent<CUnitData>();
        UnitData ud = udata.Value;

        ud.workerDrone = em.CreateEntity();
        em.AddSharedComponentData(ud.workerDrone, Wd);
        em.AddComponentData(ud.workerDrone, new Movement { speed = 14, seed = 923578 });
        em.AddBuffer<NeededItemsBuffer>(ud.workerDrone);
        DynamicBuffer<NeededItemsBuffer> wDItems = em.GetBuffer<NeededItemsBuffer>(ud.workerDrone);
        wDItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 2 });

        ud.transportDrone = em.CreateEntity();
        em.AddSharedComponentData(ud.transportDrone, td);
        em.AddComponentData(ud.transportDrone, new Movement { speed = 11, seed = 2560673489 });
        em.AddComponentData(ud.transportDrone, new TransportDrone { maxItems = 50 });
        em.AddSharedComponentData(ud.transportDrone, new RadiusSet { clickRadius = 1.5f });
        em.AddComponentData(ud.transportDrone, new Square { length = new float2(1, 2) });
        em.AddBuffer<NeededItemsBuffer>(ud.transportDrone);
        DynamicBuffer<NeededItemsBuffer> tDItems = em.GetBuffer<NeededItemsBuffer>(ud.transportDrone);
        tDItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 4 });

        ud.scanDrone = em.CreateEntity();
        em.AddSharedComponentData(ud.scanDrone, sd);
        em.AddComponentData(ud.scanDrone, new Movement { speed = 11, seed = 64832684 });
        em.AddComponentData(ud.scanDrone, new ScanDrone { state = ScanDroneState.NewTarget });
        em.AddBuffer<NeededItemsBuffer>(ud.scanDrone);
        DynamicBuffer<NeededItemsBuffer> sDItems = em.GetBuffer<NeededItemsBuffer>(ud.scanDrone);
        sDItems.Add(new Storage { itemStoring = Item.Materials, maxContains = 3 });

        udata.Value = ud;
    }
}
