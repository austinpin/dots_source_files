﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPS : MonoBehaviour
{
    public TextMeshProUGUI t;

    public int frames;
    public float dt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        frames++;
        dt += Time.deltaTime;

        if (dt>=0.5f)
        {
            t.text = (frames*2).ToString();
            dt = 0;
            frames = 0;
        }
    }
}
