﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class WriteUserAction : MonoBehaviour
{

    CUserPickReadv pick;
    GameObjectEntity self;
    EntityManager em;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        pick = GetComponent<CUserPickReadv>();
        self = GetComponent<GameObjectEntity>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickReadvancement(int re)
    {
        UserPickReadv read = em.GetComponentData<UserPickReadv>(self.Entity);
        read.Clicked = (Readvancements) re;
        em.SetComponentData(self.Entity, read);
    }

    public void BuyTech()
    {
        UserPickReadv read = em.GetComponentData<UserPickReadv>(self.Entity);
        read.tryBuy = 1;
        em.SetComponentData(self.Entity, read);
    }
}
