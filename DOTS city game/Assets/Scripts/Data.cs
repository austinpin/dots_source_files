﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

public class Data : MonoBehaviour
{
    //to do list:

        //1 week
        //Overflow limit user data types
        //imeplement blueprints process of planning building placement
        //implement monuments
        //implement research
        //move traders to come from spaceport
        //add more to traders
        //add more STUFF (items etc), for buildings, workers, trader reputation etc.

        //
        //finish new zone types
        //add respective buildings
        //add levls
        //>blueprints as above
        //add the upgrade buildings and some progression

        //make transmitters allocated???

    public static EntityArchetype drone;
    public static EntityArchetype transportDrone;
    public static EntityArchetype scanDrone;

    public static EntityArchetype buildSite;

    public static EntityArchetype building_TransportParking;
    public static EntityArchetype building_Idle;
    public static EntityArchetype building_WareHouse;
    public static EntityArchetype building_DroneFactory;
    public static EntityArchetype building_VictoryMonument;

    public static EntityArchetype building_Generator;
    public static EntityArchetype building_StrangeStructure;
    //public static EntityArchetype building_AbandonedWarehouse;
    //public static EntityArchetype building_DroneBay;
    //public static EntityArchetype building_Reactor;

    public static EntityArchetype building_Harvester;
    public static EntityArchetype building_Farm;
    public static EntityArchetype building_Refinery;
    public static EntityArchetype building_Beacon;

    public static EntityArchetype building_TransmitterFactory;

    public static EntityArchetype node;
    public static EntityArchetype unscannedNode;
    public static EntityArchetype Zone;
    public static EntityArchetype collectNode;

    public static EntityArchetype tileE;

    EntityManager em;

    // Start is called before the first frame update
    void Awake()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();

        tileE = em.CreateArchetype
            (
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<Scale>(),
            ComponentType.Create<Static>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<WorldMeshRenderBounds>(),
            ComponentType.Create<RevealTile>()
            //ComponentType.Create<RadiusSet>()
            );

        drone = em.CreateArchetype
        (
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<Movement>(),
            ComponentType.Create<Drone>()
        );

        transportDrone = em.CreateArchetype
        (
                ComponentType.Create<Position>(),
                ComponentType.Create<Rotation>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<TransportDrone>(),
                ComponentType.Create<Movement>(),
                ComponentType.Create<RadiusSet>(),
                ComponentType.Create<Square>(),
                ComponentType.Create<TransportDroneBuffer>()
        );

        scanDrone = em.CreateArchetype
        (
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<Movement>(),
            ComponentType.Create<ScanDrone>()
        );

        buildSite = em.CreateArchetype
        (
            ComponentType.Create<Position>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<DroneBuffer>(),
            ComponentType.Create<NeededItemsBuffer>(),
            ComponentType.Create<Square>(),
            ComponentType.Create<Static>(),
            ComponentType.Create<Build>(),
            ComponentType.Create<DroneBufferData>(),
            ComponentType.Create<RadiusSet>(),
            ComponentType.Create<Status>(),
            ComponentType.Create<StorageTarget>()
        );

        building_TransportParking = em.CreateArchetype
        (
            ComponentType.Create<Position>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<TPNeededItemsBuffer>(),
            ComponentType.Create<TransportParkingBuffer>(),
            ComponentType.Create<Square>(),
            ComponentType.Create<Static>(),
            ComponentType.Create<TransportParking>(),
            ComponentType.Create<RadiusSet>()
        );

        building_Idle = em.CreateArchetype
        (
            ComponentType.Create<Position>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<IdleDroneBuffer>(),
            ComponentType.Create<Square>(),
            //ComponentType.Create<Static>(),
            ComponentType.Create<RadiusSet>()
        );

        building_WareHouse = em.CreateArchetype
        (
            ComponentType.Create<Position>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<Square>(),
            ComponentType.Create<Static>(),
            ComponentType.Create<StorageBuffer>(),
            ComponentType.Create<RadiusSet>()
        );

        building_Generator = em.CreateArchetype
            (
                ComponentType.Create<Position>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<Square>(),
                ComponentType.Create<Static>(),
                ComponentType.Create<Generator>(),
                ComponentType.Create<RadiusSet>(),
                ComponentType.Create<Status>(),
ComponentType.Create<Upgradeable>()
            );

        building_StrangeStructure = em.CreateArchetype
            (
                ComponentType.Create<Position>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<DroneBuffer>(),
                ComponentType.Create<Square>(),
                ComponentType.Create<Static>(),
                ComponentType.Create<Science>(),
                ComponentType.Create<DroneBufferData>(),
                ComponentType.Create<RadiusSet>(),
                ComponentType.Create<Status>(),
                ComponentType.Create<StorageTarget>()
            );

        building_DroneFactory = em.CreateArchetype
            (
                ComponentType.Create<Position>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<DroneBuffer>(),
                ComponentType.Create<Square>(),
                ComponentType.Create<Static>(),
                ComponentType.Create<DroneFactory>(),
                ComponentType.Create<DroneBufferData>(),
                ComponentType.Create<NeededItemsBuffer>(),
                ComponentType.Create<DroneFactoryBuffer>(),
                ComponentType.Create<RadiusSet>(),
                ComponentType.Create<Status>(),
                ComponentType.Create<StorageTarget>(),
ComponentType.Create<Upgradeable>()
            );

        building_VictoryMonument = em.CreateArchetype
            (
                ComponentType.Create<Position>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                //ComponentType.Create<DroneBuffer>(),
                //ComponentType.Create<TaskBuffer>(),
                ComponentType.Create<Square>(),
                ComponentType.Create<Static>(),
                ComponentType.Create<Status>(),
                //ComponentType.Create<DroneLimit>(),
                //ComponentType.Create<DroneBufferData>(),
                ComponentType.Create<RadiusSet>(),
ComponentType.Create<Upgradeable>()
            );

        node = em.CreateArchetype
            (
                ComponentType.Create<Position>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<DroneBuffer>(),
                ComponentType.Create<Square>(),
                ComponentType.Create<Static>(),
                ComponentType.Create<DroneBufferData>(),
                ComponentType.Create<RadiusSet>(),
                ComponentType.Create<Status>(),
                ComponentType.Create<StorageTarget>()
            );

        unscannedNode = em.CreateArchetype
            (
                ComponentType.Create<Position>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<Square>(),
                //ComponentType.Create<Static>(),
                ComponentType.Create<RadiusSet>(),
                ComponentType.Create<UnscannedNode>()
            );

        Zone = em.CreateArchetype
    (
        ComponentType.Create<Position>(),
        ComponentType.Create<Scale>(),
        ComponentType.Create<MeshInstanceRenderer>(),
        ComponentType.Create<Square>(),
        //ComponentType.Create<Static>(),
        //ComponentType.Create<RadiusSet>(),
        ComponentType.Create<Zone>()
    );

        building_Beacon = em.CreateArchetype
    (
        ComponentType.Create<Position>(),
        ComponentType.Create<MeshInstanceRenderer>(),
        ComponentType.Create<Square>(),
        ComponentType.Create<Static>(),
        ComponentType.Create<RadiusSet>(),
        ComponentType.Create<Status>(),
        ComponentType.Create<Beacon>(),
ComponentType.Create<Upgradeable>()
    );

        building_Harvester = em.CreateArchetype
    (
        ComponentType.Create<Position>(),
        ComponentType.Create<MeshInstanceRenderer>(),
        ComponentType.Create<DroneBuffer>(),
        ComponentType.Create<Square>(),
        ComponentType.Create<Static>(),
        ComponentType.Create<DroneBufferData>(),
        ComponentType.Create<RadiusSet>(),
        ComponentType.Create<Harvester>(),
        ComponentType.Create<Status>(),
ComponentType.Create<Upgradeable>()
    );

        building_Farm = em.CreateArchetype
(
ComponentType.Create<Position>(),
ComponentType.Create<MeshInstanceRenderer>(),
ComponentType.Create<Square>(),
ComponentType.Create<Static>(),
ComponentType.Create<RadiusSet>(),
ComponentType.Create<Farm>(),
ComponentType.Create<Status>(),
ComponentType.Create<Upgradeable>()
);

        building_Refinery = em.CreateArchetype
        (
        ComponentType.Create<Position>(),
        ComponentType.Create<MeshInstanceRenderer>(),
        ComponentType.Create<DroneBuffer>(),
        ComponentType.Create<Square>(),
        ComponentType.Create<Static>(),
        ComponentType.Create<DroneBufferData>(),
        ComponentType.Create<RadiusSet>(),
        ComponentType.Create<Refinery>(),
        ComponentType.Create<Status>(),
ComponentType.Create<Upgradeable>()
        );

        building_TransmitterFactory = em.CreateArchetype
(
ComponentType.Create<Position>(),
ComponentType.Create<MeshInstanceRenderer>(),
ComponentType.Create<DroneBuffer>(),
ComponentType.Create<Square>(),
ComponentType.Create<Static>(),
ComponentType.Create<DroneBufferData>(),
ComponentType.Create<RadiusSet>(),
ComponentType.Create<TransmitterFactory>(),
ComponentType.Create<Status>(),
ComponentType.Create<Upgradeable>()
);


        collectNode = em.CreateArchetype
    (
        ComponentType.Create<Position>(),
        ComponentType.Create<MeshInstanceRenderer>(),
        ComponentType.Create<Square>(),
        //ComponentType.Create<Static>(),
        ComponentType.Create<RadiusSet>(),
        ComponentType.Create<CollectNode>()
    );

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
