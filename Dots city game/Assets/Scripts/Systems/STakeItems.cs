﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

//[UpdateAfter(typeof(SArrival))]
[UpdateAfter(typeof(SMovement))]
[UpdateAfter(typeof(STransferDrone))]
public class STakeItems : JobComponentSystem
{
    public static JobHandle takeItemsHandle;

    struct _Storages
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<StorageBuffer> storage;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Drone> drone;
        public ComponentDataArray<Movement> move;
    }

    [Inject] _Drones _drones;

    [Inject] _Storages _storages;

    [Inject] [ReadOnly] ComponentDataFromEntity<MineField> allMineField;

    // Start is called before the first frame update
    void Start()
    {
        Enabled = false;
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        TakeItemsJob job = new TakeItemsJob
        {
            storageSelf = _storages.self,
            storage=_storages.storage,

            Length=_drones.Length,
            droneSelf = _drones.self,
            drone = _drones.drone,
            move=_drones.move,

            deltaTime = Time.deltaTime,
        };
        takeItemsHandle = job.Schedule(_storages.Length, 1, inputDeps);

        return takeItemsHandle;
    }

    [BurstCompile]
    struct TakeItemsJob : IJobParallelFor
    {
        
        public EntityArray storageSelf;
        public BufferArray<StorageBuffer> storage;

        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;

        public float deltaTime;

        public void Execute(int index)
        {
            for (int i=0; i<Length;i++)
            {
                if (move[i].target==storageSelf[index])
                {
                    if (move[i].arrive==1)
                    {
                        Drone d = drone[i];
                        Movement m = move[i];

                        if (drone [i].getOrStore==1)
                        {
                            for (int j = 0; j < storage[index].Length; j++)
                            {
                                if (storage[index][j].storage.itemStoring == drone[i].toGet)
                                {
                                    Storage s = storage[index][j];
                                    if (s.containsAmmount>0)
                                    {
                                        s.containsAmmount--;
                                        storage[index].RemoveAt(j);
                                        storage[index].Insert(j, s);

                                        d.carrying = s.itemStoring;
                                        d.getOrStore = 0;
                                        d.toGet = Item.Empty;

                                        m.arrive = 0;
                                        m.target = d.workEntity;
                                    }
                                    break;
                                }
                            }
                        }
                        else if (drone[i].getOrStore==2)
                        {
                            Item item = drone[i].carrying;
                            for (int j = 0; j < storage[index].Length; j++)
                            {
                                if (storage[index][j].storage.itemStoring == item)
                                {
                                    Storage s = storage[index][j];
                                    if (s.containsAmmount<s.maxContains)
                                    {
                                        s.containsAmmount++;
                                        storage[index].RemoveAt(j);
                                        storage[index].Insert(j, s);

                                        d.carrying = Item.Empty;
                                        d.getOrStore = 0;

                                        m.target = d.workEntity;
                                        m.arrive = 0;
                                    }
                                    break;
                                }
                            }
                        }

                        move[i] = m;
                        drone[i] = d;
                    }
                }
            }
        }
    }
}
