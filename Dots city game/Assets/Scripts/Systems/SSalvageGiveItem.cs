﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

[UpdateAfter(typeof(SSalvageGiveItem))]
public class BSalvageGiveItem : BarrierSystem { }

//[UpdateAfter(typeof(SArrival))]
//[UpdateAfter(typeof(STakeItems))]
public class SSalvageGiveItem : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Salvage
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Salvage> salvage;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> st;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;
    }

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    [Inject] _Drones _drones;
    [Inject] _Salvage _salvage;

    [Inject] [ReadOnly] ComponentDataFromEntity<MineField> allMineField;
    //[Inject] [ReadOnly] ComponentDataFromEntity<StorageTarget> allStorageTarget;
    //[Inject] [ReadOnly] ComponentDataFromEntity<Salvage> allSalvage;

    [Inject] BSalvageGiveItem barrier;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ArriveJob job = new ArriveJob
        {
            salvageSelf = _salvage.self,
            salvage = _salvage.salvage,
            status = _salvage.status,
            st = _salvage.st,
            droneBuffer = _salvage.droneBuffer,
            droneBufferData = _salvage.droneBufferData,

            Length = _drones.Length,
            droneSelf = _drones.self,
            move = _drones.move,
            drone = _drones.drone,

            allMineField = allMineField,
            //allStorageTarget=allStorageTarget,

            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),
        };
        arrivalHandle = job.Schedule(_salvage.Length, 1, inputDeps);

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public EntityArray salvageSelf;
        public ComponentDataArray<Salvage> salvage;
        public ComponentDataArray<Status> status;
        [ReadOnly] public ComponentDataArray<StorageTarget> st;
        public BufferArray<DroneBuffer> droneBuffer;
        public ComponentDataArray<DroneBufferData> droneBufferData;

        public int Length;
        public EntityArray droneSelf;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Movement> move;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Drone> drone;

        [ReadOnly] public ComponentDataFromEntity<MineField> allMineField;

        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;

        public void Execute(int index)
        {

            Salvage s = salvage[index];
            //if there is no drones and salvage is empty safely destroy this object
            if (droneBuffer[index].Length == 0 && salvage[index].scrapAmmount == 0)
            {
                //buffer.DestroyEntity(salvageSelf[index]);
                s.change = 1;
            }

            /*
            if (status[index].active == 0)
            {
                return;
            }
            */

            if (s.scrapAmmount > 0)
            {
                for (int i = 0; i < Length; i++)
                {
                    if (move[i].target == salvageSelf[index])
                    {
                        if (move[i].arrive == 1)
                        {
                            s.currentTime += deltaTime;

                            if (s.currentTime > s.maxTime)
                            {
                                s.currentTime = 0;
                                s.scrapAmmount -= 1;
                                if (s.scrapAmmount <= 0)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (s.scrapAmmount <= 0)
            {
                DroneBufferData dbd = droneBufferData[index];
                dbd.maxDrones = 0;
                droneBufferData[index] = dbd;

                for (int i = 0; i < Length; i++)
                {
                    if (move[i].target == salvageSelf[index])
                    {
                        Drone d = drone[i];
                        Movement m = move[i];

                        //mark drones for reallocation
                        d.doChangeJob = 1;
                        d.nextJob = NextJobType.Idle;

                        for (int j = 0; j < droneBuffer[index].Length; j++)
                        {
                            if (droneBuffer[index][j] == droneSelf[i])
                            {
                                droneBuffer[index].RemoveAt(j);
                                break;
                            }
                        }

                        drone[i] = d;
                        move[i] = m;
                    }
                }
            }
            salvage[index] = s;

            /*
            //if there is no drones and salvage is empty safely destroy this object
            if (droneBuffer[index].Length==0 && salvage[index].scrapAmmount==0 && salvage[index].rewardAmmount==0)
            {
                //buffer.DestroyEntity(salvageSelf[index]);
                Salvage s = salvage[index];
                s.change = 1;
                salvage[index] = s;
            }

            if (status[index].active==0)
            {
                return;
            }

            for (int i = 0; i < Length; i++)
            {
                if (move[i].target == salvageSelf[index])
                {
                    if (move[i].arrive == 1)
                    {
                        Drone d = drone[i];
                        Movement m = move[i];

                        Salvage s = salvage[index];
                        StorageTarget _st = st[index];

                        d.currentTime += deltaTime;

                        if (d.currentTime>s.maxTime)
                        {
                            d.currentTime = 0;
                            if (s.scrapAmmount == 0 && s.rewardAmmount == 0)
                            {
                                DroneBufferData dbd = droneBufferData[index];
                                dbd.maxDrones = 0;
                                droneBufferData[index] = dbd;

                                //mark drones for reallocation
                                d.doChangeJob = 1;
                                d.nextJob = NextJobType.Idle;

                                for (int j = 0; j < droneBuffer[index].Length; j++)
                                {
                                    if (droneBuffer[index][j] == droneSelf[i])
                                    {
                                        droneBuffer[index].RemoveAt(j);
                                        break;
                                    }
                                }
                            }

                            if (s.scrapAmmount <= 0 && s.rewardAmmount > 0)
                            {
                                s.rewardAmmount--;
                                d.carrying = s.reward;
                                d.getOrStore = 2;

                                m.target = _st.storageTarget;
                                m.arrive = 0;
                            }

                            if (s.scrapAmmount>0)
                            {
                                s.scrapAmmount--;
                                d.carrying = s.scrap;

                                m.target = _st.storageTarget;
                                m.arrive = 0;
                                d.getOrStore = 2;
                            }
                        }
                        salvage[index] = s;
                        drone[i] =d;
                        move[i] =m;
                    }
                }
            }
            */
        }
    }
}
