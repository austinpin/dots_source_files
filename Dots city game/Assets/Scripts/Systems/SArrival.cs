﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

/*
[UpdateAfter(typeof(BDoPlayerAction))]
[UpdateAfter(typeof(SMovement))]
[UpdateAfter(typeof(STakeItems))] // IMPORTANT, must update after as mentioned system takes items, this system deletes it
public class SArrival : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct _Drones
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;
    }

    [Inject] _Drones _drones;

    [Inject] [ReadOnly] ComponentDataFromEntity<MineField> allMineField;
    [Inject] [ReadOnly] ComponentDataFromEntity<Status> allStatus;
    [Inject] [ReadOnly] ComponentDataFromEntity<Salvage> allSalvage;
    [Inject] [ReadOnly] ComponentDataFromEntity<Build> allBuild;
    [Inject] [ReadOnly] ComponentDataFromEntity<StorageTarget> allStorageTarget;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        /*
        ArriveJob job = new ArriveJob
        {
            self = _drones.self,
            move = _drones.move,
            drone = _drones.drone,

            allMineField=allMineField,
            allStatus=allStatus,
            allSalvage=allSalvage,
            allbuild=allBuild,

            allStorageTarget=allStorageTarget,

            deltaTime = Time.deltaTime,
        };
        arrivalHandle = job.Schedule(_drones.Length, 1, inputDeps);
        /

        return arrivalHandle;
    }

    [BurstCompile]
    struct ArriveJob : IJobParallelFor
    {
        public EntityArray self;
        public ComponentDataArray<Movement> move;
        public ComponentDataArray<Drone> drone;

        [ReadOnly] public ComponentDataFromEntity<MineField> allMineField;
        [ReadOnly] public ComponentDataFromEntity<Status> allStatus;
        [ReadOnly] public ComponentDataFromEntity<Salvage> allSalvage;
        [ReadOnly] public ComponentDataFromEntity<Build> allbuild;

        [ReadOnly] public ComponentDataFromEntity<StorageTarget> allStorageTarget;

        public float deltaTime;

        public void Execute(int index)
        {
            Drone d = drone[index];
            Movement m = move[index];

            if (m.arrive == 1)
            {
                if (d.myJob == MyJob.MineField)
                {
                    StorageTarget st = allStorageTarget[d.workEntity];
                    //minefield is technically inert object (no functional scripts)
                    //its ok to perform functions generically rather than with it as the core 
                    //Since i dont need to change it in any way
                    MineField mf = allMineField[d.workEntity];
                    Status _status = allStatus[d.workEntity];

                    if (_status.active == 1)
                    {
                        d.currentTime += deltaTime;

                        if (d.currentTime >= mf.maxTime)
                        {
                            d.carrying = mf.ore;
                            d.getOrStore = 2;
                            d.currentTime = 0;
                            m.target = st.storageTarget;
                            m.arrive = 0;
                        }
                    }
                }
                //
                //refresh movement AND drone with data from target index from task
                //drone i dont know where im going.
                //
                move[index] = m;
                drone[index] = d;
            }
        }
    }
}
*/