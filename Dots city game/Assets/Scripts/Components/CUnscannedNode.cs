﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum NodeTypes :byte
{
    SalvageZone,
    LargeSalvageZone,
    OverGrown,
    OverGrown2,
}

[System.Serializable]
public struct UnscannedNode : IComponentData
{
    public float maxScanTime;
    public float currentScanTime;
    public byte scanned;

    public NodeTypes nodeType;

    public float squareLength;

    public ushort powerNeeded;
    public byte active;
}
public class CUnscannedNode : ComponentDataWrapper<UnscannedNode> { }
