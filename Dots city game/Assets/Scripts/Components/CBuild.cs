﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum Building : byte
{
    Null,
    DumpZone,
    Idle,
    WareHouse,
    
    AbandonedWareHouse,
    Reactor,
    DroneBay,
    DroneFactory,

    VictoryMonument=200, //most things prior is not used //maybe energy relay to power space port
    Start=201,
    Beacon=202,

    Farm=80, //sell 1
    DiamondMine=81, //advanced sell material sell 2


    IrradiatedGenerator=70, //Energy set in ascending order
    WindPowerPlant=71,
    GeothermalGenerator=72,
    FissionGenerator=73,
    //SpaceSolarFarm = 73,

    RecyclingFactory = 30,

    TransmitterFactory =40, // item is used in beacons and port and other transmission objects. given free, can build few over time, decide where to use
    LevetationFactory=41, //floating things and things that need to eb lightweight
    TechCentre = 42,

    PhotiteMiner=60,
    DuriumMiner=61,
    VionMiner=62,

    NaniteMaker=63,
    ZoriumMaker=64,
    NornMaker=65,

    //Monuments, each one can only be built once and takes a lot of effort
    M_ReadvancementCentre=99, //research
    M_GrandBeacon =100,
    m_DeepMining=98, //a way to get more stuff
    M_Archives = 101, //like server room stuff
    M_Fabricator = 103, //make your own stuff
    M_TerraPort =102, //connects to space port or just space for do space things
    M_HumanColony = 97, //Take care of them
    M_FusionGenerator = 104,
    M_RelaySattelite=105, //internet and stuff
    M_Terraformer =106, //reputation 

    Base = 195,

    //final monument victory monument that powers space port.
}

[System.Serializable]
public struct Build : IComponentData
{
    public float buildTime;
    public float maxBuildTime;

    public byte spawnBuilding; //0 dont, //1 check valid, // 2 valid
    public byte boosted;
    public Building toBuild;

    public RevealType affinityDemand;
    public RevealType affinityNormal;
}
public class CBuild : ComponentDataWrapper<Build> { }
